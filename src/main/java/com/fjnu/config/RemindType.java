package com.fjnu.config;

public enum  RemindType {

    ZBJG(2), //指标警告
    NSTX(1); //农事提醒

    private int value = 0;

    private RemindType(int value) {     //必须是private的，否则编译错误
        this.value = value;
    }

    public int value() {
        return this.value;
    }

}