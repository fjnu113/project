package com.fjnu.config;

public enum  DirectionType {

    South("S%"),
    North("N%"),
    West("W%"),
    East("E%");

    private String value = null;

    private DirectionType(String value) {     //必须是private的，否则编译错误
        this.value = value;
    }

    public String value() {
        return this.value;
    }

}