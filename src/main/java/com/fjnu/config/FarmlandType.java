package com.fjnu.config;

public enum  FarmlandType {

    Intelligence(1), //智慧田
    Traditiona(2); //传统田

    private int value = 0;

    private FarmlandType(int value) {     //必须是private的，否则编译错误
        this.value = value;
    }

    public int value() {
        return this.value;
    }

}