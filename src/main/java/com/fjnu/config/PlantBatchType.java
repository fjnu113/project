package com.fjnu.config;

public enum  PlantBatchType {

    Present(1), //在植批次
    History(0); //历史批次

    private int value = 0;

    private PlantBatchType(int value) {     //必须是private的，否则编译错误
        this.value = value;
    }

    public int value() {
        return this.value;
    }


}