package com.fjnu.manage.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.client.service.Index.Interface.IndexService;
import com.fjnu.manage.entity.UserInformationPO;
import com.fjnu.manage.service.UserInformation.Interface.UserInfoService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * [后端用户信息管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/UserInformation")
public class UserInfoController {

    @Autowired
    private UserInfoService userinforService;

    @Autowired
    IndexService indexService;

    /*
* 增*/
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> add(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    /*
    * 删*/
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> delete(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(list.get(0).get("id"));
        userinforService.deleteById(id);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    /*
    * 改*/
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> edit(@RequestBody  String data, HttpServletRequest request, HttpServletResponse response){
        System.out.print(data);
        UserInformationPO userInformationPO= JSON.parseObject(data,UserInformationPO.class);
        int state = userinforService.update(userInformationPO);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        if( state == 1)
            jsonmap.put("msg","修改成功");
        else jsonmap.put("msg","修改失败");
        return  jsonmap;
    }

    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPage(@RequestParam("limit") int limit, @RequestParam("curr") int curr, HttpServletRequest request, HttpServletResponse response) {
        Page<UserInformationPO> page = new Page<>(curr, limit);
        List<UserInformationPO> list = userinforService.getPage(page, null);
        Integer count = userinforService.getCount(null);
        Map<String, Object> jsonmap = new HashMap<String, Object>();

        jsonmap.put("code", 0);
        jsonmap.put("data", list);
        jsonmap.put("count", count);
        jsonmap.put("msg", "查询成功");
        return jsonmap;
    }

    @RequestMapping(value = "/userInfoTable")
    public String table(){
        return "/pc/FarmerTable";
    }

    @RequestMapping(value = "/FarmerTable",method = RequestMethod.GET)
    public String FarmlandInfoTable(){
        return "/pc/FarmerTable";
    }

    @RequestMapping(value = "/FarmerInfoEdit",method = RequestMethod.GET)
    public String FarmlandInfoEdit(@RequestParam("data" ) String data, Model model){
        JSONArray jsonArray = JSON.parseArray(data);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        model.addAttribute("data",jsonObject);
        return "/pc/FarmerInfoEdit";
    }

    @RequestMapping(value = "/Main",method = RequestMethod.GET)
    public String Main(){return "/pc/Main";}


}