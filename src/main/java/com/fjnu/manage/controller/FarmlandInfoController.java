package com.fjnu.manage.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.FarmlandInformationPO;
import com.fjnu.manage.service.FarmlandInformation.Interface.FarmlandInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * [后端农田信息管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/FarmlandInfomation")
public class FarmlandInfoController {

    @Autowired
    public FarmlandInfoService farmlandInfoService;

    /*
    * 增*/
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> add(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("msg","添加成功");
        return  jsonmap;
    }

    /*
    * 删*/
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> delete(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(list.get(0).get("id"));
        farmlandInfoService.deleteById(id);
        Integer count = farmlandInfoService.getCount(null);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("msg","删除成功");
        jsonmap.put("count",count);
        return  jsonmap;
    }

    /*
    * 改*/
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> edit(@RequestBody  String data, HttpServletRequest request, HttpServletResponse response){
        FarmlandInformationPO farmlandInformationPO=JSON.parseObject(data,FarmlandInformationPO.class);
        int state = farmlandInfoService.update(farmlandInformationPO);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        if( state == 1)
        jsonmap.put("msg","修改成功");
        else jsonmap.put("msg","修改失败");
        return  jsonmap;
    }

    /*
    * 查询所有数据*/
    @RequestMapping(value = "/getAllList",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getAllList( HttpServletRequest request, HttpServletResponse response){
        List<FarmlandInformationPO> lists = farmlandInfoService.getAll();
        int count = lists.size();
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("data",lists);
        jsonmap.put("count",count);
        jsonmap.put("msg","查询成功");
        return jsonmap;
    }

    /*
    * 分页查询*/
    @RequestMapping(value = "/getPage",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getPage(@RequestParam("limit") int limit,@RequestParam("curr") int curr,HttpServletRequest request, HttpServletResponse response){
        Page<FarmlandInformationPO> page = new Page<>(curr,limit);
        List<FarmlandInformationPO> list = farmlandInfoService.getPage(page,null);

        Integer count = farmlandInfoService.getCount(null);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("data",list);
        jsonmap.put("count",count);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String index(){
        return "/pc/Main";
    }

    @RequestMapping(value = "/FarmlandInfoTable",method = RequestMethod.GET)
    public String FarmlandInfoTable(){
        return "/pc/FarmlandInfoTable";
    }

    @RequestMapping(value = "/FarmlandInfoEdit",method = RequestMethod.GET)
    public String FarmlandInfoEdit(@RequestParam("data" ) String data, Model model){
        JSONArray jsonArray = JSON.parseArray(data);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        model.addAttribute("data",jsonObject);
        return "/pc/FarmlandInfoEdit";
    }




}