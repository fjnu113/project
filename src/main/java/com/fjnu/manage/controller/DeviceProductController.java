package com.fjnu.manage.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.DeviceProductPO;
import com.fjnu.manage.service.DeviceProduct.Interface.DeviceProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [后端产品管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/DeviceProduct")
public class DeviceProductController {

    @Autowired
    private DeviceProductService deviceproductService;

    /*
* 增*/
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> add(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> jsonmap= getStringObjectHashMap();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    /*
    * 删*/
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> delete(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(list.get(0).get("id"));
        deviceproductService.deleteById(id);
        Map<String,Object> jsonmap= getStringObjectHashMap();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    private HashMap<String, Object> getStringObjectHashMap() {
        return new HashMap<String,Object>();
    }

    /*
    * 改*/
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> edit(@RequestBody  String data, HttpServletRequest request, HttpServletResponse response){
        DeviceProductPO deviceProductPO= JSON.parseObject(data,DeviceProductPO.class);
        int state = deviceproductService.update(deviceProductPO);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        if( state == 1)
            jsonmap.put("msg","修改成功");
        else jsonmap.put("msg","修改失败");
        return  jsonmap;
    }

    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPage(@RequestParam("limit") int limit, @RequestParam("curr") int curr, HttpServletRequest request, HttpServletResponse response) {
        Page<DeviceProductPO> page = new Page<>(curr, limit);
        List<DeviceProductPO> list = deviceproductService.getPage(page, null);
        Integer count = deviceproductService.getCount(null);
        Map<String, Object> jsonmap = getStringObjectHashMap();

        jsonmap.put("code", 0);
        jsonmap.put("data", list);
        jsonmap.put("count", count);
        jsonmap.put("msg", "查询成功");
        return jsonmap;
    }

    @RequestMapping(value = "/deviceProductTable")
    public String table(){
        return "/pc/DeviceProductTable";
    }


    @RequestMapping(value = "/DeviceProductTable",method = RequestMethod.GET)
    public String FarmlandInfoTable(){
        return "/pc/DeviceProductTable";
    }

    @RequestMapping(value = "/DeviceProductEdit",method = RequestMethod.GET)
    public String FarmlandInfoEdit(@RequestParam("data" ) String data, Model model){
        JSONArray jsonArray = JSON.parseArray(data);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        model.addAttribute("data",jsonObject);
        return "/pc/DeviceProductEdit";
    }


}