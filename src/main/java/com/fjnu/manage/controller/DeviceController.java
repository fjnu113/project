package com.fjnu.manage.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.DevicePO;
import com.fjnu.manage.service.Device.Interface.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [后端设备管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/Device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    /*
    * 增*/
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> add(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    /*
    * 删*/
    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> delete(@RequestBody  List<Map<String,String>> list, HttpServletRequest request, HttpServletResponse response){
        int id = Integer.parseInt(list.get(0).get("id"));
        deviceService.deleteById(id);
        Map<String, Object> jsonmap = getStringObjectMap();
        jsonmap.put("code",0);
        jsonmap.put("msg","查询成功");
        return  jsonmap;
    }

    private Map<String, Object> getStringObjectMap() {
        return getStringObjectHashMap();
    }

    private HashMap<String, Object> getStringObjectHashMap() {
        return new HashMap<String,Object>();
    }

    /*
    * 改*/
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> edit(@RequestBody  String data, HttpServletRequest request, HttpServletResponse response){
        DevicePO devicePO= JSON.parseObject(data,DevicePO.class);
        int state = deviceService.update(devicePO);
        Map<String,Object> jsonmap=new HashMap<String,Object>();
        jsonmap.put("code",0);
        if( state == 1)
            jsonmap.put("msg","修改成功");
        else jsonmap.put("msg","修改失败");
        return  jsonmap;
    }

    /*
    * 分页查询*/
    @RequestMapping(value = "/getPage", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getPage(@RequestParam("limit") int limit,
                                       @RequestParam("curr") int curr,
                                       HttpServletRequest request, HttpServletResponse response) {
        Page<DevicePO> page = new Page<>(curr, limit);
        List<DevicePO> list = deviceService.getPage(page,null);
        Integer count = deviceService.getCount(null);
        Map<String, Object> jsonmap = new HashMap<String, Object>();
        jsonmap.put("code", 0);
        jsonmap.put("data", list);
        jsonmap.put("count", count);
        jsonmap.put("msg", "查询成功");
        return jsonmap;
    }

    @RequestMapping(value = "/deviceTable")
    public String table(){
        return "/pc/DeviceTable";
    }

    @RequestMapping(value = "/DeviceTable",method = RequestMethod.GET)
    public String FarmlandInfoTable(){
        return "/pc/DeviceTable";
    }

    @RequestMapping(value = "/DeviceEdit",method = RequestMethod.GET)
    public String FarmlandInfoEdit(@RequestParam("data" ) String data, Model model){
        JSONArray jsonArray = JSON.parseArray(data);
        JSONObject jsonObject = jsonArray.getJSONObject(0);
        model.addAttribute("data",jsonObject);
        return "/pc/DeviceEdit";
    }


}