package com.fjnu.manage.dao;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fjnu.manage.entity.DeviceProductPO;


public interface DeviceProductDao extends BaseMapper<DeviceProductPO> {

}
