package com.fjnu.manage.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fjnu.manage.entity.UserInformationPO;

public interface UserInformationDao extends BaseMapper<UserInformationPO> {

}
