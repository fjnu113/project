package com.fjnu.manage.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fjnu.manage.entity.FarmlandInformationPO;


public interface FarmlandInformationDao extends BaseMapper<FarmlandInformationPO>{

}
