package com.fjnu.manage.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fjnu.manage.entity.WarningPO;

public interface WarningDao extends BaseMapper<WarningPO>{

}
