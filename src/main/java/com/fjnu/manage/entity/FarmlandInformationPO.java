package com.fjnu.manage.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName(value = "iwea_farmland")
public class FarmlandInformationPO {

    private Integer id;      //ID
    private String  num;     //农田ID
    private Integer area;    //农田面积
    private String  position;    //农田位置
    private Integer ic;    //光照系数
    private Integer farmer;    //负责人
    private Integer status;    //土壤状态
    private Integer deepth;    //土壤深度
    @TableField("farm_id")
    private Integer farmId;    //所属农场
    private Integer type;    //类型
    @TableField("duty_type")
    private Integer dutyType;    //值守类型

}