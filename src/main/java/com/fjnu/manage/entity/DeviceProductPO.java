package com.fjnu.manage.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Map;

@Data
@TableName("iwea_device_product")
public class DeviceProductPO
{
    private Integer id;         //ID
    private String name;         //产品名称
    private String factory;         //产品厂家
    private Integer type;         //产品类型
    private String num;         //产品型号
    private String description;         //产品描述
    @TableField("usage_type")
    private String usageType;         //设备类型用途

}