package com.fjnu.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Map;

@Data
@TableName("fog_device")
public class DevicePO {

    private Integer id;         //ID
    private String num;         //编号
    @TableField("device_product")
    private Integer deviceProduct;         //产品编号
    @TableField("usage_type")
    private String usageType;         //设备用途类型
    private String area;         //安装区域
    @TableField("farmland_id")
    private Integer farmlandId;         //农田ID
    @TableField("farm_id")
    private Integer farmId;         //农场ID
    private Integer technology;         //设备工艺
    private String parameter;         //设备参数
    private String protocol;         //设备协议
    @TableField("work_status")
    private Integer workStatus;         //工作状态
    private String host;         //设备地址
    @TableField("dtu_device")
    private Integer dtuDevice;         //dtu_设备
    private Integer type;         //产品类型

}