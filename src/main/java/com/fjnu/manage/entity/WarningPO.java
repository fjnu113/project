package com.fjnu.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Date;
import lombok.Data;


@Data
@TableName("iwea_remind")
public class WarningPO
{
    private Integer id;         //ID
    @TableField("farm_id")
    private Integer farmId;         //农场ID
    @TableField("farmland_id")
    private Integer farmlandId;         //农田ID
    private String title;         //预警标题
    private String content;         //预警内容
    @TableField("remind_time")
    private Date remindTime;         //提醒时间
    private Integer status;         //提醒状态
    @TableField("read_time")
    private Date readTime;         //阅读时间
    private Integer reminder;         //提醒人
    private Integer type;         //类别

}
