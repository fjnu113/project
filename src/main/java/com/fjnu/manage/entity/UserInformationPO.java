package com.fjnu.manage.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_farmer")
public class UserInformationPO
{

    private Integer id;    //用户ID
    private Integer name;    //用户姓名
    private String gender;    //用户性别
    private Integer level;    //用户等级
    private Integer exp;    //用户经验值
    private String introduction;    //用户介绍
    private Integer login;    //用户账号
    private String title;    //用户称号
}