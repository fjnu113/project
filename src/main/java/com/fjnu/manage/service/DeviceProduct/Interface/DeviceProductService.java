package com.fjnu.manage.service.DeviceProduct.Interface;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.DeviceProductPO;


import java.util.List;

public interface DeviceProductService {

    void insert(DeviceProductPO camerapo);

    int update(DeviceProductPO camerapo);

    List<DeviceProductPO> getAll();

    DeviceProductPO selectById(int id);

    void deleteById(int id);

    List<DeviceProductPO> getPage(Page page, Wrapper wrapper);

    Integer getCount(Wrapper wrapper);

}
