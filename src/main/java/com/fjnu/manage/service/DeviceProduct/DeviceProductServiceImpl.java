package com.fjnu.manage.service.DeviceProduct;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.dao.DeviceProductDao;
import com.fjnu.manage.entity.DeviceProductPO;
import com.fjnu.manage.service.DeviceProduct.Interface.DeviceProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class DeviceProductServiceImpl implements DeviceProductService {
    @Autowired
    private DeviceProductDao deviceProductDao;
    @Override
    public void insert(DeviceProductPO deviceProductPO) {
        deviceProductDao.insert(deviceProductPO);
    }
    @Override
    public List<DeviceProductPO> getAll() {
        return deviceProductDao.selectList(null);
    }
    @Override
    public int update(DeviceProductPO deviceProductPO) {
        return deviceProductDao.updateById(deviceProductPO);
    }
    @Override
    public DeviceProductPO selectById(int id) {
        return deviceProductDao.selectById(id);
    }
    @Override
    public void deleteById(int id) { deviceProductDao.deleteById(id); }

    @Override
    public List<DeviceProductPO> getPage(Page page, Wrapper wrapper) {
        return deviceProductDao.selectPage(page,wrapper);
    }

    @Override
    public Integer getCount(Wrapper wrapper) {
        return deviceProductDao.selectCount(wrapper);
    }
}