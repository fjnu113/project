package com.fjnu.manage.service.UserInformation;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.dao.UserInformationDao;
import com.fjnu.manage.entity.UserInformationPO;
import com.fjnu.manage.service.UserInformation.Interface.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {


    @Autowired
    private UserInformationDao userinfoDao;

    @Override
    public int insert(UserInformationPO userinfoPO) {
        return userinfoDao.insert(userinfoPO);
    }

    @Override
    public List<UserInformationPO> getAll() {
        return userinfoDao.selectList(null);
    }

    @Override
    public int update(UserInformationPO userinfoPO) {
        return userinfoDao.updateById(userinfoPO);
    }

    @Override
    public UserInformationPO selectById(int id) {
        return userinfoDao.selectById(id);
    }

    @Override
    public void deleteById(int id) {
        userinfoDao.deleteById(id);
    }

    @Override
    public List<UserInformationPO> getPage(Page page, Wrapper wrapper) {
        return userinfoDao.selectPage(page,wrapper);
    }

    @Override
    public Integer getCount(Wrapper wrapper) {
        return userinfoDao.selectCount(wrapper);
    }

}