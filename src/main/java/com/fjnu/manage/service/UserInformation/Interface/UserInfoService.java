package com.fjnu.manage.service.UserInformation.Interface;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.UserInformationPO;

import java.util.List;

public interface UserInfoService {

    int insert(UserInformationPO userinformationpo);

    int update(UserInformationPO userinformationpo);

    List<UserInformationPO> getAll();

    UserInformationPO selectById(int id);

    void deleteById(int id);

    List<UserInformationPO> getPage(Page page, Wrapper wrapper);

    Integer getCount(Wrapper wrapper);

}
