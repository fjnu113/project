package com.fjnu.manage.service.FarmlandInformation.Interface;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.FarmlandInformationPO;

import java.util.List;

public interface FarmlandInfoService {

    void insert(FarmlandInformationPO farmlandinformationpo);

    int update(FarmlandInformationPO farmlandinformationpo);

    List<FarmlandInformationPO> getAll();

    FarmlandInformationPO selectById(int id);

    void deleteById(int id);

    List<FarmlandInformationPO> getPage(Page page, Wrapper wrapper);

    Integer getCount(Wrapper wrapper);

}
