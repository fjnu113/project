package com.fjnu.manage.service.FarmlandInformation;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.dao.FarmlandInformationDao;
import com.fjnu.manage.entity.FarmlandInformationPO;
import com.fjnu.manage.service.FarmlandInformation.Interface.FarmlandInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class FarmlandInfoServiceImpl implements FarmlandInfoService {


    @Autowired
    private FarmlandInformationDao farmlandinfoDao;

    @Override
    public void insert(FarmlandInformationPO farmlandinfoPO) { farmlandinfoDao.insert(farmlandinfoPO); }

    @Override
    public List<FarmlandInformationPO> getAll() { return farmlandinfoDao.selectList(null); }

    @Override
    public int update(FarmlandInformationPO farmlandinfoPO) { return (farmlandinfoDao.updateById(farmlandinfoPO)); }

    @Override
    public FarmlandInformationPO selectById(int id) {
     return farmlandinfoDao.selectById(id);
    }

    @Override
    public void deleteById(int id) { farmlandinfoDao.deleteById(id); }

    @Override
    public List<FarmlandInformationPO> getPage(Page page,Wrapper wrapper) {
        return farmlandinfoDao.selectPage(page,wrapper);
    }

    @Override
    public Integer getCount(Wrapper wrapper) {
        return farmlandinfoDao.selectCount(wrapper);
    }

}