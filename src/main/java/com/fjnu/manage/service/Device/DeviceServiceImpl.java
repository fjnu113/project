package com.fjnu.manage.service.Device;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.dao.DeviceDao;
import com.fjnu.manage.entity.DevicePO;
import com.fjnu.manage.service.Device.Interface.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    private DeviceDao deviceDao;

    @Override
    public void insert(DevicePO devicePO) {
        deviceDao.insert(devicePO);
    }

    @Override
    public List<DevicePO> getAll() {
        return deviceDao.selectList(null);
    }

    @Override
    public int update(DevicePO devicePO) {
        return deviceDao.updateById(devicePO);
    }

    @Override
    public DevicePO selectById(int id) {
        return deviceDao.selectById(id);
    }

    @Override
    public void deleteById(int id) {
        deviceDao.deleteById(id);
    }

    @Override
    public List<DevicePO> getPage(Page page, Wrapper wrapper) {
        return deviceDao.selectPage(page,wrapper);
    }

    @Override
    public Integer getCount(Wrapper wrapper) {
        return deviceDao.selectCount(wrapper);
    }

}