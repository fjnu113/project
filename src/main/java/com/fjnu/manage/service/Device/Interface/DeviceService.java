package com.fjnu.manage.service.Device.Interface;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.DevicePO;

import java.util.List;

public interface DeviceService  {

    void insert(DevicePO devicepo);

    int update(DevicePO devicepo);

    List<DevicePO> getAll();

    DevicePO selectById(int id);

    void deleteById(int id);

    List<DevicePO> getPage(Page page, Wrapper wrapper);

    Integer getCount(Wrapper wrapper);

}
