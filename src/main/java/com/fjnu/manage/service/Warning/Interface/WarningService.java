package com.fjnu.manage.service.Warning.Interface;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.entity.WarningPO;

import java.util.List;

public interface WarningService {

    int insert(WarningPO warningpo);

    int update(WarningPO warningpo);

    List<WarningPO> getAll();

    WarningPO selectById(int id);

    void deleteById(int id);

    List<WarningPO> getPage(Page page, Wrapper wrapper);

    Integer getCount(Wrapper wrapper);

}
