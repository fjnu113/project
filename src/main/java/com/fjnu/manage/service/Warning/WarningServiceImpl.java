package com.fjnu.manage.service.Warning;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.fjnu.manage.dao.WarningDao;
import com.fjnu.manage.entity.WarningPO;
import com.fjnu.manage.service.Warning.Interface.WarningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class WarningServiceImpl implements WarningService {


    @Autowired
    private WarningDao warningDao;

    @Override
    public int insert(WarningPO warningPO) {
       return warningDao.insert(warningPO);
    }

    @Override
    public List<WarningPO> getAll() {
        return warningDao.selectList(null);
    }

    @Override
    public int update(WarningPO warningPO) {
        return warningDao.updateById(warningPO);
    }

    @Override
    public WarningPO selectById(int id) {
        return warningDao.selectById(id);
    }

    @Override
    public void deleteById(int id) {
        warningDao.deleteById(id);
    }

    @Override
    public List<WarningPO> getPage(Page page, Wrapper wrapper) {
        return warningDao.selectPage(page,wrapper);
    }

    @Override
    public Integer getCount(Wrapper wrapper) {
        return warningDao.selectCount(wrapper);
    }

}