package com.fjnu.client.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.fjnu.client.entity.po.ExpertLibraryPO;
import com.fjnu.client.entity.vo.ExpertLibraryVO;
import com.fjnu.client.entity.vo.VarietyDetailVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface ExpertLibraryDao extends BaseMapper<ExpertLibraryPO> {

    @Select("select * from iwea_variety")
    public List<ExpertLibraryVO> getExpertLibrary();

    @Select("SELECT " +
            " variety.`name` variety_name,  " +
            " variety.describetion, " +
            " plant_method.suitable_growth_time, " +
            " plant_method.cycle_length, " +
            " plant_method.per_mu, " +
            " plant_method.nutritional_value, " +
            " plant_method.habit plant_habit, " +
            " plant_method.planting_method, " +
            " plant_method.planting_consumption " +
            "FROM " +
            " iwea_variety variety,iwea_plant_method plant_method  " +
            "WHERE " +
            " variety.`name` =#{VarietyName} " +
            " and variety.id = plant_method.variety_id")
    public List<VarietyDetailVO> getVarietyDetail(@Param("VarietyName") String varietyName);
}
