package com.fjnu.client.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fjnu.client.entity.vo.FarmlandVO;
import com.fjnu.client.service.ExpertLibrary.Interface.ExpertLibraryService;
import com.fjnu.config.FarmlandType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [app专家库模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/ExpertLibrary")
public class ExpertLibraryController {

    @Autowired
    ExpertLibraryService expertLibraryService;

    /**
     * [获取专家库信息]
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/Main",method = RequestMethod.GET)
    @ResponseBody
    public Map<String ,Object> getExpertLibrary(){
        List list = expertLibraryService.getExpertLibrary();
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取品种详细信息]
     * @param data [varietyName 品种名称]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/Detail",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> getVarietyDetail(@RequestBody String data, HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        String varietyName = jsonObject.getString("varietyName");
        List list = expertLibraryService.getVarietyDetail(varietyName);
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

}