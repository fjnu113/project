package com.fjnu.client.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fjnu.client.entity.vo.FarmlandVO;
import com.fjnu.client.service.FarmlandManage.Interface.FarmlandManageService;
import com.fjnu.config.FarmlandType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [app农田管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/FarmlandManage")
public class FarmlandManageController {

    @Autowired
    private FarmlandManageService farmlandmanageService;
    
    /**
     * [获取智慧田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/intelligentFarmlandMain",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> intelligentFarmlandMain(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List<FarmlandVO> list = farmlandmanageService.getFarmlandMainInfo(login, FarmlandType.Intelligence.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取智慧田详细信息]
     * @param data [farmlandId 农田编号 farmId农场编号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/intelligentFarmlandDetail",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> intelligentFarmlandDetail(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int farmlandId = jsonObject.getInteger("farmlandId");
        int farmId = jsonObject.getInteger("farmId");
        List<FarmlandVO> list = farmlandmanageService.getIntelligentFarmlandDetailInfo(farmlandId,farmId,FarmlandType.Intelligence.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取传统田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/traditionalFarmlandMain",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> traditionalFarmlandMain(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List<FarmlandVO> list = farmlandmanageService.getFarmlandMainInfo(login,FarmlandType.Traditiona.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取传统田详细信息]
     * @param data [farmlandId 农田编号 farmId农场编号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/traditionalFarmlandDetail",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> traditionalFarmlandDetail(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int farmlandId = jsonObject.getInteger("farmlandId");
        int farmId = jsonObject.getInteger("farmId");
        List<FarmlandVO> list = farmlandmanageService.getTraditionalFarmlandDetailInfo(farmlandId,farmId,FarmlandType.Traditiona.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }
}