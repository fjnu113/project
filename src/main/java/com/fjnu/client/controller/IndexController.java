package com.fjnu.client.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fjnu.client.service.Index.Interface.IndexService;
import com.fjnu.config.DirectionType;
import com.fjnu.config.HttpUtils;
import com.fjnu.config.PlantBatchType;
import com.fjnu.config.RemindType;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [app首页模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Component
@RequestMapping("/IndexController")
public class IndexController {

    @Autowired
    IndexService indexService;
    /**
     * [获取天气信息]
     * @param data [city 城市名]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/getWeather",method = RequestMethod.POST)
    @ResponseBody
    public Map<String ,Object> getWeather(@RequestBody String data, HttpServletRequest request, HttpServletResponse response) throws Exception {
        JSONObject jsonObject = JSON.parseObject(data);
        String city = jsonObject.getString("city");
        String host = "http://saweather.market.alicloudapi.com";
        String path = "/day15";
        String method = "GET";
        String appcode = "d41de53f4cbf489ea5163fce2688d9b9";
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("area", city);
        HttpResponse res = HttpUtils.doGet(host, path, method, headers, querys);
        jsonObject = JSON.parseObject(EntityUtils.toString(res.getEntity()));
        JSONObject jsonBody = jsonObject.getJSONObject("showapi_res_body");
        JSONArray jsonArrayDayList = jsonBody.getJSONArray("dayList");
        jsonObject = jsonArrayDayList.getJSONObject(0);
        return jsonObject;

    }

    /**
     * [验证账号密码的一致性]
     * @param data [login 账号 password 密码]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/9
     * @return [通过返回 pass 否则返回 reject]
     * @version
     */
    @RequestMapping(value = "/verifyPassword",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> verifyPassword(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        String password =jsonObject.getString("password");
        String result = indexService.verifyPassword(login,password);
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("result",result);
        return jsonMap;
    }

    /**
     * [选择农田功能中获取农田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/9
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getIndexFarmlandEast",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getIndexFarmlandEast(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getFarmlandInfo(login, DirectionType.East.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [选择农田功能中获取农田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/9
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getIndexFarmlandNorth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getIndexFarmlandNorth(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getFarmlandInfo(login,DirectionType.North.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [选择农田功能中获取农田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/9
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getIndexFarmlandSouth",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getIndexFarmlandSouth(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getFarmlandInfo(login,DirectionType.South.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [选择农田功能中获取农田简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/9
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getIndexFarmlandWest",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getIndexFarmlandWest(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getFarmlandInfo(login,DirectionType.West.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /* **
     * [获取生产指标]
     *
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2020/1/30
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getProductionIndex",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getProductionIndex(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getProductionIndex(login);
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

        /* **
     * [获取在植作物]
     *
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2020/1/30
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version
     */
    @RequestMapping(value = "/getCropPlanting",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getCropPlanting(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getCropPlanting(login, PlantBatchType.Present.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

        /* **
    * [获取能耗指标(平均)]
    * @param request
    * @param response
    * @author lwf
    * @date 2020/1/30
    * @return java.util.Map<java.lang.String,java.lang.Object>
    * @version
    */
    @RequestMapping(value = "/getConsumptionIndex",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getConsumptionIndex(){
        List list = indexService.getConsumptionIndex();
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /* **
* [获取能耗指标(月)]
* @param request
* @param response
* @author lwf
* @date 2020/1/30
* @return java.util.Map<java.lang.String,java.lang.Object>
* @version
*/
    @RequestMapping(value = "/getConsumptionIndexMonth",method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> getConsumptionIndexMonth(){
        List list = indexService.getConsumptionIndexMonth();
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

        /* **
    * [获取提醒数量]
    * @param request
    * @param response
    * @author lwf
    * @date 2020/1/30
    * @return java.util.Map<java.lang.String,java.lang.Object>
    * @version
    */
    @RequestMapping(value = "/getRemindCount",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getRemindCount(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getRemindCount(login);
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /* **
* [获取提醒详细(农事提醒)]
* @param request
* @param response
* @author lwf
* @date 2020/1/30
* @return java.util.Map<java.lang.String,java.lang.Object>
* @version
*/
    @RequestMapping(value = "/getNSTX",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getNSTX(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getRemindDetail(login, RemindType.NSTX.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /* **
* [获取提醒详细(农事提醒)]
* @param request
* @param response
* @author lwf
* @date 2020/1/30
* @return java.util.Map<java.lang.String,java.lang.Object>
* @version
*/
    @RequestMapping(value = "/getZBJG",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getZBJG(@RequestBody String data,HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = indexService.getRemindDetail(login, RemindType.ZBJG.value());
        Map<String ,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }




    @RequestMapping(value = "/Index",method = RequestMethod.GET)
    public String index(){
        return "/app/index";
    }

    @RequestMapping(value = "/FarmlandManage",method = RequestMethod.GET)
    public String farmlandManage(){
        return "/app/FarmlandManage";
    }

    @RequestMapping(value = "/PlantingManage",method = RequestMethod.GET)
    public String plantingManage(){
        return "/app/PlantingManage";
    }

    @RequestMapping(value = "/ExpertLibrary",method = RequestMethod.GET)
    public String expertLibrary() {
        return "/app/ExpertLibrary";
    }

    @RequestMapping(value = "/FoodTraceability",method = RequestMethod.GET)
    public String FoodTraceability(){return "/app/FoodTraceability";}

    @RequestMapping(value = "/PasswordChange",method = RequestMethod.GET)
    public String PasswordChange(){return "/app/PasswordChange";}

    @RequestMapping(value = "/SmartFarmland",method = RequestMethod.GET)
    public String smartFarmland(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/SmartFarmland";
    }

    @RequestMapping(value = "/TraditionalFarmland",method = RequestMethod.GET)
    public String traditionalFarmland(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/TraditionalFarmland";
    }

    @RequestMapping(value = "/Plant Batches",method = RequestMethod.GET)
    public String plantBatches(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/Plant Batches";
    }

    @RequestMapping(value = "/EcologicalTraceability",method = RequestMethod.GET)
    public String ecologicalTraceability(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/EcologicalTraceability";
    }

    @RequestMapping(value = "/Cultivation",method = RequestMethod.GET)
    public String cultivation(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/CultivationDetails";
    }

    @RequestMapping(value = "/getIndexName",method = RequestMethod.GET)
    public String getIndexName(@RequestParam("data" ) String data, Model model)
    {
        JSONObject jsonObject = JSON.parseObject(data);
        model.addAttribute("data",jsonObject);
        return "/app/index";
    }
}