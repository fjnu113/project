package com.fjnu.client.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.fjnu.client.service.PlantManage.Interface.PlantManageService;
import com.fjnu.config.PlantBatchType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * [app种植管理模块]
 * @author lwf
 * @date 2019/11/29
 * @version 1.0
 */
@Controller
@RequestMapping("/PlantManage")
public class PlantManageController {

    @Autowired
    private PlantManageService plantmanageService;

    /**
     * [获取当前批次简要信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/presentPlantBath",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> presentPlantBath(@RequestBody String data, HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = plantmanageService.getPlantBatch(login, PlantBatchType.Present.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取历史批次信息]
     * @param data [login 账号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/historyPlantBath",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> historyPlantBath(@RequestBody String data, HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        int login = jsonObject.getInteger("login");
        List list = plantmanageService.getPlantBatch(login, PlantBatchType.History.value());
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }


    /**
     * [获取当前批次详细信息]
     * @param data [batchNum 批次号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/presentPlantBathDetail",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> presentPlantBathDetail(@RequestBody String data, HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        String batchNum = jsonObject.getString("batchNum");
        List list = plantmanageService.getPresentPlantBatchDetail(batchNum);
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }

    /**
     * [获取历史批次生态溯源详细信息]
     * @param data [batchNum 批次号]
     * @param request
     * @param response
     * @author lwf
     * @date 2019/12/8
     * @return java.util.Map<java.lang.String,java.lang.Object>
     * @version 1.0
     */
    @RequestMapping(value = "/ecologicalTraceabilityDetail",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> ecologicalTraceabilityDetail(@RequestBody String data, HttpServletRequest request, HttpServletResponse response){
        JSONObject jsonObject = JSON.parseObject(data);
        String batchNum = jsonObject.getString("batchNum");
        List list = plantmanageService.getHistoryPlantBatchDetail(batchNum);
        Map<String,Object> jsonMap = new HashMap<>();
        jsonMap.put("data",list);
        return jsonMap;
    }
}