package com.fjnu.client.service.ExpertLibrary.Interface;

import com.baomidou.mybatisplus.service.IService;
import com.fjnu.client.entity.po.ExpertLibraryPO;
import com.fjnu.client.entity.vo.ExpertLibraryVO;
import com.fjnu.client.entity.vo.VarietyDetailVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExpertLibraryService extends IService<ExpertLibraryPO>{

    public List getExpertLibrary();

    public List getVarietyDetail(String varietyName);

}
