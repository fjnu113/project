package com.fjnu.client.service.ExpertLibrary;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fjnu.client.dao.ExpertLibraryDao;
import com.fjnu.client.entity.po.ExpertLibraryPO;
import com.fjnu.client.entity.vo.ExpertLibraryVO;
import com.fjnu.client.entity.vo.VarietyDetailVO;
import com.fjnu.client.service.ExpertLibrary.Interface.ExpertLibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public  class ExpertLibraryServiceImpl extends ServiceImpl<ExpertLibraryDao,ExpertLibraryPO> implements ExpertLibraryService {

    @Autowired
    ExpertLibraryDao expertLibraryDao;

    @Override
    public List<ExpertLibraryVO> getExpertLibrary() {
        return expertLibraryDao.getExpertLibrary();
    }

    @Override
    public List<VarietyDetailVO> getVarietyDetail(String varietyName) {
        return expertLibraryDao.getVarietyDetail(varietyName);
    }
}