package com.fjnu.client.service.FarmlandManage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fjnu.client.dao.FarmlandManageDao;
import com.fjnu.client.entity.po.FarmlandManagePO;
import com.fjnu.client.entity.vo.FarmlandVO;
import com.fjnu.client.service.FarmlandManage.Interface.FarmlandManageService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public  class FarmlandManageServiceImpl extends ServiceImpl<FarmlandManageDao,FarmlandManagePO> implements FarmlandManageService {


    @Override
    public List<FarmlandVO> getFarmlandMainInfo(int login, int type) {
        return this.baseMapper.getFarmlandMainInfo(login,type);
    }

    @Override
    public List<FarmlandVO> getIntelligentFarmlandDetailInfo(int farmlandId, int farmId,int type) {
        return this.baseMapper.getIntelligentFarmlandDetailInfo(farmlandId,farmId,type);
    }

    @Override
    public List<FarmlandVO> getTraditionalFarmlandDetailInfo( int farmlandId, int farmId,int type) {
        return this.baseMapper.getTraditionalFarmlandDetailInfo(farmlandId,farmId,type);
    }
}