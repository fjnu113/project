package com.fjnu.client.service.FarmlandManage.Interface;

import com.baomidou.mybatisplus.service.IService;
import com.fjnu.client.entity.po.FarmlandManagePO;
import com.fjnu.client.entity.vo.FarmlandVO;

import java.util.List;

public interface FarmlandManageService extends IService<FarmlandManagePO> {

    public List getFarmlandMainInfo(int login, int type);

    public List getIntelligentFarmlandDetailInfo(int farmlandId,int farmId,int type);

    public List getTraditionalFarmlandDetailInfo(int farmlandId,int farmId,int type);

}
