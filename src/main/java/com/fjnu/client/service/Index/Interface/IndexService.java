package com.fjnu.client.service.Index.Interface;

import com.fjnu.client.entity.vo.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface IndexService {
    public String verifyPassword(int login,String password);

    public List getFarmlandInfo(int login,String direction);

    public List<ProductionIndexVO> getProductionIndex(int login);

    public List<CropPlantingVO> getCropPlanting( int login, int status);

    public List<ConsumptionIndexVO> getConsumptionIndex();

    public List<RemindCountVO> getRemindCount(int login);

    public List<RemindDetailVO> getRemindDetail( int login, int type);

    public List<ConsumptionIndexMonthVO> getConsumptionIndexMonth();

}
