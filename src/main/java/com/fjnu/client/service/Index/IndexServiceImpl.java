package com.fjnu.client.service.Index;

import com.fjnu.client.dao.IndexDao;
import com.fjnu.client.entity.vo.*;
import com.fjnu.client.service.Index.Interface.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {


    @Autowired
    private IndexDao indexDao;


    @Override
    public String verifyPassword(int login,String password) {
        String cpassword =indexDao.getPassword(login);
        if(password.equals(cpassword)){
            return "Pass";
        }
        else
            return "Reject";
    }

    @Override
    public List<IndexFarmlandVO> getFarmlandInfo(int login,String direction) {
        return indexDao.getIndexFarmland(login,direction);
    }

    @Override
    public List<ProductionIndexVO> getProductionIndex(int login) {
        return indexDao.getProductionIndex(login);
    }

    @Override
    public List<CropPlantingVO> getCropPlanting(int login, int status) {
        return indexDao.getCropPlanting(login,status);
    }

    @Override
    public List<ConsumptionIndexVO> getConsumptionIndex() {
        return indexDao.getConsumptionIndex();
    }

    @Override
    public List<RemindCountVO> getRemindCount(int login) {
        return indexDao.getRemindCount(login);
    }

    @Override
    public List<RemindDetailVO> getRemindDetail(int login, int type) {
        return indexDao.getRemindDetail(login,type);
    }

    @Override
    public List<ConsumptionIndexMonthVO> getConsumptionIndexMonth() {
        return indexDao.getConsumptionIndexMonth();
    }
}