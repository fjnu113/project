package com.fjnu.client.service.PlantManage.Interface;

import com.baomidou.mybatisplus.service.IService;
import com.fjnu.client.entity.po.PlantManagePO;
import com.fjnu.client.entity.vo.BatchDetailVO;
import com.fjnu.client.entity.vo.HistoryBatchDetailVO;
import com.fjnu.client.entity.vo.PlantBatchVO;

import java.util.List;

public interface PlantManageService extends IService<PlantManagePO>{

    public List<PlantBatchVO> getPlantBatch(int login,int status);

    public List<BatchDetailVO> getPresentPlantBatchDetail(String batchNum);

    public List<HistoryBatchDetailVO> getHistoryPlantBatchDetail(String batchNum);

}
