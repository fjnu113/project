package com.fjnu.client.service.PlantManage;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fjnu.client.dao.PlantManageDao;
import com.fjnu.client.entity.po.PlantManagePO;
import com.fjnu.client.entity.vo.BatchDetailVO;
import com.fjnu.client.entity.vo.HistoryBatchDetailVO;
import com.fjnu.client.entity.vo.PlantBatchVO;
import com.fjnu.client.service.PlantManage.Interface.PlantManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public  class PlantManageServiceImpl extends ServiceImpl<PlantManageDao,PlantManagePO> implements PlantManageService {

    @Autowired
    private PlantManageDao plantManageDao;

    @Override
    public List<PlantBatchVO> getPlantBatch(int login, int status) {
        return plantManageDao.getPlantBatchMain(login,status);
    }

    @Override
    public List<BatchDetailVO> getPresentPlantBatchDetail(String batchNum) {
        return plantManageDao.getPresentPlantBatchDetail(batchNum);
    }

    @Override
    public List<HistoryBatchDetailVO> getHistoryPlantBatchDetail(String batchNum) {
        return plantManageDao.getHistoryPlantBatchDetail(batchNum);
    }
}