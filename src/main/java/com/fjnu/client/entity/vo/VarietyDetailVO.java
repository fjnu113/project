package com.fjnu.client.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

@Data
public class VarietyDetailVO {


    private String varietyName;

    private String describetion;

    private String suitableGrowthTime;

    private String cycleLength;

    private String perMu;

    private String nutritionalValue;

    private  String plantHabit;

    private String plantingMethod;

    private String plantingConsumption;

}