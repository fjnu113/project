package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class ConsumptionIndexMonthVO {

    private String perMonth;

    private String unit;

    private String ave;

}