package com.fjnu.client.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

@Data
public class IndexFarmlandVO {

    private String farmlandName;

    private String farmerName;

    private String varietyName;

    private String growthStatus;

    private String farmlandId;

    private String farmId;


}