package com.fjnu.client.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

@Data
public class HistoryBatchDetailVO {
    private String batchNum;

    private String varietyName;

    private String farmlandName;

    private String starTime;

    private String endTime;

}