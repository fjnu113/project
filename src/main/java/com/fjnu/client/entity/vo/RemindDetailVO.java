package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class RemindDetailVO {

    private String id;

    private String farmId;

    private String farmlandId;

    private String title;

    private String content;

    private String remindTime;

    private String readingStatus;

    private String readingTime;

    private String remindPeople;

    private String type;

    private String name;
}