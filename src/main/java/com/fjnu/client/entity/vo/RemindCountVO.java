package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class RemindCountVO {

    private String type;

    private String count;

}