package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class ExpertLibraryVO {

    private String name;

    private String describetion;

}