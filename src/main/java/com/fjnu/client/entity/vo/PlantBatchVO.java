package com.fjnu.client.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

@Data
public class PlantBatchVO {


    private String batchNum;

    private String planId;

    private String planName;

    private String startTime;

    private String endTime;

    private String type;

}