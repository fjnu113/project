package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class CropPlantingVO {

    private String varietyName;

    private String farmlandName;

    private String count;
}