package com.fjnu.client.entity.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import lombok.Data;

@Data
public class BatchDetailVO {


    private String farmlandNum;

    private String type;

    private String farmerName;

    private String area;

    private String deepth;

    private String varietyName;

    private String batchStatus;

    private String batchNum;

    private String starTime;

    private String endTime;

    private String plantCount;

    private String growthStage;

    private String describetion;

    private String batchCount;

    private String batchSum;


}