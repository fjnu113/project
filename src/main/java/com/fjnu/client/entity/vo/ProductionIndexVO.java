package com.fjnu.client.entity.vo;

import lombok.Data;

@Data
public class ProductionIndexVO {

    private String batchCount;

    private String harvestCount;

    private String output;

}