package com.fjnu.client.entity.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Date;

@Data
public class FarmlandVO {

    private String farmlandId;

    private String farmlandNum;

    private String farmId;

    private String farmlandName;

    private String farmlandType;

    private String farmlandArea;

    private String farmlandDeepth;

    private String plantName;

    private String batchNum;

    private Date startTime;

    private String plantCount;


    private String growthStage;

    private String batchStatus;

    private String data;

    private String costName;

    private String describetion;

    private String costConsumption;

    private String costUnit;


    private String farmerId;


    private String farmerName;


    private String batchCount;

    private String harvestSum;

}