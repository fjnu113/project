package com.fjnu.client.entity.po;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;


@Data
@TableName("iwea_variety_type")
public class VarietyTypePO {

    private Integer id;         //ID
    private String name;         //名称

}