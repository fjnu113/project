package com.fjnu.client.entity.po;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;


@Data
@TableName("iwea_variety")
public class VarietyPO {

    private Integer id;         //ID
    private String name;         //名称
    private String describetion;         //描述
    private String suitable_growth_time;         //适宜生长时间
    private Integer type;         //品类
    private String image;         //配图

}