package com.fjnu.client.entity.po;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Map;

@Data
@TableName("fog_hardware_data")
public class HardwareDataPO {

    private Integer id;         //ID
    private Integer device_id;         //设备id
    private String data_time;         //数据时间
    private Map<String,Object> data;         //数据内容

}