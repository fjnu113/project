package com.fjnu.client.entity.po;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_plant_batch")
public class PlantBatchPO {

    private Integer id;         //ID
    private String num;         //编码
    private Integer farm_id;         //农场ID
    private Integer variety_id;         //植物品种ID
    private String start_time;         //种植开始时间
    private String end_time;         //种植结束时间
    private Integer plant_count;         //种植数量
    private Integer existing_count;         //现存数量
    private Integer status;         //状态: 0正常，1结束
    private Integer plan_id;         //种植计划

}