package com.fjnu.client.entity.po;


import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_growth_cycle_stage")
public class GrowthCycleStagePO {

    private Integer id;         //ID
    private String name;         //名称
    private Integer stage_length;         //阶段长度(天)
    private String feature;         //特征
    private Integer cycle;         //所属生长周期
    private Integer beggin_stage;         //开始阶段(0否，1是)

}