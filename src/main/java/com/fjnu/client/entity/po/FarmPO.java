package com.fjnu.client.entity.po;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_farm")
public class FarmPO {

    private Integer id;         //ID
    private String name;         //农场名称
    private String city;         //所属城市
    private String address;         //详细地址
    private String logo;         //农场logo
    private Integer area;         //农田总面积

}