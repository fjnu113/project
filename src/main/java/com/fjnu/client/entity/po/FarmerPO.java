package com.fjnu.client.entity.po;

import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

import java.util.Map;

@Data
@TableName("fog_hardware_data")
public class FarmerPO {

    private Integer id;      //ID
    private String  name;     //姓名
    private Integer gender;    //性别（1.男，2女）
    private Integer  level;    //等级
    private Integer exp;    //经验值
    private String introduction;    //简介
    private Integer login;    //账号
    private Map<String,Object> title;    //称号


}