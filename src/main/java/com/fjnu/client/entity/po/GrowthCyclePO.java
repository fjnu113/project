package com.fjnu.client.entity.po;


import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_growth_cycle")
public class GrowthCyclePO {
    private Integer id;         //ID
    private Integer variety_id;         //植物品种ID
    private String name;         //名称
    private Integer cycle_length;         //周期长度(天)
    private String feature;         //特征

}