package com.fjnu.client.entity.po;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("iwea_plant_history")
public class PlantHistoryPO {

    private Integer id;         //ID
    private Integer plant_unit_id;         //种植单元ID
    private Integer farmland_id;         //农田ID
    private Integer farm_id;         //农场ID
    private Integer batch_id;         //批次ID
    private String start_time;         //种植开始时间
    private String end_time;         //种植结束时间
    private Integer farmer;         //负责人

}