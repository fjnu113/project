package com.fjnu.client.entity.po;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Data;

@Data
@TableName("fog_hardware_data")
public class HarvestPO {

    private Integer id;         //ID
    private String name;         //名称
    private String type;         //类型
    private Integer count;         //数量
    private String unit;         //单位
    private String harvest_time;         //收获时间
    private Integer plant_unit_id;         //种植单元ID
    private Integer farmland_id;         //农田ID
    private Integer farm_id;         //农场ID
    private Integer batch_id;         //批次ID

}


