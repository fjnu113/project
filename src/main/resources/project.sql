/*
 Navicat Premium Data Transfer

 Source Server         : zhkj
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : project

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 11/12/2019 16:39:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fog_device
-- ----------------------------
DROP TABLE IF EXISTS `fog_device`;
CREATE TABLE `fog_device`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `num` char(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编号',
  `device_product` bigint(20) NOT NULL COMMENT '设备产品',
  `usage_type` json NOT NULL COMMENT '设备用途类型',
  `area` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '安装区域',
  `farmland_id` bigint(20) NOT NULL COMMENT '农田ID',
  `farm_id` bigint(20) NOT NULL COMMENT '农场ID',
  `technology` tinyint(4) NULL DEFAULT NULL COMMENT '设备工艺（0根灌，1滴灌）',
  `parameter` json NULL COMMENT '设备参数',
  `protocol` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备协议',
  `work_status` int(11) NULL DEFAULT NULL COMMENT '工作状态: 0 关闭  1 运行  2  断开连接',
  `host` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设备地址',
  `dtu_device` bigint(20) NOT NULL DEFAULT 0 COMMENT 'DTU设备',
  `type` tinyint(4) NOT NULL COMMENT '产品类型(1.采集器2.控制器，3.DTU)',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `Index_DEVICE_NUM`(`num`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '硬件设备' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fog_device
-- ----------------------------
INSERT INTO `fog_device` VALUES (1, 'DEVICE001', 1, '{}', '100', 1, 1, NULL, NULL, NULL, 1, NULL, 0, 1);

-- ----------------------------
-- Table structure for fog_hardware_data
-- ----------------------------
DROP TABLE IF EXISTS `fog_hardware_data`;
CREATE TABLE `fog_hardware_data`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `device_id` bigint(20) NOT NULL COMMENT '设备id',
  `data_time` datetime(0) NOT NULL COMMENT '数据时间',
  `data` json NOT NULL COMMENT '数据内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '设备数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fog_hardware_data
-- ----------------------------
INSERT INTO `fog_hardware_data` VALUES (1, 1, '2019-11-24 11:13:18', '{\"EC\": 0.55, \"PH\": 7.0, \"temperature\": 25.5, \"Soilmoisture\": 0.218}');

-- ----------------------------
-- Table structure for iwea_cost
-- ----------------------------
DROP TABLE IF EXISTS `iwea_cost`;
CREATE TABLE `iwea_cost`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `type` bigint(20) NOT NULL COMMENT '类型',
  `farmland_id` bigint(20) NOT NULL COMMENT '农田ID',
  `farm_id` bigint(20) NOT NULL COMMENT '农场ID',
  `batch_id` bigint(20) NOT NULL COMMENT '批次ID',
  `cost_time` datetime(0) NOT NULL COMMENT '消耗时间',
  `time_interval` bigint(20) NOT NULL COMMENT '记录间隔',
  `consumption` int(11) NOT NULL COMMENT '耗量',
  `unit` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '单位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '能耗物质' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_cost
-- ----------------------------
INSERT INTO `iwea_cost` VALUES (1, '水消耗', 1, 1, 1, 1, '2019-11-24 11:19:02', 30, 100, 'L');

-- ----------------------------
-- Table structure for iwea_farm
-- ----------------------------
DROP TABLE IF EXISTS `iwea_farm`;
CREATE TABLE `iwea_farm`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '农场名称',
  `city` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '所属城市',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '详细地址',
  `logo` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '农场logo',
  `area` int(11) NULL DEFAULT NULL COMMENT '农田总面积',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '农场' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_farm
-- ----------------------------
INSERT INTO `iwea_farm` VALUES (1, '智恒试验田', '福州', '智恒科技', NULL, NULL);

-- ----------------------------
-- Table structure for iwea_farmer
-- ----------------------------
DROP TABLE IF EXISTS `iwea_farmer`;
CREATE TABLE `iwea_farmer`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `gender` tinyint(4) NOT NULL DEFAULT 1 COMMENT '性别（1.男，2女）',
  `level` tinyint(4) NOT NULL DEFAULT 1 COMMENT '等级',
  `exp` bigint(20) NOT NULL DEFAULT 0 COMMENT '经验值',
  `introduction` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '简介',
  `login` bigint(20) NOT NULL COMMENT '账号',
  `title` json NOT NULL COMMENT '称号',
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '领养人' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_farmer
-- ----------------------------
INSERT INTO `iwea_farmer` VALUES (1, '小施', 1, 1, 0, '无', 123, '{}', '123456');

-- ----------------------------
-- Table structure for iwea_farmland
-- ----------------------------
DROP TABLE IF EXISTS `iwea_farmland`;
CREATE TABLE `iwea_farmland`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `num` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编码',
  `area` int(11) NULL DEFAULT NULL COMMENT '农田面积',
  `position` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '农田位置',
  `ic` int(11) NULL DEFAULT 1000 COMMENT '光照系数(*1000)',
  `farmer` bigint(20) NOT NULL COMMENT '负责人',
  `status` int(11) NOT NULL COMMENT '土壤状态（0空闲，1,在种，2土壤改良，3种植准备）',
  `deepth` int(11) NULL DEFAULT NULL COMMENT '土壤深度',
  `farm_id` bigint(20) NOT NULL COMMENT '所属农场',
  `type` int(11) NOT NULL COMMENT '类型（1.精作田，2.粗作田）',
  `duty_type` smallint(6) NOT NULL COMMENT '值守类型（0.手动，1自动）',
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '农田名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 144 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '农田' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_farmland
-- ----------------------------
INSERT INTO `iwea_farmland` VALUES (1, 'N1_01', 382, 'N1_01', 1000, 1, 0, NULL, 1, 1, 0, '小施实验田');
INSERT INTO `iwea_farmland` VALUES (2, 'N1_02', 466, 'N1_02', 1000, 1, 0, NULL, 2, 1, 0, '小劳实验田');
INSERT INTO `iwea_farmland` VALUES (3, 'N1_03', 466, 'N1_03', 1000, 1, 0, NULL, 1, 2, 0, '小顾实验田');
INSERT INTO `iwea_farmland` VALUES (4, 'N1_04', 466, 'N1_04', 1000, 1, 0, NULL, 2, 1, 0, '小梁实验田');
INSERT INTO `iwea_farmland` VALUES (5, 'N1_05', 166, 'N1_05', 1000, 1, 0, NULL, 1, 2, 0, '小陈实验田');
INSERT INTO `iwea_farmland` VALUES (6, 'N1_06', 466, 'N1_06', 1000, 1, 0, NULL, 2, 1, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (7, 'N1_07', 477, 'N1_07', 1000, 1, 0, NULL, 1, 1, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (8, 'N1_08', 392, 'N1_08', 1000, 1, 0, NULL, 2, 1, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (9, 'N1_09', 466, 'N1_09', 1000, 1, 0, NULL, 1, 1, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (10, 'N1_10', 380, 'N1_10', 1000, 1, 0, NULL, 2, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (11, 'N1_11', 466, 'N1_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (12, 'N1_12', 392, 'N1_12', 1000, 1, 0, NULL, 2, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (13, 'N1_13', 477, 'N1_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (14, 'N1_14', 488, 'N1_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (15, 'N1_15', 455, 'N1_15', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (16, 'N1_16', 488, 'N1_16', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (17, 'N1_17', 466, 'N1_17', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (18, 'N1_18', 488, 'N1_18', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (19, 'N1_19', 392, 'N1_19', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (20, 'N2_01', 174, 'N2_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (21, 'N2_02', 378, 'N2_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (22, 'N2_03', 444, 'N2_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (23, 'N2_04', 550, 'N2_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (24, 'N2_05', 639, 'N2_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (25, 'N2_06', 730, 'N2_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (26, 'N2_07', 647, 'N2_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (27, 'N2_08', 846, 'N2_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (28, 'N2_09', 683, 'N2_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (29, 'N2_10', 855, 'N2_10', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (30, 'N2_11', 632, 'N2_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (31, 'N2_12', 787, 'N2_12', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (32, 'N2_13', 842, 'N2_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (33, 'N2_14', 810, 'N2_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (34, 'N2_15', 767, 'N2_15', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (35, 'N2_16', 789, 'N2_16', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (36, 'N2_17', 635, 'N2_17', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (37, 'N2_18', 404, 'N2_18', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (38, 'N2_19', 425, 'N2_19', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (39, 'N2_20', 342, 'N2_20', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (40, 'N2_21', 200, 'N2_21', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (41, 'E1_01', 102, 'E1_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (42, 'E1_02', 235, 'E1_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (43, 'E1_03', 240, 'E1_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (44, 'E1_04', 240, 'E1_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (45, 'E1_05', 235, 'E1_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (46, 'E1_06', 205, 'E1_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (47, 'E1_07', 235, 'E1_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (48, 'E1_08', 205, 'E1_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (49, 'E1_09', 235, 'E1_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (50, 'E1_10', 198, 'E1_10', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (51, 'E1_11', 240, 'E1_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (52, 'E1_12', 235, 'E1_12', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (53, 'E1_13', 235, 'E1_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (54, 'E1_14', 375, 'E1_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (55, 'E2_01', 767, 'E2_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (56, 'E2_02', 403, 'E2_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (57, 'E2_03', 438, 'E2_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (58, 'E2_04', 467, 'E2_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (59, 'E2_05', 590, 'E2_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (60, 'E2_06', 944, 'E2_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (61, 'E2_07', 1035, 'E2_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (62, 'E2_08', 1179, 'E2_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (63, 'E2_09', 1032, 'E2_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (64, 'E2_10', 1039, 'E2_10', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (65, 'E2_11', 2008, 'E2_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (66, 'E2_12', 1426, 'E2_12', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (67, 'E2_13', 826, 'E2_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (68, 'E2_14', 910, 'E2_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (69, 'E2_15', 848, 'E2_15', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (70, 'E2_16', 420, 'E2_16', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (71, 'E2_17', 960, 'E2_17', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (72, 'S1_01', 175, 'S1_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (73, 'S1_02', 670, 'S1_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (74, 'S1_03', 317, 'S1_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (75, 'S1_04', 749, 'S1_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (76, 'S1_05', 2918, 'S1_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (77, 'S1_06', 1352, 'S1_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (78, 'S1_07', 6792, 'S1_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (79, 'S1_08', 668, 'S1_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (80, 'S1_09', 513, 'S1_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (81, 'S2_01', 1294, 'S2_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (82, 'S2_02', 851, 'S2_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (83, 'S2_03', 788, 'S2_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (84, 'S2_04', 138, 'S2_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (85, 'S2_05', 316, 'S2_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (86, 'S3_01', 1036, 'S3_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (87, 'S3_02', 178, 'S3_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (88, 'S3_03', 139, 'S3_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (89, 'S3_04', 1272, 'S3_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (90, 'S3_05', 948, 'S3_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (91, 'S3_06', 1174, 'S3_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (92, 'S3_07', 148, 'S3_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (93, 'S3_08', 146, 'S3_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (94, 'S3_09', 1017, 'S3_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (95, 'S4_01', 1044, 'S4_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (96, 'S4_02', 1635, 'S4_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (97, 'S4_03', 477, 'S4_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (98, 'S4_04', 1120, 'S4_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (99, 'S4_05', 3075, 'S4_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (100, 'S4_06', 1149, 'S4_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (101, 'S4_07', 3300, 'S4_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (102, 'S4_08', 1135, 'S4_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (103, 'S5_01', 563, 'S5_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (104, 'S5_02', 1604, 'S5_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (105, 'S5_03', 1477, 'S5_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (106, 'S5_04', 1586, 'S5_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (107, 'S6_01', 1587, 'S6_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (108, 'S6_02', 1508, 'S6_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (109, 'S6_03', 1571, 'S6_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (110, 'S6_04', 738, 'S6_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (111, 'W1_01', 378, 'W1_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (112, 'W1_02', 235, 'W1_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (113, 'W1_03', 241, 'W1_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (114, 'W1_04', 198, 'W1_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (115, 'W1_05', 235, 'W1_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (116, 'W1_06', 235, 'W1_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (117, 'W1_07', 235, 'W1_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (118, 'W1_08', 235, 'W1_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (119, 'W1_09', 241, 'W1_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (120, 'W1_10', 241, 'W1_10', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (121, 'W1_11', 241, 'W1_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (122, 'W1_12', 235, 'W1_12', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (123, 'W1_13', 235, 'W1_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (124, 'W1_14', 102, 'W1_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (125, 'W2_01', 525, 'W2_01', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (126, 'W2_02', 399, 'W2_02', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (127, 'W2_03', 498, 'W2_03', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (128, 'W2_04', 848, 'W2_04', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (129, 'W2_05', 910, 'W2_05', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (130, 'W2_06', 959, 'W2_06', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (131, 'W2_07', 1426, 'W2_07', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (132, 'W2_08', 2007, 'W2_08', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (133, 'W2_09', 1039, 'W2_09', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (134, 'W2_10', 1136, 'W2_10', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (135, 'W2_11', 1281, 'W2_11', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (136, 'W2_12', 1035, 'W2_12', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (137, 'W2_13', 944, 'W2_13', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (138, 'W2_14', 891, 'W2_14', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (139, 'W2_15', 551, 'W2_15', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (140, 'W2_16', 832, 'W2_16', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (141, 'W2_17', 742, 'W2_17', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (142, 'W2_18', 637, 'W2_18', 1000, 1, 0, NULL, 1, 2, 0, NULL);
INSERT INTO `iwea_farmland` VALUES (143, 'W2_19', 377, 'W2_19', 1000, 1, 0, NULL, 1, 2, 0, NULL);

-- ----------------------------
-- Table structure for iwea_growth_cycle
-- ----------------------------
DROP TABLE IF EXISTS `iwea_growth_cycle`;
CREATE TABLE `iwea_growth_cycle`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `variety_id` bigint(20) NOT NULL COMMENT '植物品种ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `cycle_length` int(11) NULL DEFAULT NULL COMMENT '周期长度(天)',
  `feature` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '特征',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '生长周期' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for iwea_growth_cycle_stage
-- ----------------------------
DROP TABLE IF EXISTS `iwea_growth_cycle_stage`;
CREATE TABLE `iwea_growth_cycle_stage`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `stage_length` int(11) NULL DEFAULT NULL COMMENT '阶段长度(天)',
  `feature` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '特征',
  `cycle` bigint(20) NOT NULL COMMENT '所属生长周期',
  `beggin_stage` tinyint(4) NOT NULL DEFAULT 0 COMMENT '开始阶段(0否，1是)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '生长周期阶段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for iwea_harvest
-- ----------------------------
DROP TABLE IF EXISTS `iwea_harvest`;
CREATE TABLE `iwea_harvest`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `type` char(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型',
  `count` int(11) NOT NULL COMMENT '数量',
  `unit` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '单位',
  `harvest_time` datetime(0) NOT NULL COMMENT '收获时间',
  `farmland_id` bigint(20) NOT NULL COMMENT '农田ID',
  `farm_id` bigint(20) NOT NULL COMMENT '农场ID',
  `batch_id` bigint(20) NOT NULL COMMENT '批次ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '收成' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_harvest
-- ----------------------------
INSERT INTO `iwea_harvest` VALUES (1, '白菜收成', '秋收', 500, 'T', '2019-11-24 11:27:29', 1, 1, 1);

-- ----------------------------
-- Table structure for iwea_plant_batch
-- ----------------------------
DROP TABLE IF EXISTS `iwea_plant_batch`;
CREATE TABLE `iwea_plant_batch`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `num` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '编码',
  `farm_id` bigint(20) NOT NULL COMMENT '农场ID',
  `variety_id` bigint(20) NOT NULL COMMENT '植物品种ID',
  `start_time` datetime(0) NOT NULL COMMENT '种植开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '种植结束时间',
  `plant_count` int(11) NOT NULL COMMENT '种植数量',
  `existing_count` int(11) NOT NULL COMMENT '现存数量',
  `status` int(11) NOT NULL COMMENT '状态: 0正常，1结束',
  `plan_id` bigint(20) NULL DEFAULT NULL COMMENT '种植计划',
  `growth_stage` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `growth_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '植物批次' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_plant_batch
-- ----------------------------
INSERT INTO `iwea_plant_batch` VALUES (1, 'BATCH001', 1, 1, '2019-11-22 11:07:33', '2019-11-24 11:07:37', 500, 300, 1, 1, '种植期', '良好');
INSERT INTO `iwea_plant_batch` VALUES (2, 'BATCH002', 1, 2, '2019-12-11 14:55:43', '2019-12-11 14:55:47', 280, 260, 2, 2, '生长期', '良好');

-- ----------------------------
-- Table structure for iwea_plant_history
-- ----------------------------
DROP TABLE IF EXISTS `iwea_plant_history`;
CREATE TABLE `iwea_plant_history`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `farmland_id` bigint(20) NOT NULL COMMENT '农田ID',
  `farm_id` bigint(20) NOT NULL COMMENT '农场ID',
  `batch_id` bigint(20) NOT NULL COMMENT '批次ID',
  `start_time` datetime(0) NOT NULL COMMENT '种植开始时间',
  `end_time` datetime(0) NOT NULL COMMENT '种植结束时间',
  `farmer` bigint(20) NOT NULL COMMENT '负责人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '农田种植记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_plant_history
-- ----------------------------
INSERT INTO `iwea_plant_history` VALUES (1, 1, 1, 1, '2019-11-24 11:06:41', '2019-11-24 11:06:49', 1);
INSERT INTO `iwea_plant_history` VALUES (2, 2, 1, 2, '2019-12-11 16:36:24', '2019-12-11 16:36:28', 1);

-- ----------------------------
-- Table structure for iwea_plant_method
-- ----------------------------
DROP TABLE IF EXISTS `iwea_plant_method`;
CREATE TABLE `iwea_plant_method`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `suitable_growth_time` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '适宜生长时间',
  `per_mu` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '亩产',
  `nutritional_value` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '营养价值',
  `habit` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '习性',
  `planting_method` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '栽培方法',
  `planting_consumption` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '栽培消耗',
  `cycle_length` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '周期长度',
  `variety_id` bigint(20) NOT NULL COMMENT '植物品种ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_plant_method
-- ----------------------------
INSERT INTO `iwea_plant_method` VALUES (1, '春夏秋冬', '3000-3500kg', '甘蓝营养元素很丰富如优质蛋白质，纤维素，矿物质，维生素等等，吃紫甘蓝可以补充营养，强身健体。', '甘蓝喜温和湿润、充足的光照。较耐寒，也有适应高温的能力。生长适温15-20℃。肉质茎膨大期如遇30℃以上高温肉质易纤维化。对土壤的选择不很严格，但宜于腐殖质丰富的粘壤土或砂壤土中种植。甘蓝的幼苗必须在0-10℃通过春化，然后在长日照和适温下抽薹、开花、结果。', '定植 选择苗龄40天左右，6-8片真叶壮苗，在阴天或晴天傍晚进行定植，行距55厘米，株距35-40厘米，亩栽3000-3500株。 水肥 早春地膜栽培定植缓苗后，因春季温度低，一般小水，应适当控制浇水以提高地温，不可大水漫灌，随水施尿素　15公斤左右。若有寒流天气，可提前施硫酸铵每亩15-22公斤，并灌水可增强植株抗寒能力。随着气温升高，进入莲座后期包心前期，加大肥水，施复合肥　20公斤左右，或每亩施尿素25-30公斤，并结合施一定草木灰。莲座末期适当控制浇水，及时中耕除草。移栽较晚的农户，雨后注意排水，防止田间积水造成烂根和叶球腐烂。 采收 当叶球达到紧实时采收，在收割时保留2片外叶（莲座叶）以保护叶球，做到表面干净、无虫害、无裂球。同时根据保鲜加工出口的标准要求，按照叶球的大小进行分级。供应市场的，可视需求情况，及时采收上市。', '1、包菜莲座期追肥：亩施尿素10-11kg，硫酸钾5-6kg，以促进花芽、花蕾分化和花球形成。 2、包菜花球形成初期追肥：亩施尿素13-15kg，硫酸钾6-8kg，以促进花球的快速膨大，防止花茎空心。 3、包菜花球形成中期追肥：亩施尿素10-11kg，硫酸钾5-6kg，以提高花球产量和花球质量。', '50-70天', 1);

-- ----------------------------
-- Table structure for iwea_variety
-- ----------------------------
DROP TABLE IF EXISTS `iwea_variety`;
CREATE TABLE `iwea_variety`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `describetion` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `type` bigint(20) NULL DEFAULT NULL COMMENT '品类',
  `image` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '配图',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '植物品种' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of iwea_variety
-- ----------------------------
INSERT INTO `iwea_variety` VALUES (1, '大白菜', '容易种植，产量高', NULL, NULL);
INSERT INTO `iwea_variety` VALUES (2, '茄子', '紫皮，好吃', NULL, NULL);

-- ----------------------------
-- Table structure for iwea_variety_type
-- ----------------------------
DROP TABLE IF EXISTS `iwea_variety_type`;
CREATE TABLE `iwea_variety_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '种植品类' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
