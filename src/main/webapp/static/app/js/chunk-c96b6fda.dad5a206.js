(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
    ["chunk-c96b6fda"], {
        "166a": function(t, e, i) {},
        1800: function(t, e, i) {
            "use strict";
            var n = i("2b0e");
            e["a"] = n["a"].extend({
                name: "v-list-item-action",
                functional: !0,
                render: function(t, e) {
                    var i = e.data,
                        n = e.children,
                        s = void 0 === n ? [] : n;
                    i.staticClass = i.staticClass ? "v-list-item__action ".concat(i.staticClass) : "v-list-item__action";
                    var r = s.filter(function(t) {
                        return !1 === t.isComment && " " !== t.text
                    });
                    return r.length > 1 && (i.staticClass += " v-list-item__action--stack"), t("div", i, s)
                }
            })
        },
        "20d6": function(t, e, i) {
            "use strict";
            var n = i("5ca1"),
                s = i("0a49")(6),
                r = "findIndex",
                a = !0;
            r in [] && Array(1)[r](function() {
                a = !1
            }), n(n.P + n.F * a, "Array", {
                findIndex: function(t) {
                    return s(this, t, arguments.length > 1 ? arguments[1] : void 0)
                }
            }), i("9c6c")(r)
        },
        3408: function(t, e, i) {},
        "34c3": function(t, e, i) {
            "use strict";
            var n = i("2b0e");
            e["a"] = n["a"].extend({
                name: "v-list-item-icon",
                functional: !0,
                render: function(t, e) {
                    var i = e.data,
                        n = e.children;
                    return i.staticClass = "v-list-item__icon ".concat(i.staticClass || "").trim(), t("div", i, n)
                }
            })
        },
        "3ad0": function(t, e, i) {},
        "5d23": function(t, e, i) {
            "use strict";
            var n = i("80d2"),
                s = i("8860"),
                r = (i("8e6e"), i("ac6a"), i("456d"), i("4917"), i("bd86")),
                a = (i("db42"), i("9d26")),
                o = i("da13"),
                c = i("34c3"),
                l = i("9d65"),
                u = i("a9ad"),
                h = i("f2e7"),
                d = i("3206"),
                p = i("5607"),
                f = i("0789"),
                v = i("58df");

            function b(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function m(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? b(i, !0).forEach(function(e) {
                        Object(r["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : b(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            var g = Object(v["a"])(l["a"], u["a"], Object(d["a"])("list"), h["a"]),
                y = g.extend().extend({
                    name: "v-list-group",
                    directives: {
                        ripple: p["a"]
                    },
                    props: {
                        activeClass: {
                            type: String,
                            default: ""
                        },
                        appendIcon: {
                            type: String,
                            default: "$vuetify.icons.expand"
                        },
                        color: {
                            type: String,
                            default: "primary"
                        },
                        disabled: Boolean,
                        group: String,
                        noAction: Boolean,
                        prependIcon: String,
                        ripple: {
                            type: [Boolean, Object],
                            default: !0
                        },
                        subGroup: Boolean
                    },
                    computed: {
                        classes: function() {
                            return {
                                "v-list-group--active": this.isActive,
                                "v-list-group--disabled": this.disabled,
                                "v-list-group--no-action": this.noAction,
                                "v-list-group--sub-group": this.subGroup
                            }
                        }
                    },
                    watch: {
                        isActive: function(t) {
                            !this.subGroup && t && this.list && this.list.listClick(this._uid)
                        },
                        $route: "onRouteChange"
                    },
                    created: function() {
                        this.list && this.list.register(this), this.group && this.$route && null == this.value && (this.isActive = this.matchRoute(this.$route.path))
                    },
                    beforeDestroy: function() {
                        this.list && this.list.unregister(this)
                    },
                    methods: {
                        click: function(t) {
                            var e = this;
                            this.disabled || (this.isBooted = !0, this.$emit("click", t), this.$nextTick(function() {
                                return e.isActive = !e.isActive
                            }))
                        },
                        genIcon: function(t) {
                            return this.$createElement(a["a"], t)
                        },
                        genAppendIcon: function() {
                            var t = !this.subGroup && this.appendIcon;
                            return t || this.$slots.appendIcon ? this.$createElement(c["a"], {
                                staticClass: "v-list-group__header__append-icon"
                            }, [this.$slots.appendIcon || this.genIcon(t)]) : null
                        },
                        genHeader: function() {
                            return this.$createElement(o["a"], {
                                staticClass: "v-list-group__header",
                                attrs: {
                                    "aria-expanded": String(this.isActive),
                                    role: "button"
                                },
                                class: Object(r["a"])({}, this.activeClass, this.isActive),
                                props: {
                                    inputValue: this.isActive
                                },
                                directives: [{
                                    name: "ripple",
                                    value: this.ripple
                                }],
                                on: m({}, this.$listeners, {
                                    click: this.click
                                })
                            }, [this.genPrependIcon(), this.$slots.activator, this.genAppendIcon()])
                        },
                        genItems: function() {
                            return this.$createElement("div", {
                                staticClass: "v-list-group__items",
                                directives: [{
                                    name: "show",
                                    value: this.isActive
                                }]
                            }, this.showLazyContent([this.$createElement("div", this.$slots.default)]))
                        },
                        genPrependIcon: function() {
                            var t = this.prependIcon ? this.prependIcon : !!this.subGroup && "$vuetify.icons.subgroup";
                            return t || this.$slots.prependIcon ? this.$createElement(c["a"], {
                                staticClass: "v-list-group__header__prepend-icon"
                            }, [this.$slots.prependIcon || this.genIcon(t)]) : null
                        },
                        onRouteChange: function(t) {
                            if (this.group) {
                                var e = this.matchRoute(t.path);
                                e && this.isActive !== e && this.list && this.list.listClick(this._uid), this.isActive = e
                            }
                        },
                        toggle: function(t) {
                            var e = this,
                                i = this._uid === t;
                            i && (this.isBooted = !0), this.$nextTick(function() {
                                return e.isActive = i
                            })
                        },
                        matchRoute: function(t) {
                            return null !== t.match(this.group)
                        }
                    },
                    render: function(t) {
                        return t("div", this.setTextColor(this.isActive && this.color, {
                            staticClass: "v-list-group",
                            class: this.classes
                        }), [this.genHeader(), t(f["a"], [this.genItems()])])
                    }
                }),
                O = (i("899c"), i("604c"));

            function j(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function w(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? j(i, !0).forEach(function(e) {
                        Object(r["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : j(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            var P = Object(v["a"])(O["a"], u["a"]).extend({
                    name: "v-list-item-group",
                    provide: function() {
                        return {
                            isInGroup: !0,
                            listItemGroup: this
                        }
                    },
                    computed: {
                        classes: function() {
                            return w({}, O["a"].options.computed.classes.call(this), {
                                "v-list-item-group": !0
                            })
                        }
                    },
                    methods: {
                        genData: function() {
                            return this.setTextColor(this.color, w({}, O["a"].options.methods.genData.call(this), {
                                attrs: {
                                    role: "listbox"
                                }
                            }))
                        }
                    }
                }),
                I = i("1800"),
                C = i("8270");
            i.d(e, "a", function() {
                return x
            }), i.d(e, "b", function() {
                return k
            }), i.d(e, "d", function() {
                return $
            }), i.d(e, "c", function() {
                return _
            });
            var x = Object(n["h"])("v-list-item__action-text", "span"),
                k = Object(n["h"])("v-list-item__content", "div"),
                $ = Object(n["h"])("v-list-item__title", "div"),
                _ = Object(n["h"])("v-list-item__subtitle", "div");
            s["a"], o["a"], I["a"], C["a"], c["a"]
        },
        "604c": function(t, e, i) {
            "use strict";
            i.d(e, "a", function() {
                return u
            });
            i("8e6e"), i("456d"), i("20d6"), i("7514"), i("ac6a"), i("6762"), i("2fdb");
            var n = i("bd86"),
                s = (i("c5f6"), i("166a"), i("a452")),
                r = i("7560"),
                a = i("58df"),
                o = i("d9bd");

            function c(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function l(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? c(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : c(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            var u = Object(a["a"])(s["a"], r["a"]).extend({
                name: "base-item-group",
                props: {
                    activeClass: {
                        type: String,
                        default: "v-item--active"
                    },
                    mandatory: Boolean,
                    max: {
                        type: [Number, String],
                        default: null
                    },
                    multiple: Boolean
                },
                data: function() {
                    return {
                        internalLazyValue: void 0 !== this.value ? this.value : this.multiple ? [] : void 0,
                        items: []
                    }
                },
                computed: {
                    classes: function() {
                        return l({
                            "v-item-group": !0
                        }, this.themeClasses)
                    },
                    selectedIndex: function() {
                        return this.selectedItem && this.items.indexOf(this.selectedItem) || -1
                    },
                    selectedItem: function() {
                        if (!this.multiple) return this.selectedItems[0]
                    },
                    selectedItems: function() {
                        var t = this;
                        return this.items.filter(function(e, i) {
                            return t.toggleMethod(t.getValue(e, i))
                        })
                    },
                    selectedValues: function() {
                        return null == this.internalValue ? [] : Array.isArray(this.internalValue) ? this.internalValue : [this.internalValue]
                    },
                    toggleMethod: function() {
                        var t = this;
                        if (!this.multiple) return function(e) {
                            return t.internalValue === e
                        };
                        var e = this.internalValue;
                        return Array.isArray(e) ? function(t) {
                            return e.includes(t)
                        } : function() {
                            return !1
                        }
                    }
                },
                watch: {
                    internalValue: function() {
                        this.$nextTick(this.updateItemsState)
                    }
                },
                created: function() {
                    this.multiple && !Array.isArray(this.internalValue) && Object(o["c"])("Model must be bound to an array if the multiple property is true.", this)
                },
                methods: {
                    genData: function() {
                        return {
                            class: this.classes
                        }
                    },
                    getValue: function(t, e) {
                        return null == t.value || "" === t.value ? e : t.value
                    },
                    onClick: function(t) {
                        this.updateInternalValue(this.getValue(t, this.items.indexOf(t)))
                    },
                    register: function(t) {
                        var e = this,
                            i = this.items.push(t) - 1;
                        t.$on("change", function() {
                            return e.onClick(t)
                        }), this.mandatory && null == this.internalLazyValue && this.updateMandatory(), this.updateItem(t, i)
                    },
                    unregister: function(t) {
                        if (!this._isDestroyed) {
                            var e = this.items.indexOf(t),
                                i = this.getValue(t, e);
                            this.items.splice(e, 1);
                            var n = this.selectedValues.indexOf(i);
                            if (!(n < 0)) {
                                if (!this.mandatory) return this.updateInternalValue(i);
                                this.multiple && Array.isArray(this.internalValue) ? this.internalValue = this.internalValue.filter(function(t) {
                                    return t !== i
                                }) : this.internalValue = void 0, this.selectedItems.length || this.updateMandatory(!0)
                            }
                        }
                    },
                    updateItem: function(t, e) {
                        var i = this.getValue(t, e);
                        t.isActive = this.toggleMethod(i)
                    },
                    updateItemsState: function() {
                        if (this.mandatory && !this.selectedItems.length) return this.updateMandatory();
                        this.items.forEach(this.updateItem)
                    },
                    updateInternalValue: function(t) {
                        this.multiple ? this.updateMultiple(t) : this.updateSingle(t)
                    },
                    updateMandatory: function(t) {
                        if (this.items.length) {
                            var e = this.items.slice();
                            t && e.reverse();
                            var i = e.find(function(t) {
                                return !t.disabled
                            });
                            if (i) {
                                var n = this.items.indexOf(i);
                                this.updateInternalValue(this.getValue(i, n))
                            }
                        }
                    },
                    updateMultiple: function(t) {
                        var e = Array.isArray(this.internalValue) ? this.internalValue : [],
                            i = e.slice(),
                            n = i.findIndex(function(e) {
                                return e === t
                            });
                        this.mandatory && n > -1 && i.length - 1 < 1 || null != this.max && n < 0 && i.length + 1 > this.max || (n > -1 ? i.splice(n, 1) : i.push(t), this.internalValue = i)
                    },
                    updateSingle: function(t) {
                        var e = t === this.internalValue;
                        this.mandatory && e || (this.internalValue = e ? void 0 : t)
                    }
                },
                render: function(t) {
                    return t("div", this.genData(), this.$slots.default)
                }
            });
            u.extend({
                name: "v-item-group",
                provide: function() {
                    return {
                        itemGroup: this
                    }
                }
            })
        },
        "61d2": function(t, e, i) {},
        "713a": function(t, e, i) {
            "use strict";
            var n = i("8212");
            e["a"] = n["a"]
        },
        8212: function(t, e, i) {
            "use strict";
            i("8e6e"), i("ac6a"), i("456d");
            var n = i("bd86"),
                s = (i("c5f6"), i("3408"), i("a9ad")),
                r = i("24b2"),
                a = i("80d2"),
                o = i("58df");

            function c(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function l(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? c(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : c(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            e["a"] = Object(o["a"])(s["a"], r["a"]).extend({
                name: "v-avatar",
                props: {
                    left: Boolean,
                    right: Boolean,
                    size: {
                        type: [Number, String],
                        default: 48
                    },
                    tile: Boolean
                },
                computed: {
                    classes: function() {
                        return {
                            "v-avatar--left": this.left,
                            "v-avatar--right": this.right,
                            "v-avatar--tile": this.tile
                        }
                    },
                    styles: function() {
                        return l({
                            height: Object(a["e"])(this.size),
                            minWidth: Object(a["e"])(this.size),
                            width: Object(a["e"])(this.size)
                        }, this.measurableStyles)
                    }
                },
                render: function(t) {
                    var e = {
                        staticClass: "v-avatar",
                        class: this.classes,
                        style: this.styles,
                        on: this.$listeners
                    };
                    return t("div", this.setBackgroundColor(this.color, e), this.$slots.default)
                }
            })
        },
        8270: function(t, e, i) {
            "use strict";
            i("8e6e"), i("ac6a"), i("456d");
            var n = i("bd86"),
                s = (i("c5f6"), i("713a"));

            function r(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function a(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? r(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : r(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            e["a"] = s["a"].extend({
                name: "v-list-item-avatar",
                props: {
                    horizontal: Boolean,
                    size: {
                        type: [Number, String],
                        default: 40
                    }
                },
                computed: {
                    classes: function() {
                        return a({
                            "v-list-item__avatar--horizontal": this.horizontal
                        }, s["a"].options.computed.classes.call(this), {
                            "v-avatar--tile": this.tile || this.horizontal
                        })
                    }
                },
                render: function(t) {
                    var e = s["a"].options.render.call(this, t);
                    return e.data = e.data || {}, e.data.staticClass += " v-list-item__avatar", e
                }
            })
        },
        8860: function(t, e, i) {
            "use strict";
            i("8e6e"), i("456d"), i("ac4d"), i("8a81"), i("ac6a"), i("20d6");
            var n = i("bd86"),
                s = (i("3ad0"), i("8dd9"));

            function r(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function a(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? r(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : r(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            e["a"] = s["a"].extend().extend({
                name: "v-list",
                provide: function() {
                    return {
                        isInList: !0,
                        list: this
                    }
                },
                inject: {
                    isInMenu: {
                        default: !1
                    },
                    isInNav: {
                        default: !1
                    }
                },
                props: {
                    dense: Boolean,
                    disabled: Boolean,
                    expand: Boolean,
                    flat: Boolean,
                    nav: Boolean,
                    rounded: Boolean,
                    shaped: Boolean,
                    subheader: Boolean,
                    threeLine: Boolean,
                    tile: {
                        type: Boolean,
                        default: !0
                    },
                    twoLine: Boolean
                },
                data: function() {
                    return {
                        groups: []
                    }
                },
                computed: {
                    classes: function() {
                        return a({}, s["a"].options.computed.classes.call(this), {
                            "v-list--dense": this.dense,
                            "v-list--disabled": this.disabled,
                            "v-list--flat": this.flat,
                            "v-list--nav": this.nav,
                            "v-list--rounded": this.rounded,
                            "v-list--shaped": this.shaped,
                            "v-list--subheader": this.subheader,
                            "v-list--two-line": this.twoLine,
                            "v-list--three-line": this.threeLine
                        })
                    }
                },
                methods: {
                    register: function(t) {
                        this.groups.push(t)
                    },
                    unregister: function(t) {
                        var e = this.groups.findIndex(function(e) {
                            return e._uid === t._uid
                        });
                        e > -1 && this.groups.splice(e, 1)
                    },
                    listClick: function(t) {
                        if (!this.expand) {
                            var e = !0,
                                i = !1,
                                n = void 0;
                            try {
                                for (var s, r = this.groups[Symbol.iterator](); !(e = (s = r.next()).done); e = !0) {
                                    var a = s.value;
                                    a.toggle(t)
                                }
                            } catch (o) {
                                i = !0, n = o
                            } finally {
                                try {
                                    e || null == r.return || r.return()
                                } finally {
                                    if (i) throw n
                                }
                            }
                        }
                    }
                },
                render: function(t) {
                    var e = {
                        staticClass: "v-list",
                        class: this.classes,
                        style: this.styles,
                        attrs: a({
                            role: this.isInNav || this.isInMenu ? void 0 : "list"
                        }, this.$attrs)
                    };
                    return t("div", this.setBackgroundColor(this.color, e), [this.$slots.default])
                }
            })
        },
        "899c": function(t, e, i) {},
        "9d65": function(t, e, i) {
            "use strict";
            var n = i("d9bd"),
                s = i("2b0e");
            e["a"] = s["a"].extend().extend({
                name: "bootable",
                props: {
                    eager: Boolean
                },
                data: function() {
                    return {
                        isBooted: !1
                    }
                },
                computed: {
                    hasContent: function() {
                        return this.isBooted || this.eager || this.isActive
                    }
                },
                watch: {
                    isActive: function() {
                        this.isBooted = !0
                    }
                },
                created: function() {
                    "lazy" in this.$attrs && Object(n["d"])("lazy", this)
                },
                methods: {
                    showLazyContent: function(t) {
                        return this.hasContent ? t : void 0
                    }
                }
            })
        },
        da13: function(t, e, i) {
            "use strict";
            i("8e6e"), i("ac6a"), i("456d");
            var n = i("bd86"),
                s = (i("61d2"), i("a9ad")),
                r = i("1c87"),
                a = i("4e82"),
                o = i("7560"),
                c = i("f2e7"),
                l = i("5607"),
                u = i("80d2"),
                h = i("d9bd"),
                d = i("58df");

            function p(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function f(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? p(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : p(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            var v = Object(d["a"])(s["a"], r["a"], o["a"], Object(a["a"])("listItemGroup"), Object(c["b"])("inputValue"));
            e["a"] = v.extend().extend({
                name: "v-list-item",
                directives: {
                    Ripple: l["a"]
                },
                inheritAttrs: !1,
                inject: {
                    isInGroup: {
                        default: !1
                    },
                    isInList: {
                        default: !1
                    },
                    isInMenu: {
                        default: !1
                    },
                    isInNav: {
                        default: !1
                    }
                },
                props: {
                    activeClass: {
                        type: String,
                        default: function() {
                            return this.listItemGroup ? this.listItemGroup.activeClass : ""
                        }
                    },
                    dense: Boolean,
                    inactive: Boolean,
                    link: Boolean,
                    tag: {
                        type: String,
                        default: "div"
                    },
                    threeLine: Boolean,
                    twoLine: Boolean,
                    value: null
                },
                data: function() {
                    return {
                        proxyClass: "v-list-item--active"
                    }
                },
                computed: {
                    classes: function() {
                        return f({
                            "v-list-item": !0
                        }, r["a"].options.computed.classes.call(this), {
                            "v-list-item--dense": this.dense,
                            "v-list-item--disabled": this.disabled,
                            "v-list-item--link": this.isClickable && !this.inactive,
                            "v-list-item--three-line": this.threeLine,
                            "v-list-item--two-line": this.twoLine
                        }, this.themeClasses)
                    },
                    isClickable: function() {
                        return Boolean(r["a"].options.computed.isClickable.call(this) || this.listItemGroup)
                    }
                },
                created: function() {
                    this.$attrs.hasOwnProperty("avatar") && Object(h["d"])("avatar", this)
                },
                methods: {
                    click: function(t) {
                        t.detail && this.$el.blur(), this.$emit("click", t), this.to || this.toggle()
                    },
                    genAttrs: function() {
                        var t = f({
                            "aria-disabled": !!this.disabled || void 0,
                            tabindex: this.isClickable && !this.disabled ? 0 : -1
                        }, this.$attrs);
                        return this.$attrs.hasOwnProperty("role") || this.isInNav || (this.isInGroup ? (t.role = "listitem", t["aria-selected"] = String(this.isActive)) : this.isInMenu ? t.role = this.isClickable ? "menuitem" : void 0 : this.isInList && !this.isLink && (t.role = "listitem")), t
                    }
                },
                render: function(t) {
                    var e = this,
                        i = this.generateRouteLink(),
                        n = i.tag,
                        s = i.data;
                    s.attrs = f({}, s.attrs, {}, this.genAttrs()), s.on = f({}, s.on, {
                        click: this.click,
                        keydown: function(t) {
                            t.keyCode === u["s"].enter && e.click(t), e.$emit("keydown", t)
                        }
                    });
                    var r = this.$scopedSlots.default ? this.$scopedSlots.default({
                        active: this.isActive,
                        toggle: this.toggle
                    }) : this.$slots.default;
                    return n = this.inactive ? "div" : n, t(n, this.setTextColor(this.color, s), r)
                }
            })
        },
        db42: function(t, e, i) {}
    }
]);
//# sourceMappingURL=chunk-c96b6fda.dad5a206.js.map