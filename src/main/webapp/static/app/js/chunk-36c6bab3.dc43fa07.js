(window["webpackJsonp"] = window["webpackJsonp"] || []).push([
    ["chunk-36c6bab3"], {
        "169a": function(t, e, i) {
            "use strict";
            i("8e6e"), i("ac6a"), i("456d"), i("6762"), i("2fdb");
            var n = i("bd86"),
                o = (i("c5f6"), i("368e"), i("4ad4")),
                a = i("b848"),
                r = i("75eb"),
                s = i("e707"),
                c = i("e4d3"),
                l = i("21be"),
                u = i("f2e7"),
                d = i("a293"),
                h = i("80d2"),
                v = i("bfc5"),
                f = i("58df"),
                p = i("d9bd");

            function m(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function y(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? m(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : m(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            var b = Object(f["a"])(o["a"], a["a"], r["a"], s["a"], c["a"], l["a"], u["a"]);
            e["a"] = b.extend({
                name: "v-dialog",
                directives: {
                    ClickOutside: d["a"]
                },
                props: {
                    dark: Boolean,
                    disabled: Boolean,
                    fullscreen: Boolean,
                    light: Boolean,
                    maxWidth: {
                        type: [String, Number],
                        default: "none"
                    },
                    noClickAnimation: Boolean,
                    origin: {
                        type: String,
                        default: "center center"
                    },
                    persistent: Boolean,
                    retainFocus: {
                        type: Boolean,
                        default: !0
                    },
                    scrollable: Boolean,
                    transition: {
                        type: [String, Boolean],
                        default: "dialog-transition"
                    },
                    width: {
                        type: [String, Number],
                        default: "auto"
                    }
                },
                data: function() {
                    return {
                        activatedBy: null,
                        animate: !1,
                        animateTimeout: -1,
                        isActive: !!this.value,
                        stackMinZIndex: 200
                    }
                },
                computed: {
                    classes: function() {
                        var t;
                        return t = {}, Object(n["a"])(t, "v-dialog ".concat(this.contentClass).trim(), !0), Object(n["a"])(t, "v-dialog--active", this.isActive), Object(n["a"])(t, "v-dialog--persistent", this.persistent), Object(n["a"])(t, "v-dialog--fullscreen", this.fullscreen), Object(n["a"])(t, "v-dialog--scrollable", this.scrollable), Object(n["a"])(t, "v-dialog--animated", this.animate), t
                    },
                    contentClasses: function() {
                        return {
                            "v-dialog__content": !0,
                            "v-dialog__content--active": this.isActive
                        }
                    },
                    hasActivator: function() {
                        return Boolean(!!this.$slots.activator || !!this.$scopedSlots.activator)
                    }
                },
                watch: {
                    isActive: function(t) {
                        t ? (this.show(), this.hideScroll()) : (this.removeOverlay(), this.unbind())
                    },
                    fullscreen: function(t) {
                        this.isActive && (t ? (this.hideScroll(), this.removeOverlay(!1)) : (this.showScroll(), this.genOverlay()))
                    }
                },
                created: function() {
                    this.$attrs.hasOwnProperty("full-width") && Object(p["d"])("full-width", this)
                },
                beforeMount: function() {
                    var t = this;
                    this.$nextTick(function() {
                        t.isBooted = t.isActive, t.isActive && t.show()
                    })
                },
                beforeDestroy: function() {
                    "undefined" !== typeof window && this.unbind()
                },
                methods: {
                    animateClick: function() {
                        var t = this;
                        this.animate = !1, this.$nextTick(function() {
                            t.animate = !0, window.clearTimeout(t.animateTimeout), t.animateTimeout = window.setTimeout(function() {
                                return t.animate = !1
                            }, 150)
                        })
                    },
                    closeConditional: function(t) {
                        var e = t.target;
                        return !(this._isDestroyed || !this.isActive || this.$refs.content.contains(e) || this.overlay && e && !this.overlay.$el.contains(e)) && (this.$emit("click:outside"), this.persistent && this.overlay ? (this.noClickAnimation || this.overlay.$el !== e && !this.overlay.$el.contains(e) || this.animateClick(), !1) : this.activeZIndex >= this.getMaxZIndex())
                    },
                    hideScroll: function() {
                        this.fullscreen ? document.documentElement.classList.add("overflow-y-hidden") : s["a"].options.methods.hideScroll.call(this)
                    },
                    show: function() {
                        var t = this;
                        !this.fullscreen && !this.hideOverlay && this.genOverlay(), this.$nextTick(function() {
                            t.$refs.content.focus(), t.bind()
                        })
                    },
                    bind: function() {
                        window.addEventListener("focusin", this.onFocusin)
                    },
                    unbind: function() {
                        window.removeEventListener("focusin", this.onFocusin)
                    },
                    onKeydown: function(t) {
                        if (t.keyCode === h["s"].esc && !this.getOpenDependents().length)
                            if (this.persistent) this.noClickAnimation || this.animateClick();
                            else {
                                this.isActive = !1;
                                var e = this.getActivator();
                                this.$nextTick(function() {
                                    return e && e.focus()
                                })
                            }
                        this.$emit("keydown", t)
                    },
                    onFocusin: function(t) {
                        if (t && t.target !== document.activeElement && this.retainFocus) {
                            var e = t.target;
                            if (e && ![document, this.$refs.content].includes(e) && !this.$refs.content.contains(e) && this.activeZIndex >= this.getMaxZIndex() && !this.getOpenDependentElements().some(function(t) {
                                    return t.contains(e)
                                })) {
                                var i = this.$refs.content.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
                                i.length && i[0].focus()
                            }
                        }
                    }
                },
                render: function(t) {
                    var e = this,
                        i = [],
                        n = {
                            class: this.classes,
                            ref: "dialog",
                            directives: [{
                                name: "click-outside",
                                value: function() {
                                    e.isActive = !1
                                },
                                args: {
                                    closeConditional: this.closeConditional,
                                    include: this.getOpenDependentElements
                                }
                            }, {
                                name: "show",
                                value: this.isActive
                            }],
                            on: {
                                click: function(t) {
                                    t.stopPropagation()
                                }
                            },
                            style: {}
                        };
                    this.fullscreen || (n.style = {
                        maxWidth: "none" === this.maxWidth ? void 0 : Object(h["e"])(this.maxWidth),
                        width: "auto" === this.width ? void 0 : Object(h["e"])(this.width)
                    }), i.push(this.genActivator());
                    var o = t("div", n, this.showLazyContent(this.getContentSlot()));
                    return this.transition && (o = t("transition", {
                        props: {
                            name: this.transition,
                            origin: this.origin
                        }
                    }, [o])), i.push(t("div", {
                        class: this.contentClasses,
                        attrs: y({
                            role: "document",
                            tabindex: this.isActive ? 0 : void 0
                        }, this.getScopeIdAttrs()),
                        on: {
                            keydown: this.onKeydown
                        },
                        style: {
                            zIndex: this.activeZIndex
                        },
                        ref: "content"
                    }, [this.$createElement(v["a"], {
                        props: {
                            root: !0,
                            light: this.light,
                            dark: this.dark
                        }
                    }, [o])])), t("div", {
                        staticClass: "v-dialog__container",
                        attrs: {
                            role: "dialog"
                        }
                    }, i)
                }
            })
        },
        "16b7": function(t, e, i) {
            "use strict";
            i("c5f6");
            var n = i("2b0e");
            e["a"] = n["a"].extend().extend({
                name: "delayable",
                props: {
                    openDelay: {
                        type: [Number, String],
                        default: 0
                    },
                    closeDelay: {
                        type: [Number, String],
                        default: 0
                    }
                },
                data: function() {
                    return {
                        openTimeout: void 0,
                        closeTimeout: void 0
                    }
                },
                methods: {
                    clearDelay: function() {
                        clearTimeout(this.openTimeout), clearTimeout(this.closeTimeout)
                    },
                    runDelay: function(t, e) {
                        var i = this;
                        this.clearDelay();
                        var n = parseInt(this["".concat(t, "Delay")], 10);
                        this["".concat(t, "Timeout")] = setTimeout(e || function() {
                            i.isActive = {
                                open: !0,
                                close: !1
                            }[t]
                        }, n)
                    }
                }
            })
        },
        "1abc": function(t, e, i) {
            "use strict";
            var n = i("a797");
            e["a"] = n["a"]
        },
        "21be": function(t, e, i) {
            "use strict";
            i("6762"), i("2fdb");
            var n = i("75fc"),
                o = i("2b0e"),
                a = i("80d2");
            e["a"] = o["a"].extend().extend({
                name: "stackable",
                data: function() {
                    return {
                        stackElement: null,
                        stackExclude: null,
                        stackMinZIndex: 0,
                        isActive: !1
                    }
                },
                computed: {
                    activeZIndex: function() {
                        if ("undefined" === typeof window) return 0;
                        var t = this.stackElement || this.$refs.content,
                            e = this.isActive ? this.getMaxZIndex(this.stackExclude || [t]) + 2 : Object(a["q"])(t);
                        return null == e ? e : parseInt(e)
                    }
                },
                methods: {
                    getMaxZIndex: function() {
                        for (var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : [], e = this.$el, i = [this.stackMinZIndex, Object(a["q"])(e)], o = [].concat(Object(n["a"])(document.getElementsByClassName("v-menu__content--active")), Object(n["a"])(document.getElementsByClassName("v-dialog__content--active"))), r = 0; r < o.length; r++) t.includes(o[r]) || i.push(Object(a["q"])(o[r]));
                        return Math.max.apply(Math, i)
                    }
                }
            })
        },
        "297c": function(t, e, i) {
            "use strict";
            i("c5f6");
            var n = i("2b0e"),
                o = i("37c6");
            e["a"] = n["a"].extend().extend({
                name: "loadable",
                props: {
                    loading: {
                        type: [Boolean, String],
                        default: !1
                    },
                    loaderHeight: {
                        type: [Number, String],
                        default: 2
                    }
                },
                methods: {
                    genProgress: function() {
                        return !1 === this.loading ? null : this.$slots.progress || this.$createElement(o["a"], {
                            props: {
                                absolute: !0,
                                color: !0 === this.loading || "" === this.loading ? this.color || "primary" : this.loading,
                                height: this.loaderHeight,
                                indeterminate: !0
                            }
                        })
                    }
                }
            })
        },
        "368e": function(t, e, i) {},
        "37c6": function(t, e, i) {
            "use strict";
            var n = i("8e36");
            e["a"] = n["a"]
        },
        "3c93": function(t, e, i) {},
        "4ad4": function(t, e, i) {
            "use strict";
            i("ac6a"), i("456d");
            var n = i("7618"),
                o = (i("6762"), i("2fdb"), i("16b7")),
                a = i("f2e7"),
                r = i("58df"),
                s = i("80d2"),
                c = i("d9bd"),
                l = Object(r["a"])(o["a"], a["a"]);
            e["a"] = l.extend({
                name: "activatable",
                props: {
                    activator: {
                        default: null,
                        validator: function(t) {
                            return ["string", "object"].includes(Object(n["a"])(t))
                        }
                    },
                    disabled: Boolean,
                    internalActivator: Boolean,
                    openOnHover: Boolean
                },
                data: function() {
                    return {
                        activatorElement: null,
                        activatorNode: [],
                        events: ["click", "mouseenter", "mouseleave"],
                        listeners: {}
                    }
                },
                watch: {
                    activator: "resetActivator",
                    activatorElement: function(t) {
                        t && this.addActivatorEvents()
                    },
                    openOnHover: "resetActivator"
                },
                mounted: function() {
                    var t = Object(s["p"])(this, "activator", !0);
                    t && ["v-slot", "normal"].includes(t) && Object(c["b"])('The activator slot must be bound, try \'<template v-slot:activator="{ on }"><v-btn v-on="on">\'', this), this.getActivator()
                },
                beforeDestroy: function() {
                    this.removeActivatorEvents()
                },
                methods: {
                    addActivatorEvents: function() {
                        if (this.activator && !this.disabled && this.activatorElement) {
                            this.listeners = this.genActivatorListeners();
                            for (var t = Object.keys(this.listeners), e = 0, i = t; e < i.length; e++) {
                                var n = i[e];
                                this.activatorElement.addEventListener(n, this.listeners[n])
                            }
                        }
                    },
                    genActivator: function() {
                        var t = Object(s["o"])(this, "activator", Object.assign(this.getValueProxy(), {
                            on: this.genActivatorListeners(),
                            attrs: this.genActivatorAttributes()
                        })) || [];
                        return this.activatorNode = t, t
                    },
                    genActivatorAttributes: function() {
                        return {
                            role: "button",
                            "aria-haspopup": !0,
                            "aria-expanded": String(this.isActive)
                        }
                    },
                    genActivatorListeners: function() {
                        var t = this;
                        if (this.disabled) return {};
                        var e = {};
                        return this.openOnHover ? (e.mouseenter = function(e) {
                            t.getActivator(e), t.runDelay("open")
                        }, e.mouseleave = function(e) {
                            t.getActivator(e), t.runDelay("close")
                        }) : e.click = function(e) {
                            t.activatorElement && t.activatorElement.focus(), t.isActive = !t.isActive
                        }, e
                    },
                    getActivator: function(t) {
                        if (this.activatorElement) return this.activatorElement;
                        var e = null;
                        if (this.activator) {
                            var i = this.internalActivator ? this.$el : document;
                            e = "string" === typeof this.activator ? i.querySelector(this.activator) : this.activator.$el ? this.activator.$el : this.activator
                        } else t ? e = t.currentTarget || t.target : this.activatorNode.length && (e = this.activatorNode[0].elm);
                        return this.activatorElement = e, this.activatorElement
                    },
                    getContentSlot: function() {
                        return Object(s["o"])(this, "default", this.getValueProxy(), !0)
                    },
                    getValueProxy: function() {
                        var t = this;
                        return {
                            get value() {
                                return t.isActive
                            },
                            set value(e) {
                                t.isActive = e
                            }
                        }
                    },
                    removeActivatorEvents: function() {
                        if (this.activator && this.activatorElement) {
                            for (var t = Object.keys(this.listeners), e = 0, i = t; e < i.length; e++) {
                                var n = i[e];
                                this.activatorElement.removeEventListener(n, this.listeners[n])
                            }
                            this.listeners = {}
                        }
                    },
                    resetActivator: function() {
                        this.activatorElement = null, this.getActivator()
                    }
                }
            })
        },
        "75eb": function(t, e, i) {
            "use strict";
            var n = i("bd86"),
                o = (i("ac6a"), i("7618")),
                a = i("9d65"),
                r = i("80d2"),
                s = i("58df"),
                c = i("d9bd");

            function l(t) {
                var e = Object(o["a"])(t);
                return "boolean" === e || "string" === e || t.nodeType === Node.ELEMENT_NODE
            }
            e["a"] = Object(s["a"])(a["a"]).extend({
                name: "detachable",
                props: {
                    attach: {
                        default: !1,
                        validator: l
                    },
                    contentClass: {
                        type: String,
                        default: ""
                    }
                },
                data: function() {
                    return {
                        activatorNode: null,
                        hasDetached: !1
                    }
                },
                watch: {
                    attach: function() {
                        this.hasDetached = !1, this.initDetach()
                    },
                    hasContent: "initDetach"
                },
                beforeMount: function() {
                    var t = this;
                    this.$nextTick(function() {
                        if (t.activatorNode) {
                            var e = Array.isArray(t.activatorNode) ? t.activatorNode : [t.activatorNode];
                            e.forEach(function(e) {
                                if (e.elm && t.$el.parentNode) {
                                    var i = t.$el === t.$el.parentNode.firstChild ? t.$el : t.$el.nextSibling;
                                    t.$el.parentNode.insertBefore(e.elm, i)
                                }
                            })
                        }
                    })
                },
                mounted: function() {
                    this.eager && this.initDetach()
                },
                deactivated: function() {
                    this.isActive = !1
                },
                beforeDestroy: function() {
                    try {
                        if (this.$refs.content && this.$refs.content.parentNode && this.$refs.content.parentNode.removeChild(this.$refs.content), this.activatorNode) {
                            var t = Array.isArray(this.activatorNode) ? this.activatorNode : [this.activatorNode];
                            t.forEach(function(t) {
                                t.elm && t.elm.parentNode && t.elm.parentNode.removeChild(t.elm)
                            })
                        }
                    } catch (e) {
                        console.log(e)
                    }
                },
                methods: {
                    getScopeIdAttrs: function() {
                        var t = Object(r["m"])(this.$vnode, "context.$options._scopeId");
                        return t && Object(n["a"])({}, t, "")
                    },
                    initDetach: function() {
                        var t;
                        this._isDestroyed || !this.$refs.content || this.hasDetached || "" === this.attach || !0 === this.attach || "attach" === this.attach || (t = !1 === this.attach ? document.querySelector("[data-app]") : "string" === typeof this.attach ? document.querySelector(this.attach) : this.attach, t ? (t.insertBefore(this.$refs.content, t.firstChild), this.hasDetached = !0) : Object(c["c"])("Unable to locate target ".concat(this.attach || "[data-app]"), this))
                    }
                }
            })
        },
        a293: function(t, e, i) {
            "use strict";

            function n() {
                return !1
            }

            function o(t, e, i) {
                i.args = i.args || {};
                var o = i.args.closeConditional || n;
                if (t && !1 !== o(t) && !("isTrusted" in t && !t.isTrusted || "pointerType" in t && !t.pointerType)) {
                    var a = (i.args.include || function() {
                        return []
                    })();
                    a.push(e), !a.some(function(e) {
                        return e.contains(t.target)
                    }) && setTimeout(function() {
                        o(t) && i.value && i.value(t)
                    }, 0)
                }
            }
            var a = {
                inserted: function(t, e) {
                    var i = function(i) {
                            return o(i, t, e)
                        },
                        n = document.querySelector("[data-app]") || document.body;
                    n.addEventListener("click", i, !0), t._clickOutside = i
                },
                unbind: function(t) {
                    if (t._clickOutside) {
                        var e = document.querySelector("[data-app]") || document.body;
                        e && e.removeEventListener("click", t._clickOutside, !0), delete t._clickOutside
                    }
                }
            };
            e["a"] = a
        },
        a797: function(t, e, i) {
            "use strict";
            i("8e6e"), i("ac6a"), i("456d");
            var n = i("bd86"),
                o = (i("c5f6"), i("3c93"), i("a9ad")),
                a = i("7560"),
                r = i("f2e7"),
                s = i("58df");

            function c(t, e) {
                var i = Object.keys(t);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(t);
                    e && (n = n.filter(function(e) {
                        return Object.getOwnPropertyDescriptor(t, e).enumerable
                    })), i.push.apply(i, n)
                }
                return i
            }

            function l(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var i = null != arguments[e] ? arguments[e] : {};
                    e % 2 ? c(i, !0).forEach(function(e) {
                        Object(n["a"])(t, e, i[e])
                    }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(t, Object.getOwnPropertyDescriptors(i)) : c(i).forEach(function(e) {
                        Object.defineProperty(t, e, Object.getOwnPropertyDescriptor(i, e))
                    })
                }
                return t
            }
            e["a"] = Object(s["a"])(o["a"], a["a"], r["a"]).extend({
                name: "v-overlay",
                props: {
                    absolute: Boolean,
                    color: {
                        type: String,
                        default: "#212121"
                    },
                    dark: {
                        type: Boolean,
                        default: !0
                    },
                    opacity: {
                        type: [Number, String],
                        default: .46
                    },
                    value: {
                        default: !0
                    },
                    zIndex: {
                        type: [Number, String],
                        default: 5
                    }
                },
                computed: {
                    __scrim: function() {
                        var t = this.setBackgroundColor(this.color, {
                            staticClass: "v-overlay__scrim",
                            style: {
                                opacity: this.computedOpacity
                            }
                        });
                        return this.$createElement("div", t)
                    },
                    classes: function() {
                        return l({
                            "v-overlay--absolute": this.absolute,
                            "v-overlay--active": this.isActive
                        }, this.themeClasses)
                    },
                    computedOpacity: function() {
                        return Number(this.isActive ? this.opacity : 0)
                    },
                    styles: function() {
                        return {
                            zIndex: this.zIndex
                        }
                    }
                },
                methods: {
                    genContent: function() {
                        return this.$createElement("div", {
                            staticClass: "v-overlay__content"
                        }, this.$slots.default)
                    }
                },
                render: function(t) {
                    var e = [this.__scrim];
                    return this.isActive && e.push(this.genContent()), t("div", {
                        staticClass: "v-overlay",
                        class: this.classes,
                        style: this.styles
                    }, e)
                }
            })
        },
        b848: function(t, e, i) {
            "use strict";
            var n = i("75fc"),
                o = i("58df");

            function a(t) {
                for (var e = [], i = 0; i < t.length; i++) {
                    var o = t[i];
                    o.isActive && o.isDependent ? e.push(o) : e.push.apply(e, Object(n["a"])(a(o.$children)))
                }
                return e
            }
            e["a"] = Object(o["a"])().extend({
                name: "dependent",
                data: function() {
                    return {
                        closeDependents: !0,
                        isActive: !1,
                        isDependent: !0
                    }
                },
                watch: {
                    isActive: function(t) {
                        if (!t)
                            for (var e = this.getOpenDependents(), i = 0; i < e.length; i++) e[i].isActive = !1
                    }
                },
                methods: {
                    getOpenDependents: function() {
                        return this.closeDependents ? a(this.$children) : []
                    },
                    getOpenDependentElements: function() {
                        for (var t = [], e = this.getOpenDependents(), i = 0; i < e.length; i++) t.push.apply(t, Object(n["a"])(e[i].getClickableDependentElements()));
                        return t
                    },
                    getClickableDependentElements: function() {
                        var t = [this.$el];
                        return this.$refs.content && t.push(this.$refs.content), this.overlay && t.push(this.overlay.$el), t.push.apply(t, Object(n["a"])(this.getOpenDependentElements())), t
                    }
                }
            })
        },
        bfc5: function(t, e, i) {
            "use strict";
            i("7514");
            var n = i("7560"),
                o = i("58df");
            e["a"] = Object(o["a"])(n["a"]).extend({
                name: "theme-provider",
                props: {
                    root: Boolean
                },
                computed: {
                    isDark: function() {
                        return this.root ? this.rootIsDark : n["a"].options.computed.isDark.call(this)
                    }
                },
                render: function() {
                    return this.$slots.default && this.$slots.default.find(function(t) {
                        return !t.isComment && " " !== t.text
                    })
                }
            })
        },
        e4d3: function(t, e, i) {
            "use strict";
            var n = i("2b0e");
            e["a"] = n["a"].extend({
                name: "returnable",
                props: {
                    returnValue: null
                },
                data: function() {
                    return {
                        isActive: !1,
                        originalValue: null
                    }
                },
                watch: {
                    isActive: function(t) {
                        t ? this.originalValue = this.returnValue : this.$emit("update:return-value", this.originalValue)
                    }
                },
                methods: {
                    save: function(t) {
                        var e = this;
                        this.originalValue = t, setTimeout(function() {
                            e.isActive = !1
                        })
                    }
                }
            })
        },
        e707: function(t, e, i) {
            "use strict";
            i("6762"), i("2fdb");
            var n = i("1abc"),
                o = i("80d2"),
                a = i("2b0e");
            e["a"] = a["a"].extend().extend({
                name: "overlayable",
                props: {
                    hideOverlay: Boolean
                },
                data: function() {
                    return {
                        overlay: null
                    }
                },
                watch: {
                    hideOverlay: function(t) {
                        t ? this.removeOverlay() : this.genOverlay()
                    }
                },
                beforeDestroy: function() {
                    this.removeOverlay()
                },
                methods: {
                    createOverlay: function() {
                        var t = new n["a"]({
                            propsData: {
                                absolute: this.absolute,
                                value: !1
                            }
                        });
                        t.$mount();
                        var e = this.absolute ? this.$el.parentNode : document.querySelector("[data-app]");
                        e && e.insertBefore(t.$el, e.firstChild), this.overlay = t
                    },
                    genOverlay: function() {
                        var t = this;
                        if (this.hideScroll(), !this.hideOverlay) return this.overlay || this.createOverlay(), requestAnimationFrame(function() {
                            t.overlay && (void 0 !== t.activeZIndex ? t.overlay.zIndex = String(t.activeZIndex - 1) : t.$el && (t.overlay.zIndex = Object(o["q"])(t.$el)), t.overlay.value = !0)
                        }), !0
                    },
                    removeOverlay: function() {
                        var t = this,
                            e = !(arguments.length > 0 && void 0 !== arguments[0]) || arguments[0];
                        this.overlay && (Object(o["a"])(this.overlay.$el, "transitionend", function() {
                            t.overlay && t.overlay.$el && t.overlay.$el.parentNode && !t.overlay.value && (t.overlay.$el.parentNode.removeChild(t.overlay.$el), t.overlay.$destroy(), t.overlay = null)
                        }), this.overlay.value = !1), e && this.showScroll()
                    },
                    scrollListener: function(t) {
                        if ("keydown" === t.type) {
                            if (["INPUT", "TEXTAREA", "SELECT"].includes(t.target.tagName) || t.target.isContentEditable) return;
                            var e = [o["s"].up, o["s"].pageup],
                                i = [o["s"].down, o["s"].pagedown];
                            if (e.includes(t.keyCode)) t.deltaY = -1;
                            else {
                                if (!i.includes(t.keyCode)) return;
                                t.deltaY = 1
                            }
                        }(t.target === this.overlay || "keydown" !== t.type && t.target === document.body || this.checkPath(t)) && t.preventDefault()
                    },
                    hasScrollbar: function(t) {
                        if (!t || t.nodeType !== Node.ELEMENT_NODE) return !1;
                        var e = window.getComputedStyle(t);
                        return ["auto", "scroll"].includes(e.overflowY) && t.scrollHeight > t.clientHeight
                    },
                    shouldScroll: function(t, e) {
                        return 0 === t.scrollTop && e < 0 || t.scrollTop + t.clientHeight === t.scrollHeight && e > 0
                    },
                    isInside: function(t, e) {
                        return t === e || null !== t && t !== document.body && this.isInside(t.parentNode, e)
                    },
                    checkPath: function(t) {
                        var e = t.path || this.composedPath(t),
                            i = t.deltaY;
                        if ("keydown" === t.type && e[0] === document.body) {
                            var n = this.$refs.dialog,
                                o = window.getSelection().anchorNode;
                            return !(n && this.hasScrollbar(n) && this.isInside(o, n)) || this.shouldScroll(n, i)
                        }
                        for (var a = 0; a < e.length; a++) {
                            var r = e[a];
                            if (r === document) return !0;
                            if (r === document.documentElement) return !0;
                            if (r === this.$refs.content) return !0;
                            if (this.hasScrollbar(r)) return this.shouldScroll(r, i)
                        }
                        return !0
                    },
                    composedPath: function(t) {
                        if (t.composedPath) return t.composedPath();
                        var e = [],
                            i = t.target;
                        while (i) {
                            if (e.push(i), "HTML" === i.tagName) return e.push(document), e.push(window), e;
                            i = i.parentElement
                        }
                        return e
                    },
                    hideScroll: function() {
                        this.$vuetify.breakpoint.smAndDown ? document.documentElement.classList.add("overflow-y-hidden") : (Object(o["b"])(window, "wheel", this.scrollListener, {
                            passive: !1
                        }), window.addEventListener("keydown", this.scrollListener))
                    },
                    showScroll: function() {
                        document.documentElement.classList.remove("overflow-y-hidden"), window.removeEventListener("wheel", this.scrollListener), window.removeEventListener("keydown", this.scrollListener)
                    }
                }
            })
        }
    }
]);
//# sourceMappingURL=chunk-36c6bab3.dc43fa07.js.map