<!DOCTYPE html>
<!-- saved from url=(0028)http://ag.soft.gszh.cn/login -->
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>
<html lang="zh-Hans">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=gb2312">
    <style class="vjs-styles-defaults">
        .video-js {
            width: 300px;
            height: 150px;
        }

        .vjs-fluid {
            padding-top: 56.25%
        }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <link rel="icon" href="http://ag.soft.gszh.cn/favicon.ico">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="static/app/css/chunk-09ab10ea.15fa0623.css" rel="prefetch">
    <link href="static/app/css/chunk-0bb83762.bbec304b.css" rel="prefetch">
    <link href="static/app/css/chunk-0bed03c6.14eb00c8.css" rel="prefetch">
    <link href="static/app/css/chunk-0f750b20.f68e3ec7.css" rel="prefetch">
    <link href="static/app/css/chunk-106400cf.fe7212bc.css" rel="prefetch">
    <link href="static/app/css/chunk-1574105c.c6210d3e.css" rel="prefetch">
    <link href="static/app/css/chunk-178c6be4.5c537ce3.css" rel="prefetch">
    <link href="static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="static/app/css/chunk-1b28f5a6.930ec35f.css" rel="prefetch">
    <link href="static/app/css/chunk-25158862.03700a9a.css" rel="prefetch">
    <link href="static/app/css/chunk-27a492a3.ff01f531.css" rel="prefetch">
    <link href="static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="static/app/css/chunk-2f8119ea.f8966651.css" rel="prefetch">
    <link href="static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="static/app/css/chunk-36c6bab3.1976c0ac.css" rel="prefetch">
    <link href="static/app/css/chunk-455ddd9a.7c775a1b.css" rel="prefetch">
    <link href="static/app/css/chunk-49099d7a.2fa6b608.css" rel="prefetch">
    <link href="static/app/css/chunk-6035521e.4fbc4feb.css" rel="prefetch">
    <link href="static/app/css/chunk-6651766b.7429f851.css" rel="prefetch">
    <link href="static/app/css/chunk-6a09404c.a7f98917.css" rel="prefetch">
    <link href="static/app/css/chunk-6bb7c6fc.01b906da.css" rel="prefetch">
    <link href="static/app/css/chunk-71eab13e.0ac3eb1b.css" rel="prefetch">
    <link href="static/app/css/chunk-75a24b7b.fbd8b555.css" rel="prefetch">
    <link href="static/app/css/chunk-776d3ee6.3981d2b5.css" rel="prefetch">
    <link href="static/app/css/chunk-78d58620.d1a5072e.css" rel="prefetch">
    <link href="static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="static/app/css/chunk-923d3768.9af9bc64.css" rel="prefetch">
    <link href="static/app/css/chunk-951086b4.d1a5072e.css" rel="prefetch">
    <link href="static/app/css/chunk-aed9619c.d1a5072e.css" rel="prefetch">
    <link href="static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="static/app/css/chunk-b87d2c68.77ab4765.css" rel="prefetch">
    <link href="static/app/css/chunk-bd143cfa.cf0834ff.css" rel="prefetch">
    <link href="static/app/css/chunk-be332352.ea3b6773.css" rel="prefetch">
    <link href="static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="static/app/css/chunk-db2ee608.a3fb6984.css" rel="prefetch">
    <link href="static/app/css/chunk-e55b657e.19afc90a.css" rel="prefetch">
    <link href="static/app/css/chunk-e7464a4c.942ad61f.css" rel="prefetch">
    <link href="static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="static/app/css/chunk-vendors.017e9771.css" rel="preload" as="style">
    <link href="static/app/css/chunk-vendors.017e9771.css" rel="stylesheet">
    <link href="static/app/css/app.f748b306.css" rel="stylesheet">
    <style type="text/css">.vts__snackbar {
        max-width: none !important;
        width: auto !important
    }

    .vts__snackbar .v-snack__content {
        justify-content: flex-start
    }

    .vts__snackbar__icon {
        margin-right: 12px
    }

    .vts__snackbar__message {
        margin-right: auto
    }

    .vts__snackbar__close {
        margin: 0 -9px 0 24px !important;
        justify-self: flex-end
    }

    .vts__snackbar.v-snack--vertical .vts__snackbar__icon {
        margin: 0 0 12px !important
    }

    .vts__snackbar.v-snack--vertical .v-snack__content {
        padding-bottom: 24px !important
    }

    .vts__snackbar.v-snack--vertical .vts__snackbar__message {
        padding-top: 12px
    }

    .vts__snackbar.v-snack--vertical .vts__snackbar__icon + .vts__snackbar__message {
        padding-top: 0
    }

    .vts__snackbar.v-snack--vertical .vts__snackbar__close {
        margin: 0 !important;
        position: absolute;
        right: 4px;
        top: 4px;
        transform: scale(.75);
        padding: 4px !important
    }</style>
    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-b14a74f2.e8d83e75.css">

    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-7df73ecf.f28a53a1.css">

    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-34951fe0.a46de2ef.css">

    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-c96b6fda.9a391f00.css">

    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-36c6bab3.1976c0ac.css">

    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-49099d7a.2fa6b608.css">
    <style type="text/css" id="vuetify-theme-stylesheet">
        :root {
            --v-anchor-base: #107064;
            --v-primary-base: #107064;
            --v-primary-lighten5: #a4f8e9;
            --v-primary-lighten4: #88dccd;
            --v-primary-lighten3: #6cc0b1;
            --v-primary-lighten2: #50a497;
            --v-primary-lighten1: #348a7d;
            --v-primary-darken1: #00574c;
            --v-primary-darken2: #003f36;
            --v-primary-darken3: #002920;
            --v-primary-darken4: #001809;
            --v-secondary-base: #81c784;
            --v-secondary-lighten5: #ffffff;
            --v-secondary-lighten4: #f1fff2;
            --v-secondary-lighten3: #d4ffd5;
            --v-secondary-lighten2: #b8ffba;
            --v-secondary-lighten1: #9ce39e;
            --v-secondary-darken1: #66ab6a;
            --v-secondary-darken2: #4c9152;
            --v-secondary-darken3: #31773a;
            --v-secondary-darken4: #145d23;
            --v-accent-base: #00c853;
            --v-accent-lighten5: #c4ffdb;
            --v-accent-lighten4: #a5ffbf;
            --v-accent-lighten3: #85ffa3;
            --v-accent-lighten2: #65ff88;
            --v-accent-lighten1: #41e56d;
            --v-accent-darken1: #00ac39;
            --v-accent-darken2: #00901e;
            --v-accent-darken3: #007500;
            --v-accent-darken4: #005b00;
            --v-error-base: #ff5252;
            --v-error-lighten5: #ffe4d5;
            --v-error-lighten4: #ffc6b9;
            --v-error-lighten3: #ffa99e;
            --v-error-lighten2: #ff8c84;
            --v-error-lighten1: #ff6f6a;
            --v-error-darken1: #df323b;
            --v-error-darken2: #bf0025;
            --v-error-darken3: #9f0010;
            --v-error-darken4: #800000;
            --v-info-base: #2196f3;
            --v-info-lighten5: #d4ffff;
            --v-info-lighten4: #b5ffff;
            --v-info-lighten3: #95e8ff;
            --v-info-lighten2: #75ccff;
            --v-info-lighten1: #51b0ff;
            --v-info-darken1: #007cd6;
            --v-info-darken2: #0064ba;
            --v-info-darken3: #004d9f;
            --v-info-darken4: #003784;
            --v-success-base: #4caf50;
            --v-success-lighten5: #dcffd6;
            --v-success-lighten4: #beffba;
            --v-success-lighten3: #a2ff9e;
            --v-success-lighten2: #85e783;
            --v-success-lighten1: #69cb69;
            --v-success-darken1: #2d9437;
            --v-success-darken2: #00791e;
            --v-success-darken3: #006000;
            --v-success-darken4: #004700;
            --v-warning-base: #fb8c00;
            --v-warning-lighten5: #ffff9e;
            --v-warning-lighten4: #fffb82;
            --v-warning-lighten3: #ffdf67;
            --v-warning-lighten2: #ffc24b;
            --v-warning-lighten1: #ffa72d;
            --v-warning-darken1: #db7200;
            --v-warning-darken2: #bb5900;
            --v-warning-darken3: #9d4000;
            --v-warning-darken4: #802700;
        }

        .v-application a {
            color: var(--v-anchor-base);
        }

        .v-application .primary {
            background-color: var(--v-primary-base) !important;
            border-color: var(--v-primary-base) !important;
        }

        .v-application .primary--text {
            color: var(--v-primary-base) !important;
            caret-color: var(--v-primary-base) !important;
        }

        .v-application .primary.lighten-5 {
            background-color: var(--v-primary-lighten5) !important;
            border-color: var(--v-primary-lighten5) !important;
        }

        .v-application .primary--text.text--lighten-5 {
            color: var(--v-primary-lighten5) !important;
            caret-color: var(--v-primary-lighten5) !important;
        }

        .v-application .primary.lighten-4 {
            background-color: var(--v-primary-lighten4) !important;
            border-color: var(--v-primary-lighten4) !important;
        }

        .v-application .primary--text.text--lighten-4 {
            color: var(--v-primary-lighten4) !important;
            caret-color: var(--v-primary-lighten4) !important;
        }

        .v-application .primary.lighten-3 {
            background-color: var(--v-primary-lighten3) !important;
            border-color: var(--v-primary-lighten3) !important;
        }

        .v-application .primary--text.text--lighten-3 {
            color: var(--v-primary-lighten3) !important;
            caret-color: var(--v-primary-lighten3) !important;
        }

        .v-application .primary.lighten-2 {
            background-color: var(--v-primary-lighten2) !important;
            border-color: var(--v-primary-lighten2) !important;
        }

        .v-application .primary--text.text--lighten-2 {
            color: var(--v-primary-lighten2) !important;
            caret-color: var(--v-primary-lighten2) !important;
        }

        .v-application .primary.lighten-1 {
            background-color: var(--v-primary-lighten1) !important;
            border-color: var(--v-primary-lighten1) !important;
        }

        .v-application .primary--text.text--lighten-1 {
            color: var(--v-primary-lighten1) !important;
            caret-color: var(--v-primary-lighten1) !important;
        }

        .v-application .primary.darken-1 {
            background-color: var(--v-primary-darken1) !important;
            border-color: var(--v-primary-darken1) !important;
        }

        .v-application .primary--text.text--darken-1 {
            color: var(--v-primary-darken1) !important;
            caret-color: var(--v-primary-darken1) !important;
        }

        .v-application .primary.darken-2 {
            background-color: var(--v-primary-darken2) !important;
            border-color: var(--v-primary-darken2) !important;
        }

        .v-application .primary--text.text--darken-2 {
            color: var(--v-primary-darken2) !important;
            caret-color: var(--v-primary-darken2) !important;
        }

        .v-application .primary.darken-3 {
            background-color: var(--v-primary-darken3) !important;
            border-color: var(--v-primary-darken3) !important;
        }

        .v-application .primary--text.text--darken-3 {
            color: var(--v-primary-darken3) !important;
            caret-color: var(--v-primary-darken3) !important;
        }

        .v-application .primary.darken-4 {
            background-color: var(--v-primary-darken4) !important;
            border-color: var(--v-primary-darken4) !important;
        }

        .v-application .primary--text.text--darken-4 {
            color: var(--v-primary-darken4) !important;
            caret-color: var(--v-primary-darken4) !important;
        }

        .v-application .secondary {
            background-color: var(--v-secondary-base) !important;
            border-color: var(--v-secondary-base) !important;
        }

        .v-application .secondary--text {
            color: var(--v-secondary-base) !important;
            caret-color: var(--v-secondary-base) !important;
        }

        .v-application .secondary.lighten-5 {
            background-color: var(--v-secondary-lighten5) !important;
            border-color: var(--v-secondary-lighten5) !important;
        }

        .v-application .secondary--text.text--lighten-5 {
            color: var(--v-secondary-lighten5) !important;
            caret-color: var(--v-secondary-lighten5) !important;
        }

        .v-application .secondary.lighten-4 {
            background-color: var(--v-secondary-lighten4) !important;
            border-color: var(--v-secondary-lighten4) !important;
        }

        .v-application .secondary--text.text--lighten-4 {
            color: var(--v-secondary-lighten4) !important;
            caret-color: var(--v-secondary-lighten4) !important;
        }

        .v-application .secondary.lighten-3 {
            background-color: var(--v-secondary-lighten3) !important;
            border-color: var(--v-secondary-lighten3) !important;
        }

        .v-application .secondary--text.text--lighten-3 {
            color: var(--v-secondary-lighten3) !important;
            caret-color: var(--v-secondary-lighten3) !important;
        }

        .v-application .secondary.lighten-2 {
            background-color: var(--v-secondary-lighten2) !important;
            border-color: var(--v-secondary-lighten2) !important;
        }

        .v-application .secondary--text.text--lighten-2 {
            color: var(--v-secondary-lighten2) !important;
            caret-color: var(--v-secondary-lighten2) !important;
        }

        .v-application .secondary.lighten-1 {
            background-color: var(--v-secondary-lighten1) !important;
            border-color: var(--v-secondary-lighten1) !important;
        }

        .v-application .secondary--text.text--lighten-1 {
            color: var(--v-secondary-lighten1) !important;
            caret-color: var(--v-secondary-lighten1) !important;
        }

        .v-application .secondary.darken-1 {
            background-color: var(--v-secondary-darken1) !important;
            border-color: var(--v-secondary-darken1) !important;
        }

        .v-application .secondary--text.text--darken-1 {
            color: var(--v-secondary-darken1) !important;
            caret-color: var(--v-secondary-darken1) !important;
        }

        .v-application .secondary.darken-2 {
            background-color: var(--v-secondary-darken2) !important;
            border-color: var(--v-secondary-darken2) !important;
        }

        .v-application .secondary--text.text--darken-2 {
            color: var(--v-secondary-darken2) !important;
            caret-color: var(--v-secondary-darken2) !important;
        }

        .v-application .secondary.darken-3 {
            background-color: var(--v-secondary-darken3) !important;
            border-color: var(--v-secondary-darken3) !important;
        }

        .v-application .secondary--text.text--darken-3 {
            color: var(--v-secondary-darken3) !important;
            caret-color: var(--v-secondary-darken3) !important;
        }

        .v-application .secondary.darken-4 {
            background-color: var(--v-secondary-darken4) !important;
            border-color: var(--v-secondary-darken4) !important;
        }

        .v-application .secondary--text.text--darken-4 {
            color: var(--v-secondary-darken4) !important;
            caret-color: var(--v-secondary-darken4) !important;
        }

        .v-application .accent {
            background-color: var(--v-accent-base) !important;
            border-color: var(--v-accent-base) !important;
        }

        .v-application .accent--text {
            color: var(--v-accent-base) !important;
            caret-color: var(--v-accent-base) !important;
        }

        .v-application .accent.lighten-5 {
            background-color: var(--v-accent-lighten5) !important;
            border-color: var(--v-accent-lighten5) !important;
        }

        .v-application .accent--text.text--lighten-5 {
            color: var(--v-accent-lighten5) !important;
            caret-color: var(--v-accent-lighten5) !important;
        }

        .v-application .accent.lighten-4 {
            background-color: var(--v-accent-lighten4) !important;
            border-color: var(--v-accent-lighten4) !important;
        }

        .v-application .accent--text.text--lighten-4 {
            color: var(--v-accent-lighten4) !important;
            caret-color: var(--v-accent-lighten4) !important;
        }

        .v-application .accent.lighten-3 {
            background-color: var(--v-accent-lighten3) !important;
            border-color: var(--v-accent-lighten3) !important;
        }

        .v-application .accent--text.text--lighten-3 {
            color: var(--v-accent-lighten3) !important;
            caret-color: var(--v-accent-lighten3) !important;
        }

        .v-application .accent.lighten-2 {
            background-color: var(--v-accent-lighten2) !important;
            border-color: var(--v-accent-lighten2) !important;
        }

        .v-application .accent--text.text--lighten-2 {
            color: var(--v-accent-lighten2) !important;
            caret-color: var(--v-accent-lighten2) !important;
        }

        .v-application .accent.lighten-1 {
            background-color: var(--v-accent-lighten1) !important;
            border-color: var(--v-accent-lighten1) !important;
        }

        .v-application .accent--text.text--lighten-1 {
            color: var(--v-accent-lighten1) !important;
            caret-color: var(--v-accent-lighten1) !important;
        }

        .v-application .accent.darken-1 {
            background-color: var(--v-accent-darken1) !important;
            border-color: var(--v-accent-darken1) !important;
        }

        .v-application .accent--text.text--darken-1 {
            color: var(--v-accent-darken1) !important;
            caret-color: var(--v-accent-darken1) !important;
        }

        .v-application .accent.darken-2 {
            background-color: var(--v-accent-darken2) !important;
            border-color: var(--v-accent-darken2) !important;
        }

        .v-application .accent--text.text--darken-2 {
            color: var(--v-accent-darken2) !important;
            caret-color: var(--v-accent-darken2) !important;
        }

        .v-application .accent.darken-3 {
            background-color: var(--v-accent-darken3) !important;
            border-color: var(--v-accent-darken3) !important;
        }

        .v-application .accent--text.text--darken-3 {
            color: var(--v-accent-darken3) !important;
            caret-color: var(--v-accent-darken3) !important;
        }

        .v-application .accent.darken-4 {
            background-color: var(--v-accent-darken4) !important;
            border-color: var(--v-accent-darken4) !important;
        }

        .v-application .accent--text.text--darken-4 {
            color: var(--v-accent-darken4) !important;
            caret-color: var(--v-accent-darken4) !important;
        }

        .v-application .error {
            background-color: var(--v-error-base) !important;
            border-color: var(--v-error-base) !important;
        }

        .v-application .error--text {
            color: var(--v-error-base) !important;
            caret-color: var(--v-error-base) !important;
        }

        .v-application .error.lighten-5 {
            background-color: var(--v-error-lighten5) !important;
            border-color: var(--v-error-lighten5) !important;
        }

        .v-application .error--text.text--lighten-5 {
            color: var(--v-error-lighten5) !important;
            caret-color: var(--v-error-lighten5) !important;
        }

        .v-application .error.lighten-4 {
            background-color: var(--v-error-lighten4) !important;
            border-color: var(--v-error-lighten4) !important;
        }

        .v-application .error--text.text--lighten-4 {
            color: var(--v-error-lighten4) !important;
            caret-color: var(--v-error-lighten4) !important;
        }

        .v-application .error.lighten-3 {
            background-color: var(--v-error-lighten3) !important;
            border-color: var(--v-error-lighten3) !important;
        }

        .v-application .error--text.text--lighten-3 {
            color: var(--v-error-lighten3) !important;
            caret-color: var(--v-error-lighten3) !important;
        }

        .v-application .error.lighten-2 {
            background-color: var(--v-error-lighten2) !important;
            border-color: var(--v-error-lighten2) !important;
        }

        .v-application .error--text.text--lighten-2 {
            color: var(--v-error-lighten2) !important;
            caret-color: var(--v-error-lighten2) !important;
        }

        .v-application .error.lighten-1 {
            background-color: var(--v-error-lighten1) !important;
            border-color: var(--v-error-lighten1) !important;
        }

        .v-application .error--text.text--lighten-1 {
            color: var(--v-error-lighten1) !important;
            caret-color: var(--v-error-lighten1) !important;
        }

        .v-application .error.darken-1 {
            background-color: var(--v-error-darken1) !important;
            border-color: var(--v-error-darken1) !important;
        }

        .v-application .error--text.text--darken-1 {
            color: var(--v-error-darken1) !important;
            caret-color: var(--v-error-darken1) !important;
        }

        .v-application .error.darken-2 {
            background-color: var(--v-error-darken2) !important;
            border-color: var(--v-error-darken2) !important;
        }

        .v-application .error--text.text--darken-2 {
            color: var(--v-error-darken2) !important;
            caret-color: var(--v-error-darken2) !important;
        }

        .v-application .error.darken-3 {
            background-color: var(--v-error-darken3) !important;
            border-color: var(--v-error-darken3) !important;
        }

        .v-application .error--text.text--darken-3 {
            color: var(--v-error-darken3) !important;
            caret-color: var(--v-error-darken3) !important;
        }

        .v-application .error.darken-4 {
            background-color: var(--v-error-darken4) !important;
            border-color: var(--v-error-darken4) !important;
        }

        .v-application .error--text.text--darken-4 {
            color: var(--v-error-darken4) !important;
            caret-color: var(--v-error-darken4) !important;
        }

        .v-application .info {
            background-color: var(--v-info-base) !important;
            border-color: var(--v-info-base) !important;
        }

        .v-application .info--text {
            color: var(--v-info-base) !important;
            caret-color: var(--v-info-base) !important;
        }

        .v-application .info.lighten-5 {
            background-color: var(--v-info-lighten5) !important;
            border-color: var(--v-info-lighten5) !important;
        }

        .v-application .info--text.text--lighten-5 {
            color: var(--v-info-lighten5) !important;
            caret-color: var(--v-info-lighten5) !important;
        }

        .v-application .info.lighten-4 {
            background-color: var(--v-info-lighten4) !important;
            border-color: var(--v-info-lighten4) !important;
        }

        .v-application .info--text.text--lighten-4 {
            color: var(--v-info-lighten4) !important;
            caret-color: var(--v-info-lighten4) !important;
        }

        .v-application .info.lighten-3 {
            background-color: var(--v-info-lighten3) !important;
            border-color: var(--v-info-lighten3) !important;
        }

        .v-application .info--text.text--lighten-3 {
            color: var(--v-info-lighten3) !important;
            caret-color: var(--v-info-lighten3) !important;
        }

        .v-application .info.lighten-2 {
            background-color: var(--v-info-lighten2) !important;
            border-color: var(--v-info-lighten2) !important;
        }

        .v-application .info--text.text--lighten-2 {
            color: var(--v-info-lighten2) !important;
            caret-color: var(--v-info-lighten2) !important;
        }

        .v-application .info.lighten-1 {
            background-color: var(--v-info-lighten1) !important;
            border-color: var(--v-info-lighten1) !important;
        }

        .v-application .info--text.text--lighten-1 {
            color: var(--v-info-lighten1) !important;
            caret-color: var(--v-info-lighten1) !important;
        }

        .v-application .info.darken-1 {
            background-color: var(--v-info-darken1) !important;
            border-color: var(--v-info-darken1) !important;
        }

        .v-application .info--text.text--darken-1 {
            color: var(--v-info-darken1) !important;
            caret-color: var(--v-info-darken1) !important;
        }

        .v-application .info.darken-2 {
            background-color: var(--v-info-darken2) !important;
            border-color: var(--v-info-darken2) !important;
        }

        .v-application .info--text.text--darken-2 {
            color: var(--v-info-darken2) !important;
            caret-color: var(--v-info-darken2) !important;
        }

        .v-application .info.darken-3 {
            background-color: var(--v-info-darken3) !important;
            border-color: var(--v-info-darken3) !important;
        }

        .v-application .info--text.text--darken-3 {
            color: var(--v-info-darken3) !important;
            caret-color: var(--v-info-darken3) !important;
        }

        .v-application .info.darken-4 {
            background-color: var(--v-info-darken4) !important;
            border-color: var(--v-info-darken4) !important;
        }

        .v-application .info--text.text--darken-4 {
            color: var(--v-info-darken4) !important;
            caret-color: var(--v-info-darken4) !important;
        }

        .v-application .success {
            background-color: var(--v-success-base) !important;
            border-color: var(--v-success-base) !important;
        }

        .v-application .success--text {
            color: var(--v-success-base) !important;
            caret-color: var(--v-success-base) !important;
        }

        .v-application .success.lighten-5 {
            background-color: var(--v-success-lighten5) !important;
            border-color: var(--v-success-lighten5) !important;
        }

        .v-application .success--text.text--lighten-5 {
            color: var(--v-success-lighten5) !important;
            caret-color: var(--v-success-lighten5) !important;
        }

        .v-application .success.lighten-4 {
            background-color: var(--v-success-lighten4) !important;
            border-color: var(--v-success-lighten4) !important;
        }

        .v-application .success--text.text--lighten-4 {
            color: var(--v-success-lighten4) !important;
            caret-color: var(--v-success-lighten4) !important;
        }

        .v-application .success.lighten-3 {
            background-color: var(--v-success-lighten3) !important;
            border-color: var(--v-success-lighten3) !important;
        }

        .v-application .success--text.text--lighten-3 {
            color: var(--v-success-lighten3) !important;
            caret-color: var(--v-success-lighten3) !important;
        }

        .v-application .success.lighten-2 {
            background-color: var(--v-success-lighten2) !important;
            border-color: var(--v-success-lighten2) !important;
        }

        .v-application .success--text.text--lighten-2 {
            color: var(--v-success-lighten2) !important;
            caret-color: var(--v-success-lighten2) !important;
        }

        .v-application .success.lighten-1 {
            background-color: var(--v-success-lighten1) !important;
            border-color: var(--v-success-lighten1) !important;
        }

        .v-application .success--text.text--lighten-1 {
            color: var(--v-success-lighten1) !important;
            caret-color: var(--v-success-lighten1) !important;
        }

        .v-application .success.darken-1 {
            background-color: var(--v-success-darken1) !important;
            border-color: var(--v-success-darken1) !important;
        }

        .v-application .success--text.text--darken-1 {
            color: var(--v-success-darken1) !important;
            caret-color: var(--v-success-darken1) !important;
        }

        .v-application .success.darken-2 {
            background-color: var(--v-success-darken2) !important;
            border-color: var(--v-success-darken2) !important;
        }

        .v-application .success--text.text--darken-2 {
            color: var(--v-success-darken2) !important;
            caret-color: var(--v-success-darken2) !important;
        }

        .v-application .success.darken-3 {
            background-color: var(--v-success-darken3) !important;
            border-color: var(--v-success-darken3) !important;
        }

        .v-application .success--text.text--darken-3 {
            color: var(--v-success-darken3) !important;
            caret-color: var(--v-success-darken3) !important;
        }

        .v-application .success.darken-4 {
            background-color: var(--v-success-darken4) !important;
            border-color: var(--v-success-darken4) !important;
        }

        .v-application .success--text.text--darken-4 {
            color: var(--v-success-darken4) !important;
            caret-color: var(--v-success-darken4) !important;
        }

        .v-application .warning {
            background-color: var(--v-warning-base) !important;
            border-color: var(--v-warning-base) !important;
        }

        .v-application .warning--text {
            color: var(--v-warning-base) !important;
            caret-color: var(--v-warning-base) !important;
        }

        .v-application .warning.lighten-5 {
            background-color: var(--v-warning-lighten5) !important;
            border-color: var(--v-warning-lighten5) !important;
        }

        .v-application .warning--text.text--lighten-5 {
            color: var(--v-warning-lighten5) !important;
            caret-color: var(--v-warning-lighten5) !important;
        }

        .v-application .warning.lighten-4 {
            background-color: var(--v-warning-lighten4) !important;
            border-color: var(--v-warning-lighten4) !important;
        }

        .v-application .warning--text.text--lighten-4 {
            color: var(--v-warning-lighten4) !important;
            caret-color: var(--v-warning-lighten4) !important;
        }

        .v-application .warning.lighten-3 {
            background-color: var(--v-warning-lighten3) !important;
            border-color: var(--v-warning-lighten3) !important;
        }

        .v-application .warning--text.text--lighten-3 {
            color: var(--v-warning-lighten3) !important;
            caret-color: var(--v-warning-lighten3) !important;
        }

        .v-application .warning.lighten-2 {
            background-color: var(--v-warning-lighten2) !important;
            border-color: var(--v-warning-lighten2) !important;
        }

        .v-application .warning--text.text--lighten-2 {
            color: var(--v-warning-lighten2) !important;
            caret-color: var(--v-warning-lighten2) !important;
        }

        .v-application .warning.lighten-1 {
            background-color: var(--v-warning-lighten1) !important;
            border-color: var(--v-warning-lighten1) !important;
        }

        .v-application .warning--text.text--lighten-1 {
            color: var(--v-warning-lighten1) !important;
            caret-color: var(--v-warning-lighten1) !important;
        }

        .v-application .warning.darken-1 {
            background-color: var(--v-warning-darken1) !important;
            border-color: var(--v-warning-darken1) !important;
        }

        .v-application .warning--text.text--darken-1 {
            color: var(--v-warning-darken1) !important;
            caret-color: var(--v-warning-darken1) !important;
        }

        .v-application .warning.darken-2 {
            background-color: var(--v-warning-darken2) !important;
            border-color: var(--v-warning-darken2) !important;
        }

        .v-application .warning--text.text--darken-2 {
            color: var(--v-warning-darken2) !important;
            caret-color: var(--v-warning-darken2) !important;
        }

        .v-application .warning.darken-3 {
            background-color: var(--v-warning-darken3) !important;
            border-color: var(--v-warning-darken3) !important;
        }

        .v-application .warning--text.text--darken-3 {
            color: var(--v-warning-darken3) !important;
            caret-color: var(--v-warning-darken3) !important;
        }

        .v-application .warning.darken-4 {
            background-color: var(--v-warning-darken4) !important;
            border-color: var(--v-warning-darken4) !important;
        }

        .v-application .warning--text.text--darken-4 {
            color: var(--v-warning-darken4) !important;
            caret-color: var(--v-warning-darken4) !important;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="static/app/css/chunk-19e06ac6.9864336b.css">


    <script src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script src="../../static/app/js/nav.js"></script>

    <script type="text/javascript" src="../../static/app/js/iscroll.js"></script>
    <script type="text/javascript">
        function verifyPassword(obj)
        {
            var login=document.getElementById("login").value;
            var password=document.getElementById("password").value;
            var logindata={"login":login,"password":password}

            $.ajax({
                url:'/IndexController/verifyPassword',
                data:JSON.stringify(logindata),
                type:'POST',
                async:false,
                contentType:'application/json',
                success:function(res) {
                    if(res.result=='Pass')
                    {
                        obj.setAttribute("href","/IndexController/Index");

                    }
                    else
                    {
                        if(login=="")
                        {
                            alert("账号不能为空，请输入账号");
                        }
                        else if(password=="")
                        {
                            alert("密码不能为空，请输入密码");
                        }
                        else
                        {
                            alert("账号密码错误,请重新输入");
                        }

                    }
                }
                })

        }    </script>

</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent" style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div data-v-5f1f2330="" class="container login pb-12 fill-height">
                <div data-v-5f1f2330="" class="layout align-center">
                    <div data-v-5f1f2330="" class="flex">
                        <div data-v-5f1f2330="" class="layout column">
                            <div data-v-5f1f2330="" class="login-title">
                                <div data-v-5f1f2330="" class="login-title__logo"><img data-v-5f1f2330=""
                                                                                       src="static/app/img/navigation-logo.6f4594d8.png">
                                </div>
                                <div data-v-5f1f2330="" class="login-title__text">
                                    缘田生态科技
                                </div>
                            </div>
                            <div data-v-5f1f2330="" class="flex">
                                <div data-v-5f1f2330=""
                                     class="login-card v-card v-card--flat v-sheet theme--light transparent">
                                    <div data-v-5f1f2330="" class="container">
                                        <form data-v-5f1f2330="" novalidate="novalidate" class="v-form">
                                            <div data-v-5f1f2330=""
                                                 class="v-input v-input--is-label-active v-input--is-dirty theme--dark v-text-field v-text-field--is-booted v-text-field--placeholder">
                                                <div class="v-input__control">
                                                    <div class="v-input__slot">
                                                        <div class="v-input__prepend-inner"><img data-v-5f1f2330=""
                                                                                                 src="static/app/img/username.icon.c5022aa7.svg"
                                                                                                 alt="icon"
                                                                                                 class="login-card__form-icon">
                                                        </div>
                                                        <div class="v-text-field__slot"><input name="accountName"
                                                                                               data-vv-as="账号"
                                                                                               id="login"
                                                                                               placeholder="账号"
                                                                                               type="text"></div>
                                                    </div>
                                                    <div class="v-text-field__details">
                                                        <div class="v-messages theme--dark">
                                                            <div class="v-messages__wrapper"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-v-5f1f2330=""
                                                 class="v-input v-input--is-label-active v-input--is-dirty theme--dark v-text-field v-text-field--is-booted v-text-field--placeholder">
                                                <div class="v-input__control">
                                                    <div class="v-input__slot">
                                                        <div class="v-input__prepend-inner"><img data-v-5f1f2330=""
                                                                                                 src="static/app/img/password.icon.ba15f5df.svg"
                                                                                                 alt="icon"
                                                                                                 class="login-card__form-icon">
                                                        </div>
                                                        <div class="v-text-field__slot"><input name="password"
                                                                                               data-vv-as="密码"
                                                                                               id="password"
                                                                                               placeholder="密码"
                                                                                               type="password"></div>
                                                    </div>
                                                    <div class="v-text-field__details">
                                                        <div class="v-messages theme--dark">
                                                            <div class="v-messages__wrapper"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-v-5f1f2330=""
                                                 class="v-input login-card__checkbox v-input--is-label-active v-input--is-dirty theme--dark v-input--selection-controls v-input--checkbox white--text">
                                                <div class="v-input__control">
                                                    <div class="v-input__slot">
                                                        <div class="v-input--selection-controls__input"><input
                                                                aria-checked="true" id="input-169" role="checkbox"
                                                                type="checkbox" value="">
                                                            <div class="v-input--selection-controls__ripple white--text"></div>
                                                            <i aria-hidden="true"
                                                               class="v-icon notranslate mdi mdi-checkbox-marked theme--dark white--text"></i>
                                                        </div>
                                                        <label for="input-169" class="v-label theme--dark"
                                                               style="left: 0px; right: auto; position: relative;">记住密码</label>
                                                    </div>
                                                    <div class="v-messages theme--dark white--text">
                                                        <div class="v-messages__wrapper"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="" onclick="verifyPassword(this)" id="loginButton" ><button data-v-5f1f2330="" type="button"
                                                                                           class="login-form__submit-button v-btn v-btn--block theme--dark elevation-0 v-size--large"
                                                                                           style="background-color: rgb(78, 134, 187);
                                                    border-color: rgb(78, 134, 187);"> <span
                                                    class="v-btn__content">
                  登录
                </span></button></a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-v-5f1f2330="" class="powered-by">Powered by 缘田生态科技服务平台</div>
            </div>
        </div>
    </div>
</div>
</body>
</html>