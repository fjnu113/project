<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <link rel="icon" href="http://ag.soft.gszh.cn/favicon.ico">
    <title>修改密码</title>
    <base href=".">
    <link href="./../../static/app/css/chunk-31c15f22.7e0f23d7.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-776d3ee6.3981d2b5.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-db2ee608.a3fb6984.css" rel="prefetch">
    <link href="./../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="./../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-776d3ee6.3981d2b5.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-31c15f22.7e0f23d7.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-db2ee608.a3fb6984.css">
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent"
                     style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div>
                <header data-v-2eae54fc=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed primary"
                        data-booted="true"
                        style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div onclick="window.location.href='/IndexController/Index';"
                                 data-v-2eae54fc="" class="toolbar-left"><img data-v-2eae54fc=""
                                                                              src="./../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                              alt="icon"></div>
                            修改密码
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                </header>
                <main class="v-content" data-booted="true" style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div class="container" style="position: relative;">
                            <form novalidate="novalidate" class="v-form">
                                <div class="v-input theme--light v-text-field v-text-field--is-booted">
                                    <div class="v-input__control">
                                        <div class="v-input__slot">
                                            <div class="v-text-field__slot"><input
                                                    name="oldPassword" placeholder="原密码"
                                                    id="input-450" type="password"></div>
                                        </div>
                                        <div class="v-text-field__details">
                                            <div class="v-messages theme--light">
                                                <div class="v-messages__wrapper"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="v-input theme--light v-text-field v-text-field--is-booted">
                                    <div class="v-input__control">
                                        <div class="v-input__slot">
                                            <div class="v-text-field__slot"><input
                                                    name="newPassword" placeholder="新密码"
                                                    id="input-453" type="password"></div>
                                        </div>
                                        <div class="v-text-field__details">
                                            <div class="v-messages theme--light">
                                                <div class="v-messages__wrapper"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="v-input theme--light v-text-field v-text-field--is-booted">
                                    <div class="v-input__control">
                                        <div class="v-input__slot">
                                            <div class="v-text-field__slot"><input
                                                    name="confirmation" placeholder="确认密码"
                                                    id="input-456" type="password"></div>
                                        </div>
                                        <div class="v-text-field__details">
                                            <div class="v-messages theme--light">
                                                <div class="v-messages__wrapper"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="button"
                                        class="v-btn v-btn--block v-btn--contained theme--light v-size--large primary">
                                    <span class="v-btn__content">确定</span></button>
                            </form>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>

</body>
</html>
