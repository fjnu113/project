<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0045)http://ag.soft.gszh.cn/croplands/60/tradition -->
<html lang="zh-Hans">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/apphead.css" rel="stylesheet">
    <link href="../../static/app/css/chunk-107b5196.d80540dc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="../../static/app/css/chunk-4182d364.bbec304b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-6a09404c.a7f98917.css" rel="prefetch">
    <link href="../../static/app/css/chunk-6d00f192.7af6302c.css" rel="prefetch">
    <link href="../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="../../static/app/css/chunk-e7464a4c.942ad61f.css" rel="prefetch">
    <link href="../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-19e06ac6.9864336b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-2c59789a.c26f4895.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-6a09404c.a7f98917.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-e7464a4c.942ad61f.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-107b5196.d80540dc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-4182d364.bbec304b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-6d00f192.7af6302c.css">

    <script type="text/javascript" src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            presentPlantBathDetail();
        })
        function presentPlantBathDetail(){
            var batchNum="${data.batchNum}"
            var data={"batchNum":batchNum}     //忘了添加键值
            $.ajax({
                url:'/PlantManage/presentPlantBathDetail',
                data:JSON.stringify(data),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    for(i=0;i<=res.data.length;i++){
                        var data={"varietyName":res.data[i].varietyName}
                        document.getElementById("farmlandNum").innerHTML=res.data[i].farmlandNum;
                        document.getElementById("varietyName1").innerHTML=res.data[i].varietyName;
                        document.getElementById("varietyName2").innerHTML=res.data[i].varietyName;
                        document.getElementById("varietyName3").innerHTML=res.data[i].varietyName;
                        document.getElementById("area").innerHTML=res.data[i].area;
                        document.getElementById("deepth").innerHTML=res.data[i].deepth;
                        document.getElementById("batchNum").innerHTML=res.data[i].batchNum;
                        document.getElementById("starTime").innerHTML=res.data[i].starTime;
                        document.getElementById("plantCount").innerHTML=res.data[i].plantCount;
                        document.getElementById("growthStage").innerHTML=res.data[i].growthStage;
                        document.getElementById("describetion").innerHTML=res.data[i].describetion;
                        document.getElementById("url").href='/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'';
                    }
                }
            })
        }
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent" style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div>
                <header data-v-2eae54fc=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed v-app-bar--is-scrolled primary"
                        data-booted="true" style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div data-v-2eae54fc="" class="toolbar-left" onclick="window.location.href='/IndexController/PlantingManage'"><img data-v-2eae54fc=""
                                                                              src="../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                              alt="icon"></div>
                            贵生实验4田
                            <div data-v-2eae54fc="" class="toolbar-right">
                                <div>
                                    <button type="button"
                                            class="v-btn v-btn--flat v-btn--icon v-btn--round v-btn--text v-btn--tile theme--dark v-size--default">
                                        <span class="v-btn__content"><i aria-hidden="true"
                                                                        class="v-icon notranslate mdi mdi-menu theme--dark"></i></span>
                                    </button>
                                    <div class="v-menu">
                                        <div role="menu" class="v-menu__content theme--light "
                                             style="min-width: 0px; top: 12px; left: 0px; transform-origin: left top; z-index: 0; display: none;"></div>
                                    </div>
                                    <div role="dialog" class="v-dialog__container">
                                        <div role="document" class="v-dialog__content" style="z-index: 0;">
                                            <div class="v-dialog v-dialog--persistent" style="display: none;"></div>
                                        </div>
                                    </div>
                                    <div role="dialog" class="v-dialog__container">
                                        <div role="document" class="v-dialog__content" style="z-index: 0;">
                                            <div class="v-dialog v-dialog--persistent" style="display: none;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main class="v-content grey lighten-5" data-booted="true" style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div class="v-responsive v-image" style="height: 250px;">
                            <div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>
                            <div class="v-image__image v-image__image--cover"
                                 style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>
                            <div class="v-responsive__content" style="width: 1080px;"></div>
                        </div>
                        <div role="list"
                             class="v-list my-3 v-sheet v-sheet--tile theme--light v-list--subheader v-list--two-line">
                            <div class="v-subheader theme--light"><img src="../../static/app/img/dot.icon.7f85dfff.svg"
                                                                       alt="icon">
                                &nbsp;基本信息
                            </div>
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        编号
                                    </div>
                                    <div class="v-list-item__title" id="farmlandNum">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        类型
                                    </div>
                                    <div class="v-list-item__title">
                                        传统田
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        认养人
                                    </div>
                                    <div class="v-list-item__title">
                                        小顾
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        面积
                                    </div>
                                    <div class="v-list-item__title" id="area">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        土壤深度
                                    </div>
                                    <div class="v-list-item__title" id="deepth">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        在植作物
                                    </div>
                                    <div class="v-list-item__title" id="varietyName1">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        状态
                                    </div>
                                    <div class="v-list-item__title">
                                        在种
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-3 v-sheet theme--light" subheader="" two-line="">
                            <div class="v-subheader theme--light"><img src="../../static/app/img/dot.icon.7f85dfff.svg"
                                                                       alt="icon">
                                &nbsp;种植信息
                            </div>
                            <div tabindex="-1" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        批次号
                                    </div>
                                    <div class="v-list-item__title" id="batchNum">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        种植日期
                                    </div>
                                    <div class="v-list-item__title" id="starTime">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        种植品种
                                    </div>
                                    <div class="v-list-item__title" id="varietyName2">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        播种数量
                                    </div>
                                    <div class="v-list-item__title" id="plantCount">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        生长阶段
                                    </div>
                                    <div class="v-list-item__title" id="growthStage">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal" class="ml-4 v-divider theme--light">
                            <div tabindex="-1" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        生长状况
                                    </div>
                                    <div class="v-list-item__title">
                                        良
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        预计产出:
                                    </div>
                                    <div class="v-list-item__title">
                                        -
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="list" class="v-list my-3 v-sheet v-sheet--tile theme--light v-list--subheader">
                            <div class="v-subheader theme--light"><img src="../../static/app/img/dot.icon.7f85dfff.svg"
                                                                       alt="icon">
                                &nbsp;作物信息
                            </div>
                            <div data-v-210fea08="" class="px-3 zjk">
                                <ul data-v-210fea08="">
                                    <li data-v-210fea08="">
                                        <div data-v-210fea08="" class="shucai_bg"><img data-v-210fea08=""
                                                                                       src="../../static/app/img/baocai.jpg">
                                        </div>
                                        <div data-v-210fea08="" class="sc_js"><p data-v-210fea08="" id="varietyName3"></p>
                                            <p data-v-210fea08="" id="describetion">
                                            </p>
                                            <a data-v-210fea08="" href="" id="url">培育详情</a></div>
                                        <div data-v-210fea08="" class="clearfix"></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div role="list" class="v-list my-3 v-sheet v-sheet--tile theme--light v-list--subheader">
                            <div class="v-subheader theme--light">
                                <div class="row no-gutters">
                                    <div class="col col-5"><img src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon">
                                        &nbsp;产量记录
                                    </div>
                                    <div class="spacer"></div>
                                    <div class="relative text-right col col-4"><a
                                            href="http://ag.soft.gszh.cn/croplands/60/units/history"
                                            class="v-btn v-btn--flat v-btn--router v-btn--text theme--light v-size--default blue-grey--text"
                                            style="margin-right: -16px; height: 12px;"><span class="v-btn__content">
              更多
              <i aria-hidden="true" class="v-icon notranslate mdi mdi-chevron-right theme--light"></i></span></a></div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row no-gutters">
                                    <div class="text-center col">
                                        <div class="caption">累计种植批次</div>
                                        <div class="title">
                                            0
                                        </div>
                                    </div>
                                    <div class="text-center col">
                                        <div class="caption">累计产出</div>
                                        <div class="title">
                                            0
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>