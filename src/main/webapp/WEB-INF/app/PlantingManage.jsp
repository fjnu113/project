<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0048)http://ag.soft.gszh.cn/cultivation/units/history -->
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/apphead.css" rel="stylesheet">
    <link href="../../static/app/css/chunk-107b5196.d80540dc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="../../static/app/css/chunk-4182d364.bbec304b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5dc2f224.009d7923.css" rel="prefetch">
    <link href="../../static/app/css/chunk-6a09404c.a7f98917.css" rel="prefetch">
    <link href="../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="../../static/app/css/chunk-e7464a4c.942ad61f.css" rel="prefetch">
    <link href="../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-19e06ac6.9864336b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-2c59789a.c26f4895.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-6a09404c.a7f98917.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-e7464a4c.942ad61f.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-4182d364.bbec304b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-107b5196.d80540dc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5dc2f224.009d7923.css">
    <script type="text/javascript" src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            presentPlantBath();
            historyPlantBath();
        });
        function presentPlantBath() {
            var login={"login":'123'};
            $.ajax({
                url:'/PlantManage/presentPlantBath',
                data:JSON.stringify(login),
                type: 'POST',
                contentType: 'application/json',
                success:function(res){
                    for (i = 0; i <= res.data.length; ++i) {
                        var data={"batchNum":res.data[i].batchNum};
                        content = '<div data-v-e11ea4c0="" class="pa-2 col col-6">';

                        content += '<a data-v-e11ea4c0="" href="/IndexController/Plant Batches?data='+encodeURIComponent(JSON.stringify(data))+'" ' +
                                'class="mx-auto card-radius v-card v-card--link v-sheet theme--light" tabindex="0">';
                        content += '<div data-v-e11ea4c0="" class="v-responsive v-image" style="height: 120px;">';
                        content += '<div class="v-responsive__sizer" style="padding-bottom: 66.6%;"></div>';
                        content += '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/baocai.jpg&quot;); background-position: center center;"></div>';
                        content += '<div class="v-responsive__content" style="width: 500px;"></div>';
                        content += '</div>';
                        content += '<div data-v-e11ea4c0="" class="v-card__text pt-2 pb-1">';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0 flex-nowrap"><span data-v-e11ea4c0="" class="font-weight-bold primary--text grow" style="width: 4.5em;">';
                        content += res.data[i].planName;
                        content += '</span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="" class="d-inline-block text-truncate">';
                        content += res.data[i].batchNum;
                        content += '</span></div>';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0"><span data-v-e11ea4c0="">栽种时间 </span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="">';
                        content += res.data[i].startTime;
                        content += '</span></div>';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0"><span data-v-e11ea4c0="">预计收获</span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="">';
                        content += res.data[i].endTime;
                        content += '</span></div>';
                        content += '</div></a></div>';
                        $("#now").append(content);
                    }
                }
            });
        }
        function historyPlantBath(){
            var login={"login":"123"}
            $.ajax({
                url:'/PlantManage/historyPlantBath',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    for(i=0; i<=res.data.length; ++i){
                        var data={"batchNum":res.data[i].batchNum};
                        content ='<div data-v-e11ea4c0="" class="pa-2 col col-6">';
                        content +='<a data-v-e11ea4c0="" href="/IndexController/EcologicalTraceability?data='+encodeURIComponent(JSON.stringify(data))+'" ' +
                                'class="mx-auto card-radius v-card v-card--link v-sheet theme--light" tabindex="0">';
                        content += '<div data-v-e11ea4c0="" class="v-responsive v-image" style="height: 120px;">';
                        content += '<div class="v-responsive__sizer" style="padding-bottom: 66.6%;"></div>';
                        content += '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/baocai.jpg&quot;); background-position: center center;"></div>';
                        content += '<div class="v-responsive__content" style="width: 500px;"></div>';
                        content += '</div>';
                        content += '<div data-v-e11ea4c0="" class="v-card__text pt-2 pb-1">';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0 flex-nowrap"><span data-v-e11ea4c0="" class="font-weight-bold primary--text grow" style="width: 4.5em;">';
                        content += res.data[i].planName;
                        content += '</span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="" class="d-inline-block text-truncate">';
                        content += res.data[i].batchNum;
                        content += '</span></div>';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0"><span data-v-e11ea4c0="">栽种时间</span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="">';
                        content += res.data[i].startTime;
                        content += '</span></div>';
                        content += '<div data-v-e11ea4c0="" class="row pa-1 pt-0"><span data-v-e11ea4c0="">收获时间</span>';
                        content += '<div data-v-e11ea4c0="" class="spacer"></div>';
                        content += '<span data-v-e11ea4c0="">';
                        content += res.data[i].endTime;
                        content += '</span></div>';
                        content += '</div></a></div>';
                        $("#history").append(content);
                    }
                }
            })
        }

        function changeN(){
            var now1=$('#now1');
            var now2=$('#now2');
            var history1=$('#history1');
            var history2=$('#history2');
            var c=document.getElementById('now1').className;
            if(c=="font-weight-medium v-tab"){
                now1.addClass('v-tab--active');
                now1.attr('aria-selected','true');
                history1.removeClass('v-tab--active');
                history1.attr('aria-selected','false');
                now2.addClass('v-window-item--active');
                now2.css('display','block');
                history2.removeClass('v-window-item--active');
                history2.css('display','none');
            }
        }

        function changeH(){
            var now1=$('#now1');
            var now2=$('#now2');
            var history1=$('#history1');
            var history2=$('#history2');
            var c=document.getElementById('history1').className;
            if(c=="font-weight-medium v-tab"){
                now1.removeClass('v-tab--active');
                now1.attr('aria-selected','false');
                history1.addClass('v-tab--active');
                history1.attr('aria-selected','true');
                now2.removeClass('v-window-item--active');
                now2.css('display','none');
                history2.addClass('v-window-item--active');
                history2.css('display','block');
            }
        }
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent" style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div data-v-6aa72c76="">
                <header data-v-2eae54fc="" data-v-6aa72c76=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-toolbar--extended v-app-bar v-app-bar--fixed primary"
                        data-booted="true" style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div onclick="window.location.href='/IndexController/Index'" data-v-2eae54fc="" class="toolbar-left">
                                <img data-v-2eae54fc="" src="../../static/app/img/arrow-left.ac6c6d62.svg" alt="icon">
                            </div>
                            <div data-v-6aa72c76="" class="header__title">种植管理</div>
                            <div data-v-2eae54fc="" class="toolbar-right">
                                <a data-v-6aa72c76="" class="v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router v-btn--text v-btn--tile theme--dark v-size--default"><span
                                    class="v-btn__content">
                                    <i data-v-6aa72c76="" aria-hidden="true" class="v-icon notranslate mdi mdi-calendar-month theme--dark"></i></span></a><a
                                    data-v-6aa72c76="" href="http://ag.soft.gszh.cn/croplands/cultivation/freecropland"
                                    class="v-btn v-btn--flat v-btn--icon v-btn--round v-btn--router v-btn--text v-btn--tile theme--dark v-size--default"><span
                                    class="v-btn__content">
                                    <i data-v-6aa72c76="" aria-hidden="true" class="v-icon notranslate mdi mdi-plus theme--dark"></i>
                                </span>
                            </a>
                            </div>
                        </div>
                    </div>
                    <div class="v-toolbar__extension" style="height: 48px;">
                        <div data-v-6aa72c76="" class="v-tabs v-tabs--grow theme--light">
                            <div role="tablist"
                                 class="v-item-group theme--light v-slide-group v-tabs-bar v-tabs-bar--is-mobile v-tabs-bar--show-arrows primary--text"
                                 data-booted="true">
                                <div class="v-slide-group__prev v-slide-group__prev--disabled"><i aria-hidden="true"
                                                                                                  class="v-icon notranslate v-icon--disabled mdi mdi-chevron-left theme--light"></i>
                                </div>
                                <div class="v-slide-group__wrapper">
                                    <div class="v-slide-group__content v-tabs-bar__content"
                                         style="transform: translateX(0px);">
                                        <!--<a data-v-6aa72c76=""-->
                                                                                <!--href="http://ag.soft.gszh.cn/cultivation/units"-->
                                                                                <!--class="v-tab" tabindex="0"-->
                                                                                <!--aria-selected="false" role="tab">-->
                                        <!--在植批次-->
                                    <!--</a><a data-v-6aa72c76="" href="http://ag.soft.gszh.cn/cultivation/units/history"-->
                                           <!--class="font-weight-bold v-tab&#45;&#45;active v-tab font-weight-bold" tabindex="0"-->
                                           <!--aria-selected="true" role="tab">历史批次-->
                                    <!--</a>-->
                                        <button id="now1" style="height: 48px; width: 194px;" aria-selected="true" class="font-weight-medium v-tab v-tab--active" onclick="changeN()">
                                            在植批次
                                        </button>
                                        <button id="history1" style="height: 48px; width: 194px;" aria-selected="false" class="font-weight-medium v-tab" onclick="changeH()">
                                            历史批次
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!- 下面展示的农作物展示图 ->
                <main data-v-6aa72c76="" class="v-content" data-booted="true" style="padding: 96px 0px 0px;">
                    <div class="v-content__wrap">
                        <div data-v-6aa72c76="" class="container">
                            <div data-v-6aa72c76="">
                                <!-for展示多个缩略图->
                                <div id="now2" data-v-e11ea4c0="" class="v-window-item v-window-item--active" style="">
                                    <div data-v-e11ea4c0="" class="row px-2" id="now" style="font-size: 9px;">
                                    </div>
                                </div>
                                <div id="history2" data-v-e11ea4c0="" class="v-window-item" style="display: none;">
                                    <div data-v-e11ea4c0="" class="row px-2" id="history" style="font-size: 9px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>