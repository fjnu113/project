<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0026)http://ag.soft.gszh.cn/56/ -->
<html lang="zh-Hans">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">

    <link href="../../static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="../../static/app/css/chunk-36c6bab3.1976c0ac.css" rel="prefetch">
    <link href="../../static/app/css/chunk-49099d7a.2fa6b608.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="../../static/app/css/chunk-vendors.017e9771.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.017e9771.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link href="../../static/app/css/nav_sytle.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-36c6bab3.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-49099d7a.2fa6b608.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-19e06ac6.9864336b.css">

    <style type="text/css" id="vuetify-theme-stylesheet">:root {
        --v-anchor-base: #107064;
        --v-primary-base: #107064;
        --v-primary-lighten5: #a4f8e9;
        --v-primary-lighten4: #88dccd;
        --v-primary-lighten3: #6cc0b1;
        --v-primary-lighten2: #50a497;
        --v-primary-lighten1: #348a7d;
        --v-primary-darken1: #00574c;
        --v-primary-darken2: #003f36;
        --v-primary-darken3: #002920;
        --v-primary-darken4: #001809;
        --v-secondary-base: #81c784;
        --v-secondary-lighten5: #ffffff;
        --v-secondary-lighten4: #f1fff2;
        --v-secondary-lighten3: #d4ffd5;
        --v-secondary-lighten2: #b8ffba;
        --v-secondary-lighten1: #9ce39e;
        --v-secondary-darken1: #66ab6a;
        --v-secondary-darken2: #4c9152;
        --v-secondary-darken3: #31773a;
        --v-secondary-darken4: #145d23;
        --v-accent-base: #00c853;
        --v-accent-lighten5: #c4ffdb;
        --v-accent-lighten4: #a5ffbf;
        --v-accent-lighten3: #85ffa3;
        --v-accent-lighten2: #65ff88;
        --v-accent-lighten1: #41e56d;
        --v-accent-darken1: #00ac39;
        --v-accent-darken2: #00901e;
        --v-accent-darken3: #007500;
        --v-accent-darken4: #005b00;
        --v-error-base: #ff5252;
        --v-error-lighten5: #ffe4d5;
        --v-error-lighten4: #ffc6b9;
        --v-error-lighten3: #ffa99e;
        --v-error-lighten2: #ff8c84;
        --v-error-lighten1: #ff6f6a;
        --v-error-darken1: #df323b;
        --v-error-darken2: #bf0025;
        --v-error-darken3: #9f0010;
        --v-error-darken4: #800000;
        --v-info-base: #2196f3;
        --v-info-lighten5: #d4ffff;
        --v-info-lighten4: #b5ffff;
        --v-info-lighten3: #95e8ff;
        --v-info-lighten2: #75ccff;
        --v-info-lighten1: #51b0ff;
        --v-info-darken1: #007cd6;
        --v-info-darken2: #0064ba;
        --v-info-darken3: #004d9f;
        --v-info-darken4: #003784;
        --v-success-base: #4caf50;
        --v-success-lighten5: #dcffd6;
        --v-success-lighten4: #beffba;
        --v-success-lighten3: #a2ff9e;
        --v-success-lighten2: #85e783;
        --v-success-lighten1: #69cb69;
        --v-success-darken1: #2d9437;
        --v-success-darken2: #00791e;
        --v-success-darken3: #006000;
        --v-success-darken4: #004700;
        --v-warning-base: #fb8c00;
        --v-warning-lighten5: #ffff9e;
        --v-warning-lighten4: #fffb82;
        --v-warning-lighten3: #ffdf67;
        --v-warning-lighten2: #ffc24b;
        --v-warning-lighten1: #ffa72d;
        --v-warning-darken1: #db7200;
        --v-warning-darken2: #bb5900;
        --v-warning-darken3: #9d4000;
        --v-warning-darken4: #802700;
    }

    .v-application a {
        color: var(--v-anchor-base);
    }

    .v-application .primary {
        background-color: var(--v-primary-base) !important;
        border-color: var(--v-primary-base) !important;
    }

    .v-application .primary--text {
        color: var(--v-primary-base) !important;
        caret-color: var(--v-primary-base) !important;
    }

    .v-application .primary.lighten-5 {
        background-color: var(--v-primary-lighten5) !important;
        border-color: var(--v-primary-lighten5) !important;
    }

    .v-application .primary--text.text--lighten-5 {
        color: var(--v-primary-lighten5) !important;
        caret-color: var(--v-primary-lighten5) !important;
    }

    .v-application .primary.lighten-4 {
        background-color: var(--v-primary-lighten4) !important;
        border-color: var(--v-primary-lighten4) !important;
    }

    .v-application .primary--text.text--lighten-4 {
        color: var(--v-primary-lighten4) !important;
        caret-color: var(--v-primary-lighten4) !important;
    }

    .v-application .primary.lighten-3 {
        background-color: var(--v-primary-lighten3) !important;
        border-color: var(--v-primary-lighten3) !important;
    }

    .v-application .primary--text.text--lighten-3 {
        color: var(--v-primary-lighten3) !important;
        caret-color: var(--v-primary-lighten3) !important;
    }

    .v-application .primary.lighten-2 {
        background-color: var(--v-primary-lighten2) !important;
        border-color: var(--v-primary-lighten2) !important;
    }

    .v-application .primary--text.text--lighten-2 {
        color: var(--v-primary-lighten2) !important;
        caret-color: var(--v-primary-lighten2) !important;
    }

    .v-application .primary.lighten-1 {
        background-color: var(--v-primary-lighten1) !important;
        border-color: var(--v-primary-lighten1) !important;
    }

    .v-application .primary--text.text--lighten-1 {
        color: var(--v-primary-lighten1) !important;
        caret-color: var(--v-primary-lighten1) !important;
    }

    .v-application .primary.darken-1 {
        background-color: var(--v-primary-darken1) !important;
        border-color: var(--v-primary-darken1) !important;
    }

    .v-application .primary--text.text--darken-1 {
        color: var(--v-primary-darken1) !important;
        caret-color: var(--v-primary-darken1) !important;
    }

    .v-application .primary.darken-2 {
        background-color: var(--v-primary-darken2) !important;
        border-color: var(--v-primary-darken2) !important;
    }

    .v-application .primary--text.text--darken-2 {
        color: var(--v-primary-darken2) !important;
        caret-color: var(--v-primary-darken2) !important;
    }

    .v-application .primary.darken-3 {
        background-color: var(--v-primary-darken3) !important;
        border-color: var(--v-primary-darken3) !important;
    }

    .v-application .primary--text.text--darken-3 {
        color: var(--v-primary-darken3) !important;
        caret-color: var(--v-primary-darken3) !important;
    }

    .v-application .primary.darken-4 {
        background-color: var(--v-primary-darken4) !important;
        border-color: var(--v-primary-darken4) !important;
    }

    .v-application .primary--text.text--darken-4 {
        color: var(--v-primary-darken4) !important;
        caret-color: var(--v-primary-darken4) !important;
    }

    .v-application .secondary {
        background-color: var(--v-secondary-base) !important;
        border-color: var(--v-secondary-base) !important;
    }

    .v-application .secondary--text {
        color: var(--v-secondary-base) !important;
        caret-color: var(--v-secondary-base) !important;
    }

    .v-application .secondary.lighten-5 {
        background-color: var(--v-secondary-lighten5) !important;
        border-color: var(--v-secondary-lighten5) !important;
    }

    .v-application .secondary--text.text--lighten-5 {
        color: var(--v-secondary-lighten5) !important;
        caret-color: var(--v-secondary-lighten5) !important;
    }

    .v-application .secondary.lighten-4 {
        background-color: var(--v-secondary-lighten4) !important;
        border-color: var(--v-secondary-lighten4) !important;
    }

    .v-application .secondary--text.text--lighten-4 {
        color: var(--v-secondary-lighten4) !important;
        caret-color: var(--v-secondary-lighten4) !important;
    }

    .v-application .secondary.lighten-3 {
        background-color: var(--v-secondary-lighten3) !important;
        border-color: var(--v-secondary-lighten3) !important;
    }

    .v-application .secondary--text.text--lighten-3 {
        color: var(--v-secondary-lighten3) !important;
        caret-color: var(--v-secondary-lighten3) !important;
    }

    .v-application .secondary.lighten-2 {
        background-color: var(--v-secondary-lighten2) !important;
        border-color: var(--v-secondary-lighten2) !important;
    }

    .v-application .secondary--text.text--lighten-2 {
        color: var(--v-secondary-lighten2) !important;
        caret-color: var(--v-secondary-lighten2) !important;
    }

    .v-application .secondary.lighten-1 {
        background-color: var(--v-secondary-lighten1) !important;
        border-color: var(--v-secondary-lighten1) !important;
    }

    .v-application .secondary--text.text--lighten-1 {
        color: var(--v-secondary-lighten1) !important;
        caret-color: var(--v-secondary-lighten1) !important;
    }

    .v-application .secondary.darken-1 {
        background-color: var(--v-secondary-darken1) !important;
        border-color: var(--v-secondary-darken1) !important;
    }

    .v-application .secondary--text.text--darken-1 {
        color: var(--v-secondary-darken1) !important;
        caret-color: var(--v-secondary-darken1) !important;
    }

    .v-application .secondary.darken-2 {
        background-color: var(--v-secondary-darken2) !important;
        border-color: var(--v-secondary-darken2) !important;
    }

    .v-application .secondary--text.text--darken-2 {
        color: var(--v-secondary-darken2) !important;
        caret-color: var(--v-secondary-darken2) !important;
    }

    .v-application .secondary.darken-3 {
        background-color: var(--v-secondary-darken3) !important;
        border-color: var(--v-secondary-darken3) !important;
    }

    .v-application .secondary--text.text--darken-3 {
        color: var(--v-secondary-darken3) !important;
        caret-color: var(--v-secondary-darken3) !important;
    }

    .v-application .secondary.darken-4 {
        background-color: var(--v-secondary-darken4) !important;
        border-color: var(--v-secondary-darken4) !important;
    }

    .v-application .secondary--text.text--darken-4 {
        color: var(--v-secondary-darken4) !important;
        caret-color: var(--v-secondary-darken4) !important;
    }

    .v-application .accent {
        background-color: var(--v-accent-base) !important;
        border-color: var(--v-accent-base) !important;
    }

    .v-application .accent--text {
        color: var(--v-accent-base) !important;
        caret-color: var(--v-accent-base) !important;
    }

    .v-application .accent.lighten-5 {
        background-color: var(--v-accent-lighten5) !important;
        border-color: var(--v-accent-lighten5) !important;
    }

    .v-application .accent--text.text--lighten-5 {
        color: var(--v-accent-lighten5) !important;
        caret-color: var(--v-accent-lighten5) !important;
    }

    .v-application .accent.lighten-4 {
        background-color: var(--v-accent-lighten4) !important;
        border-color: var(--v-accent-lighten4) !important;
    }

    .v-application .accent--text.text--lighten-4 {
        color: var(--v-accent-lighten4) !important;
        caret-color: var(--v-accent-lighten4) !important;
    }

    .v-application .accent.lighten-3 {
        background-color: var(--v-accent-lighten3) !important;
        border-color: var(--v-accent-lighten3) !important;
    }

    .v-application .accent--text.text--lighten-3 {
        color: var(--v-accent-lighten3) !important;
        caret-color: var(--v-accent-lighten3) !important;
    }

    .v-application .accent.lighten-2 {
        background-color: var(--v-accent-lighten2) !important;
        border-color: var(--v-accent-lighten2) !important;
    }

    .v-application .accent--text.text--lighten-2 {
        color: var(--v-accent-lighten2) !important;
        caret-color: var(--v-accent-lighten2) !important;
    }

    .v-application .accent.lighten-1 {
        background-color: var(--v-accent-lighten1) !important;
        border-color: var(--v-accent-lighten1) !important;
    }

    .v-application .accent--text.text--lighten-1 {
        color: var(--v-accent-lighten1) !important;
        caret-color: var(--v-accent-lighten1) !important;
    }

    .v-application .accent.darken-1 {
        background-color: var(--v-accent-darken1) !important;
        border-color: var(--v-accent-darken1) !important;
    }

    .v-application .accent--text.text--darken-1 {
        color: var(--v-accent-darken1) !important;
        caret-color: var(--v-accent-darken1) !important;
    }

    .v-application .accent.darken-2 {
        background-color: var(--v-accent-darken2) !important;
        border-color: var(--v-accent-darken2) !important;
    }

    .v-application .accent--text.text--darken-2 {
        color: var(--v-accent-darken2) !important;
        caret-color: var(--v-accent-darken2) !important;
    }

    .v-application .accent.darken-3 {
        background-color: var(--v-accent-darken3) !important;
        border-color: var(--v-accent-darken3) !important;
    }

    .v-application .accent--text.text--darken-3 {
        color: var(--v-accent-darken3) !important;
        caret-color: var(--v-accent-darken3) !important;
    }

    .v-application .accent.darken-4 {
        background-color: var(--v-accent-darken4) !important;
        border-color: var(--v-accent-darken4) !important;
    }

    .v-application .accent--text.text--darken-4 {
        color: var(--v-accent-darken4) !important;
        caret-color: var(--v-accent-darken4) !important;
    }

    .v-application .error {
        background-color: var(--v-error-base) !important;
        border-color: var(--v-error-base) !important;
    }

    .v-application .error--text {
        color: var(--v-error-base) !important;
        caret-color: var(--v-error-base) !important;
    }

    .v-application .error.lighten-5 {
        background-color: var(--v-error-lighten5) !important;
        border-color: var(--v-error-lighten5) !important;
    }

    .v-application .error--text.text--lighten-5 {
        color: var(--v-error-lighten5) !important;
        caret-color: var(--v-error-lighten5) !important;
    }

    .v-application .error.lighten-4 {
        background-color: var(--v-error-lighten4) !important;
        border-color: var(--v-error-lighten4) !important;
    }

    .v-application .error--text.text--lighten-4 {
        color: var(--v-error-lighten4) !important;
        caret-color: var(--v-error-lighten4) !important;
    }

    .v-application .error.lighten-3 {
        background-color: var(--v-error-lighten3) !important;
        border-color: var(--v-error-lighten3) !important;
    }

    .v-application .error--text.text--lighten-3 {
        color: var(--v-error-lighten3) !important;
        caret-color: var(--v-error-lighten3) !important;
    }

    .v-application .error.lighten-2 {
        background-color: var(--v-error-lighten2) !important;
        border-color: var(--v-error-lighten2) !important;
    }

    .v-application .error--text.text--lighten-2 {
        color: var(--v-error-lighten2) !important;
        caret-color: var(--v-error-lighten2) !important;
    }

    .v-application .error.lighten-1 {
        background-color: var(--v-error-lighten1) !important;
        border-color: var(--v-error-lighten1) !important;
    }

    .v-application .error--text.text--lighten-1 {
        color: var(--v-error-lighten1) !important;
        caret-color: var(--v-error-lighten1) !important;
    }

    .v-application .error.darken-1 {
        background-color: var(--v-error-darken1) !important;
        border-color: var(--v-error-darken1) !important;
    }

    .v-application .error--text.text--darken-1 {
        color: var(--v-error-darken1) !important;
        caret-color: var(--v-error-darken1) !important;
    }

    .v-application .error.darken-2 {
        background-color: var(--v-error-darken2) !important;
        border-color: var(--v-error-darken2) !important;
    }

    .v-application .error--text.text--darken-2 {
        color: var(--v-error-darken2) !important;
        caret-color: var(--v-error-darken2) !important;
    }

    .v-application .error.darken-3 {
        background-color: var(--v-error-darken3) !important;
        border-color: var(--v-error-darken3) !important;
    }

    .v-application .error--text.text--darken-3 {
        color: var(--v-error-darken3) !important;
        caret-color: var(--v-error-darken3) !important;
    }

    .v-application .error.darken-4 {
        background-color: var(--v-error-darken4) !important;
        border-color: var(--v-error-darken4) !important;
    }

    .v-application .error--text.text--darken-4 {
        color: var(--v-error-darken4) !important;
        caret-color: var(--v-error-darken4) !important;
    }

    .v-application .info {
        background-color: var(--v-info-base) !important;
        border-color: var(--v-info-base) !important;
    }

    .v-application .info--text {
        color: var(--v-info-base) !important;
        caret-color: var(--v-info-base) !important;
    }

    .v-application .info.lighten-5 {
        background-color: var(--v-info-lighten5) !important;
        border-color: var(--v-info-lighten5) !important;
    }

    .v-application .info--text.text--lighten-5 {
        color: var(--v-info-lighten5) !important;
        caret-color: var(--v-info-lighten5) !important;
    }

    .v-application .info.lighten-4 {
        background-color: var(--v-info-lighten4) !important;
        border-color: var(--v-info-lighten4) !important;
    }

    .v-application .info--text.text--lighten-4 {
        color: var(--v-info-lighten4) !important;
        caret-color: var(--v-info-lighten4) !important;
    }

    .v-application .info.lighten-3 {
        background-color: var(--v-info-lighten3) !important;
        border-color: var(--v-info-lighten3) !important;
    }

    .v-application .info--text.text--lighten-3 {
        color: var(--v-info-lighten3) !important;
        caret-color: var(--v-info-lighten3) !important;
    }

    .v-application .info.lighten-2 {
        background-color: var(--v-info-lighten2) !important;
        border-color: var(--v-info-lighten2) !important;
    }

    .v-application .info--text.text--lighten-2 {
        color: var(--v-info-lighten2) !important;
        caret-color: var(--v-info-lighten2) !important;
    }

    .v-application .info.lighten-1 {
        background-color: var(--v-info-lighten1) !important;
        border-color: var(--v-info-lighten1) !important;
    }

    .v-application .info--text.text--lighten-1 {
        color: var(--v-info-lighten1) !important;
        caret-color: var(--v-info-lighten1) !important;
    }

    .v-application .info.darken-1 {
        background-color: var(--v-info-darken1) !important;
        border-color: var(--v-info-darken1) !important;
    }

    .v-application .info--text.text--darken-1 {
        color: var(--v-info-darken1) !important;
        caret-color: var(--v-info-darken1) !important;
    }

    .v-application .info.darken-2 {
        background-color: var(--v-info-darken2) !important;
        border-color: var(--v-info-darken2) !important;
    }

    .v-application .info--text.text--darken-2 {
        color: var(--v-info-darken2) !important;
        caret-color: var(--v-info-darken2) !important;
    }

    .v-application .info.darken-3 {
        background-color: var(--v-info-darken3) !important;
        border-color: var(--v-info-darken3) !important;
    }

    .v-application .info--text.text--darken-3 {
        color: var(--v-info-darken3) !important;
        caret-color: var(--v-info-darken3) !important;
    }

    .v-application .info.darken-4 {
        background-color: var(--v-info-darken4) !important;
        border-color: var(--v-info-darken4) !important;
    }

    .v-application .info--text.text--darken-4 {
        color: var(--v-info-darken4) !important;
        caret-color: var(--v-info-darken4) !important;
    }

    .v-application .success {
        background-color: var(--v-success-base) !important;
        border-color: var(--v-success-base) !important;
    }

    .v-application .success--text {
        color: var(--v-success-base) !important;
        caret-color: var(--v-success-base) !important;
    }

    .v-application .success.lighten-5 {
        background-color: var(--v-success-lighten5) !important;
        border-color: var(--v-success-lighten5) !important;
    }

    .v-application .success--text.text--lighten-5 {
        color: var(--v-success-lighten5) !important;
        caret-color: var(--v-success-lighten5) !important;
    }

    .v-application .success.lighten-4 {
        background-color: var(--v-success-lighten4) !important;
        border-color: var(--v-success-lighten4) !important;
    }

    .v-application .success--text.text--lighten-4 {
        color: var(--v-success-lighten4) !important;
        caret-color: var(--v-success-lighten4) !important;
    }

    .v-application .success.lighten-3 {
        background-color: var(--v-success-lighten3) !important;
        border-color: var(--v-success-lighten3) !important;
    }

    .v-application .success--text.text--lighten-3 {
        color: var(--v-success-lighten3) !important;
        caret-color: var(--v-success-lighten3) !important;
    }

    .v-application .success.lighten-2 {
        background-color: var(--v-success-lighten2) !important;
        border-color: var(--v-success-lighten2) !important;
    }

    .v-application .success--text.text--lighten-2 {
        color: var(--v-success-lighten2) !important;
        caret-color: var(--v-success-lighten2) !important;
    }

    .v-application .success.lighten-1 {
        background-color: var(--v-success-lighten1) !important;
        border-color: var(--v-success-lighten1) !important;
    }

    .v-application .success--text.text--lighten-1 {
        color: var(--v-success-lighten1) !important;
        caret-color: var(--v-success-lighten1) !important;
    }

    .v-application .success.darken-1 {
        background-color: var(--v-success-darken1) !important;
        border-color: var(--v-success-darken1) !important;
    }

    .v-application .success--text.text--darken-1 {
        color: var(--v-success-darken1) !important;
        caret-color: var(--v-success-darken1) !important;
    }

    .v-application .success.darken-2 {
        background-color: var(--v-success-darken2) !important;
        border-color: var(--v-success-darken2) !important;
    }

    .v-application .success--text.text--darken-2 {
        color: var(--v-success-darken2) !important;
        caret-color: var(--v-success-darken2) !important;
    }

    .v-application .success.darken-3 {
        background-color: var(--v-success-darken3) !important;
        border-color: var(--v-success-darken3) !important;
    }

    .v-application .success--text.text--darken-3 {
        color: var(--v-success-darken3) !important;
        caret-color: var(--v-success-darken3) !important;
    }

    .v-application .success.darken-4 {
        background-color: var(--v-success-darken4) !important;
        border-color: var(--v-success-darken4) !important;
    }

    .v-application .success--text.text--darken-4 {
        color: var(--v-success-darken4) !important;
        caret-color: var(--v-success-darken4) !important;
    }

    .v-application .warning {
        background-color: var(--v-warning-base) !important;
        border-color: var(--v-warning-base) !important;
    }

    .v-application .warning--text {
        color: var(--v-warning-base) !important;
        caret-color: var(--v-warning-base) !important;
    }

    .v-application .warning.lighten-5 {
        background-color: var(--v-warning-lighten5) !important;
        border-color: var(--v-warning-lighten5) !important;
    }

    .v-application .warning--text.text--lighten-5 {
        color: var(--v-warning-lighten5) !important;
        caret-color: var(--v-warning-lighten5) !important;
    }

    .v-application .warning.lighten-4 {
        background-color: var(--v-warning-lighten4) !important;
        border-color: var(--v-warning-lighten4) !important;
    }

    .v-application .warning--text.text--lighten-4 {
        color: var(--v-warning-lighten4) !important;
        caret-color: var(--v-warning-lighten4) !important;
    }

    .v-application .warning.lighten-3 {
        background-color: var(--v-warning-lighten3) !important;
        border-color: var(--v-warning-lighten3) !important;
    }

    .v-application .warning--text.text--lighten-3 {
        color: var(--v-warning-lighten3) !important;
        caret-color: var(--v-warning-lighten3) !important;
    }

    .v-application .warning.lighten-2 {
        background-color: var(--v-warning-lighten2) !important;
        border-color: var(--v-warning-lighten2) !important;
    }

    .v-application .warning--text.text--lighten-2 {
        color: var(--v-warning-lighten2) !important;
        caret-color: var(--v-warning-lighten2) !important;
    }

    .v-application .warning.lighten-1 {
        background-color: var(--v-warning-lighten1) !important;
        border-color: var(--v-warning-lighten1) !important;
    }

    .v-application .warning--text.text--lighten-1 {
        color: var(--v-warning-lighten1) !important;
        caret-color: var(--v-warning-lighten1) !important;
    }

    .v-application .warning.darken-1 {
        background-color: var(--v-warning-darken1) !important;
        border-color: var(--v-warning-darken1) !important;
    }

    .v-application .warning--text.text--darken-1 {
        color: var(--v-warning-darken1) !important;
        caret-color: var(--v-warning-darken1) !important;
    }

    .v-application .warning.darken-2 {
        background-color: var(--v-warning-darken2) !important;
        border-color: var(--v-warning-darken2) !important;
    }

    .v-application .warning--text.text--darken-2 {
        color: var(--v-warning-darken2) !important;
        caret-color: var(--v-warning-darken2) !important;
    }

    .v-application .warning.darken-3 {
        background-color: var(--v-warning-darken3) !important;
        border-color: var(--v-warning-darken3) !important;
    }

    .v-application .warning--text.text--darken-3 {
        color: var(--v-warning-darken3) !important;
        caret-color: var(--v-warning-darken3) !important;
    }

    .v-application .warning.darken-4 {
        background-color: var(--v-warning-darken4) !important;
        border-color: var(--v-warning-darken4) !important;
    }

    .v-application .warning--text.text--darken-4 {
        color: var(--v-warning-darken4) !important;
        caret-color: var(--v-warning-darken4) !important;
    }</style>

    <script src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script src="../../static/app/js/nav.js"></script>

    <script type="text/javascript" src="../../static/app/js/iscroll.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            getFarmlandEast();
            getFarmlandSouth();
            getFarmlandWest();
            getFarmlandNorth();
            getFarnlandName();
            getProductionIndex();
            getRemindCount();
            getCropPlanting();
            getConsumption();
            getConsumptionMonth();
        })

        function getConsumptionMonth()
        {
            $.ajax({
                url:'/IndexController/getConsumptionIndexMonth',
                type:'get',
                contentType:'application/json',
                success:function(res)
                {

                    document.getElementById("totalWaterConsumptionPerMonth").innerHTML="消耗("+res.data[0].unit+")";
                    document.getElementById("averageWaterConsumptionPerMonth").innerHTML="平均值("+res.data[0].unit+")";
                    document.getElementById("perMonthWaterTotalConsumption").innerHTML=res.data[0].perMonth;
                    document.getElementById("averageMonthWaterConsumption").innerHTML=res.data[0].ave;


                    document.getElementById("totalElectricConsumption").innerHTML="消耗("+res.data[1].unit+")";
                    document.getElementById("averageElectricConsumption").innerHTML="平均值("+res.data[1].unit+")";
                    document.getElementById("perMonthElectricTotalConsumption").innerHTML=res.data[1].perMonth;
                    document.getElementById("averageMonthElectricConsumption").innerHTML=res.data[1].ave;


                }
            })
        }

        function getConsumption()
        {

            $.ajax({
                url:'/IndexController/getConsumptionIndex',
                type:'get',
                contentType:'application/json',
                success:function(res)
                {

                        document.getElementById("liter").innerHTML=res.data[0].perUnit+res.data[0].unit;
                        document.getElementById("kilowatt").innerHTML=res.data[1].perUnit+res.data[1].unit;

                }
            })
        }

        function getCropPlanting()
        {
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getCropPlanting',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    document.getElementById("count").innerHTML=res.data[0].count;
                    for(i=0;i<res.data.length;i++)
                    {
                        if(res.data[i].varietyName=='包菜')
                        {
                            var data={"varietyName":res.data[i].varietyName}
                            content= '<a data-v-789dbbe9="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'" class="gallery__item v-card v-card--link v-card--outlined v-sheet theme--light" tabindex="0" style="width:150px;">';
                            content+= '<div data-v-789dbbe9="" class="v-responsive v-image white--text align-end">';
                            content+= '<div class="v-responsive__sizer"style="padding-bottom: 90.9091%;"></div>';
                            content+= '<div class="v-image__image v-image__image--cover"style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/baocai.jpg&quot;); background-position: center center;"></div>';
                            content+= '<div class="v-responsive__content">';
                            content+= '<div data-v-789dbbe9="" class="gallery__item-content">';
                            content+= '<div data-v-789dbbe9="" class="v-card__title gallery__item-content__title">';
                            content+= res.data[i].varietyName;
                            content+='</div>';
                            content+= '<div data-v-789dbbe9="" class="v-card__text gallery__item-content__desc">';
                            content+= '栽种田：'
                            content+=res.data[i].farmlandName;
                            content+='</div>';
                            content+= '</div></div></div></div></a>';
                            $('#scroller').append(content);
                        }
                        else if(res.data[i].varietyName=='茄子')
                        {
                            var data={"varietyName":res.data[i].varietyName}
                            content= '<a data-v-789dbbe9="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'" class="gallery__item v-card v-card--link v-card--outlined v-sheet theme--light" tabindex="0" style="width:150px;">';
                            content+= '<div data-v-789dbbe9="" class="v-responsive v-image white--text align-end">';
                            content+= '<div class="v-responsive__sizer"style="padding-bottom: 90.9091%;"></div>';
                            content+= '<div class="v-image__image v-image__image--cover"style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/qiezi.jpg&quot;); background-position: center center;"></div>';
                            content+= '<div class="v-responsive__content">';
                            content+= '<div data-v-789dbbe9="" class="gallery__item-content">';
                            content+= '<div data-v-789dbbe9="" class="v-card__title gallery__item-content__title">';
                            content+= res.data[i].varietyName;
                            content+='</div>';
                            content+= '<div data-v-789dbbe9="" class="v-card__text gallery__item-content__desc">';
                            content+= '栽种田：'
                            content+=res.data[i].farmlandName;
                            content+='</div>';
                            content+= '</div></div></div></div></a>';
                            $('#scroller').append(content);
                        }
                        else
                        {
                            var data={"varietyName":res.data[i].varietyName}
                            content= '<a data-v-789dbbe9="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'" class="gallery__item v-card v-card--link v-card--outlined v-sheet theme--light" tabindex="0" style="width:150px;">';
                            content+= '<div data-v-789dbbe9="" class="v-responsive v-image white--text align-end">';
                            content+= '<div class="v-responsive__sizer"style="padding-bottom: 90.9091%;"></div>';
                            content+= '<div class="v-image__image v-image__image--cover"style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/baocai.jpg&quot;); background-position: center center;"></div>';
                            content+= '<div class="v-responsive__content">';
                            content+= '<div data-v-789dbbe9="" class="gallery__item-content">';
                            content+= '<div data-v-789dbbe9="" class="v-card__title gallery__item-content__title">';
                            content+= res.data[i].varietyName;
                            content+='</div>';
                            content+= '<div data-v-789dbbe9="" class="v-card__text gallery__item-content__desc">';
                            content+= '栽种田：'
                            content+=res.data[i].farmlandName;
                            content+='</div>';
                            content+= '</div></div></div></div></a>';
                            $('#scroller').append(content);
                        }
                    }
                }
            })
        }

        function getRemindCount()
        {
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getRemindCount',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res) {
                    for (i = 0; i < res.data.length; i++)
                    {
                        if(res.data[i].type==1)
                        {
                            document.getElementById("farmRemind").innerHTML=res.data[i].count;
                        }
                        else if(res.data[i].type==2)
                        {
                            document.getElementById("indexRemind").innerHTML=res.data[i].count;
                        }
                        else if(res.data[i].type==3)
                        {
                            document.getElementById("disasterRemind").innerHTML=res.data[i].count;
                        }
                    }
                }
            })
        }

        function getProductionIndex()
        {
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getProductionIndex',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res) {
                    for (i = 0; i < res.data.length; i++)
                    {
                        document.getElementById("batchCount").innerHTML=res.data[i].batchCount;
                        document.getElementById("harvestCount").innerHTML=res.data[i].harvestCount;
                        document.getElementById("output").innerHTML=res.data[i].output;
                    }
                }
            })
        }


        function getFarnlandName(){
            if("${data.flag}"=="true"){
                document.getElementById("farmlandName1").innerHTML="${data.farmlandName}";
            }
        }
        function getFarmlandEast(){
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getIndexFarmlandEast',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    document.getElementById("east1").innerHTML="东区("+res.data.length+")";
                    for(i=res.data.length-1;i>=0;i--){
                        var data={"farmlandName":res.data[i].farmlandName,"flag":"true"};
                        content=  '<div data-v-315f272a="">';
                        content+= '<a href="/IndexController/getIndexName?data='+encodeURIComponent(JSON.stringify(data))+'" class="router-link-exact-active router-link-active v-card v-card--link v-sheet theme--light mt-2" tabindex="0">';
                        content+= '<div tabindex="-1" class="v-list-item theme--light">';
                        content+= '<div class="v-avatar v-list-item__avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<span class="v-badge v-badge--overlap" right="">';
                        content+= '<div class="v-avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<div class="v-responsive v-image">';
                        content+= '<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content+= '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content+= '<div class="v-responsive__content" style="width: 1080px;"></div></div></div><!----></span></div>';
                        content+= '<div class="v-list-item__content align-self-start">';
                        content+= '<div class="v-list-item__title title grey--text text--darken-2">';
                        content+= res.data[i].farmlandName;
                        content+= '</div><div class="container pa-0"><div class="row no-gutters"><div class="body-2 grey--text col">认养人</div><div class="body-1 col" id="farmerName">';
                        content+= res.data[i].farmerName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">栽培</div><div class="body-1 col" id="varietyName">';
                        content+= res.data[i].varietyName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">生长状况</div><div class="body-1 col" id="growthStatus">';
                        content+= res.data[i].growthStatus;
                        content+= '</div></div></div></div></div></a></div>';
                        $('#east2').append(content);
                    }
                }
            })
        }
        function getFarmlandSouth(){
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getIndexFarmlandSouth',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    document.getElementById("south1").innerHTML="南区("+res.data.length+")";
                    for(i=res.data.length-1;i>=0;i--){
                        var data={"farmlandName":res.data[i].farmlandName,"flag":"true"};
                        content=  '<div data-v-315f272a="">';
                        content+= '<a href="/IndexController/getIndexName?data='+encodeURIComponent(JSON.stringify(data))+'" class="router-link-exact-active router-link-active v-card v-card--link v-sheet theme--light mt-2" tabindex="0">';
                        content+= '<div tabindex="-1" class="v-list-item theme--light">';
                        content+= '<div class="v-avatar v-list-item__avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<span class="v-badge v-badge--overlap" right="">';
                        content+= '<div class="v-avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<div class="v-responsive v-image">';
                        content+= '<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content+= '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content+= '<div class="v-responsive__content" style="width: 1080px;"></div></div></div><!----></span></div>';
                        content+= '<div class="v-list-item__content align-self-start">';
                        content+= '<div class="v-list-item__title title grey--text text--darken-2">';
                        content+= res.data[i].farmlandName;
                        content+= '</div><div class="container pa-0"><div class="row no-gutters"><div class="body-2 grey--text col">认养人</div><div class="body-1 col" id="farmerName">';
                        content+= res.data[i].farmerName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">栽培</div><div class="body-1 col" id="varietyName">';
                        content+= res.data[i].varietyName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">生长状况</div><div class="body-1 col" id="growthStatus">';
                        content+= res.data[i].growthStatus;
                        content+= '</div></div></div></div></div></a></div>';
                        $('#south2').append(content);
                    }
                }
            })
        }
        function getFarmlandWest(){
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getIndexFarmlandWest',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    document.getElementById("west1").innerHTML="西区("+res.data.length+")";
                    for(i=res.data.length-1;i>=0;i--){
                        var data={"farmlandName":res.data[i].farmlandName,"flag":"true"};
                        content=  '<div data-v-315f272a="">';
                        content+= '<a href="/IndexController/getIndexName?data='+encodeURIComponent(JSON.stringify(data))+'" class="router-link-exact-active router-link-active v-card v-card--link v-sheet theme--light mt-2" tabindex="0">';
                        content+= '<div tabindex="-1" class="v-list-item theme--light">';
                        content+= '<div class="v-avatar v-list-item__avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<span class="v-badge v-badge--overlap" right="">';
                        content+= '<div class="v-avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<div class="v-responsive v-image">';
                        content+= '<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content+= '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content+= '<div class="v-responsive__content" style="width: 1080px;"></div></div></div><!----></span></div>';
                        content+= '<div class="v-list-item__content align-self-start">';
                        content+= '<div class="v-list-item__title title grey--text text--darken-2">';
                        content+= res.data[i].farmlandName;
                        content+= '</div><div class="container pa-0"><div class="row no-gutters"><div class="body-2 grey--text col">认养人</div><div class="body-1 col" id="farmerName">';
                        content+= res.data[i].farmerName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">栽培</div><div class="body-1 col" id="varietyName">';
                        content+= res.data[i].varietyName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">生长状况</div><div class="body-1 col" id="growthStatus">';
                        content+= res.data[i].growthStatus;
                        content+= '</div></div></div></div></div></a></div>';
                        $('#west2').append(content);
                    }
                }
            })
        }
        function getFarmlandNorth(){
            var login={"login":'123'}
            $.ajax({
                url:'/IndexController/getIndexFarmlandNorth',
                data:JSON.stringify(login),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    document.getElementById("north1").innerHTML="北区("+res.data.length+")";
                    for(i=res.data.length-1;i>=0;i--){
                        var data={"farmlandName":res.data[i].farmlandName,"flag":"true"};
                        content=  '<div data-v-315f272a="">';
                        content+= '<a href="/IndexController/getIndexName?data='+encodeURIComponent(JSON.stringify(data))+'" class="router-link-exact-active router-link-active v-card v-card--link v-sheet theme--light mt-2" tabindex="0">';
                        content+= '<div tabindex="-1" class="v-list-item theme--light">';
                        content+= '<div class="v-avatar v-list-item__avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<span class="v-badge v-badge--overlap" right="">';
                        content+= '<div class="v-avatar v-avatar--tile" style="height: 96px; min-width: 96px; width: 96px;">';
                        content+= '<div class="v-responsive v-image">';
                        content+= '<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content+= '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content+= '<div class="v-responsive__content" style="width: 1080px;"></div></div></div><!----></span></div>';
                        content+= '<div class="v-list-item__content align-self-start">';
                        content+= '<div class="v-list-item__title title grey--text text--darken-2">';
                        content+= res.data[i].farmlandName;
                        content+= '</div><div class="container pa-0"><div class="row no-gutters"><div class="body-2 grey--text col">认养人</div><div class="body-1 col" id="farmerName">';
                        content+= res.data[i].farmerName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">栽培</div><div class="body-1 col" id="varietyName">';
                        content+= res.data[i].varietyName;
                        content+= '</div></div><div class="row no-gutters"><div class="body-2 grey--text col">生长状况</div><div class="body-1 col" id="growthStatus">';
                        content+= res.data[i].growthStatus;
                        content+= '</div></div></div></div></div></a></div>';
                        $('#north2').append(content);
                    }
                }
            })
        }



        function change(){
            var aScroll;
            var bScroll;
            var wrapper;
            var arrowUp;
            var arrowRight;
            wrapper=$('#wrapper');
            arrowUp=$("#arrowUp");
            arrowRight=$('#arrowRight');
            var c=document.getElementById('wrapper').className;
            if(c=="detail-layer animate transparent"){
                aScroll = new IScroll('#wrapper', {scrollX: false, scrollY: true, mouseWheel: false ,keyBindings: true });
                bScroll = new IScroll('#about', { eventPassthrough: true, scrollX: true, scrollY: false });
                wrapper.addClass('popup');
                arrowUp.css('display','none');
                arrowRight.css('display','block');
            }else{
                aScroll = new IScroll('#wrapper', {scrollX: false, scrollY: false, mouseWheel: false ,keyBindings: true });
                wrapper.removeClass('popup');
                arrowUp.css('display','block');
                arrowRight.css('display','none');
            }
        }
        function choiceField(){
            var outLayer=$('#outLayer');
            var inLayer=$('#inLayer');
            outLayer.addClass('v-dialog__content--active');
            inLayer.addClass('v-dialog--active');
            inLayer.css('top','0px');
        }
        function cancel(){
            var outLayer=$('#outLayer');
            var inLayer=$('#inLayer');
            outLayer.removeClass('v-dialog__content--active');
            inLayer.removeClass('v-dialog--active');
            inLayer.css('top','1500px');
        }
        function changeEast(){
            var east1=$('#east1');
            var south1=$('#south1');
            var west1=$('#west1');
            var north1=$('#north1');
            var east2=$('#east2');
            var south2=$('#south2');
            var west2=$('#west2');
            var north2=$('#north2');
            var line=$('#line');
            var e=document.getElementById('east1').className;
            var s=document.getElementById('south1').className;
            var w=document.getElementById('west1').className;
            var n=document.getElementById('north1').className;
            if(e=="v-tab"){
                east1.addClass('v-tab--active');
                east1.attr('aria-selected','true');
                line.css('left','0%');
                east2.addClass('v-window-item--active');
                east2.css('display','block');
                if(s=="v-tab v-tab--active"){
                    south1.removeClass('v-tab--active');
                    south1.attr('aria-selected','false');
                    south2.removeClass('v-window-item--active');
                    south2.css('display','none');
                }
                if(w=="v-tab v-tab--active"){
                    west1.removeClass('v-tab--active')
                    west1.attr('aria-selected','false');
                    west2.removeClass('v-window-item--active');
                    west2.css('display','none');
                }
                if(n=="v-tab v-tab--active"){
                    north1.removeClass('v-tab--active');
                    north1.attr('aria-selected','false');
                    north2.removeClass('v-window-item--active');
                    north2.css('display','none');
                }
            }
        }
        function changeSouth(){
            var east1=$('#east1');
            var south1=$('#south1');
            var west1=$('#west1');
            var north1=$('#north1');
            var east2=$('#east2');
            var south2=$('#south2');
            var west2=$('#west2');
            var north2=$('#north2');
            var line=$('#line');
            var e=document.getElementById('east1').className;
            var s=document.getElementById('south1').className;
            var w=document.getElementById('west1').className;
            var n=document.getElementById('north1').className;
            if(s=="v-tab"){
                south1.addClass('v-tab--active');
                south1.attr('aria-selected','true');
                line.css('left','25%');
                south2.addClass('v-window-item--active');
                south2.css('display','block');
                if(e=="v-tab v-tab--active"){
                    east1.removeClass('v-tab--active');
                    east1.attr('aria-selected','false');
                    east2.removeClass('v-window-item--active');
                    east2.css('display','none');
                }
                if(w=="v-tab v-tab--active"){
                    west1.removeClass('v-tab--active')
                    west1.attr('aria-selected','false');
                    west2.removeClass('v-window-item--active');
                    west2.css('display','none');
                }
                if(n=="v-tab v-tab--active"){
                    north1.removeClass('v-tab--active');
                    north1.attr('aria-selected','false');
                    north2.removeClass('v-window-item--active');
                    north2.css('display','none');
                }
            }
        }
        function changeWest(){
            var east1=$('#east1');
            var south1=$('#south1');
            var west1=$('#west1');
            var north1=$('#north1');
            var east2=$('#east2');
            var south2=$('#south2');
            var west2=$('#west2');
            var north2=$('#north2');
            var line=$('#line');
            var e=document.getElementById('east1').className;
            var s=document.getElementById('south1').className;
            var w=document.getElementById('west1').className;
            var n=document.getElementById('north1').className;
            if(w=="v-tab"){
                west1.addClass('v-tab--active');
                west1.attr('aria-selected','true');
                line.css('left','50%');
                west2.addClass('v-window-item--active');
                west2.css('display','block');
                if(s=="v-tab v-tab--active"){
                    south1.removeClass('v-tab--active');
                    south1.attr('aria-selected','false');
                    south2.removeClass('v-window-item--active');
                    south2.css('display','none');
                }
                if(e=="v-tab v-tab--active"){
                    east1.removeClass('v-tab--active')
                    east1.attr('aria-selected','false');
                    east2.removeClass('v-window-item--active');
                    east2.css('display','none');
                }
                if(n=="v-tab v-tab--active"){
                    north1.removeClass('v-tab--active');
                    north1.attr('aria-selected','false');
                    north2.removeClass('v-window-item--active');
                    north2.css('display','none');
                }
            }
        }
        function changeNorth(){
            var east1=$('#east1');
            var south1=$('#south1');
            var west1=$('#west1');
            var north1=$('#north1');
            var east2=$('#east2');
            var south2=$('#south2');
            var west2=$('#west2');
            var north2=$('#north2');
            var line=$('#line');
            var e=document.getElementById('east1').className;
            var s=document.getElementById('south1').className;
            var w=document.getElementById('west1').className;
            var n=document.getElementById('north1').className;
            if(n=="v-tab"){
                north1.addClass('v-tab--active');
                north1.attr('aria-selected','true');
                line.css('left','75%');
                north2.addClass('v-window-item--active');
                north2.css('display','block');
                if(s=="v-tab v-tab--active"){
                    south1.removeClass('v-tab--active');
                    south1.attr('aria-selected','false');
                    south2.removeClass('v-window-item--active');
                    south2.css('display','none');
                }
                if(w=="v-tab v-tab--active"){
                    west1.removeClass('v-tab--active')
                    west1.attr('aria-selected','false');
                    west2.removeClass('v-window-item--active');
                    west2.css('display','none');
                }
                if(e=="v-tab v-tab--active"){
                    east1.removeClass('v-tab--active');
                    east1.attr('aria-selected','false');
                    east2.removeClass('v-window-item--active');
                    east2.css('display','none');
                }
            }
        }
    </script>

    <style type="text/css">
        #wrapper {
            z-index: 1;
            top: 0px;
            bottom: 56px;
            left: 0;
            width: 100%;
            background: #ccc;
            overflow: hidden;
        }
        #about {
            position: relative;
            z-index: 1;
            right: 0;
            height: 134.54px;
            width: 100%;
            background: #FFFFFF;
            overflow: hidden;
            -ms-touch-action: none;
        }
        #scroller {
            left: 12px;
            position: absolute;
            z-index: 1;
            -webkit-tap-highlight-color: rgba(0,0,0,0);
            width: 1500px;
            height: 134.54px;
            -webkit-transform: translateZ(0);
            -moz-transform: translateZ(0);
            -ms-transform: translateZ(0);
            -o-transform: translateZ(0);
            transform: translateZ(0);
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-text-size-adjust: none;
            -moz-text-size-adjust: none;
            -ms-text-size-adjust: none;
            -o-text-size-adjust: none;
            text-size-adjust: none;
        }
        #scroller a {
            width: 150px;
            height: 134.54px;
            line-height: 134.54px;
            float: left;
            overflow: hidden;
        }
    </style>
</head>
<body>

<div data-app="true" class="v-application patch v-application--is-ltr theme--light">
    <div id="outLayer" role="document" data-v-315f272a="" class="v-dialog__content"
         style="z-index: 202;">
        <div id="inLayer" class="v-dialog v-dialog--fullscreen" style="position: absolute;left: 0px;top:1500px">
            <div data-v-315f272a="" class="v-card v-sheet v-sheet--tile theme--light">
                <header data-v-315f272a=""
                        class="v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-toolbar--extended primary">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-315f272a="" style="width:100%;font-size:18px;text-align:center">智慧田选择</div>
                        <button data-v-315f272a="" type="button"
                                class="body-1 v-btn v-btn--flat v-btn--text v-btn--tile theme--dark v-size--default"
                                style="height: 48px;position:absolute;top:0;right: 0" onclick="cancel()"><span class="v-btn__content">
          取消
        </span></button>
                    </div>
                    <div class="v-toolbar__extension" style="height: 48px;">
                        <div data-v-315f272a="" class="v-tabs v-tabs--grow theme--light">
                            <div role="tablist" class="v-item-group theme--light v-slide-group v-tabs-bar primary--text"
                                 data-booted="true">
                                <div class="v-slide-group__prev v-slide-group__prev--disabled"><!----></div>
                                <div class="v-slide-group__wrapper">
                                    <div class="v-slide-group__content v-tabs-bar__content">
                                        <div id="line" class="v-tabs-slider-wrapper"
                                             style="height: 2px; left: 0%; width: 25%;">
                                            <div class="v-tabs-slider"></div>
                                        </div>
                                        <button id="east1" style="height: 48px;width: 97px;" data-v-315f272a="" tabindex="0"
                                                aria-selected="true" role="tab" class="v-tab v-tab--active" onclick="changeEast()"></button><button
                                            id="south1" style="height: 48px;width: 97px;" data-v-315f272a="" tabindex="0"
                                            aria-selected="false" role="tab" class="v-tab" onclick="changeSouth()"></button><button
                                            id="west1" style="height: 48px;width: 97px;" data-v-315f272a="" tabindex="0"
                                            aria-selected="false" role="tab" class="v-tab" onclick="changeWest()"></button><button
                                            id="north1" style="height: 48px;width: 97px;" data-v-315f272a="" tabindex="0"
                                            aria-selected="false" role="tab" class="v-tab" onclick="changeNorth()"></button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div data-v-315f272a="" class="container">
                    <div data-v-315f272a="" class="v-window v-item-group theme--light v-tabs-items">
                        <div class="v-window__container">
                            <div data-v-315f272a="" class="v-window-item v-window-item--active" id="east2" style=""></div>
                            <div data-v-315f272a="" class="v-window-item" id="south2" style="display: none;"></div>
                            <div data-v-315f272a="" class="v-window-item" id="west2" style="display: none;"></div>
                            <div data-v-315f272a="" class="v-window-item" id="north2" style="display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">                                                                       <!--改成class="bgDiv"-->
            <div data-v-69aa1152="" class="host"><!---->
                <div data-v-60aca95c="" data-v-69aa1152="" class="layers">
                    <div data-v-b14f6c86="" data-v-60aca95c="" class="background-layer"><!----></div>
                    <div data-v-5b76eaaa="" data-v-60aca95c="" class="interaction-layer">

                        <div class="bgDiv"></div>

                        <div data-v-5b76eaaa="" class="app-bar align-center justify-space-between">
                            <button data-v-5b76eaaa="" type="button"
                                    class="left v-btn v-btn--flat v-btn--text v-btn--tile theme--light v-size--default white--text"
                                    style="height: 48px; min-width: 0px;"><span class="v-btn__content"><img
                                    data-v-5b76eaaa="" src="../../static/app/img/burger.c38b8526.svg" alt="icon"></span></button>
                            <button data-v-5b76eaaa="" type="button"
                                    class="appbar__title v-btn v-btn--flat v-btn--text v-btn--tile theme--light v-size--default white--text"
                                    style="height: 48px;" onclick="choiceField()"><span class="v-btn__content">
                                <div id="farmlandName1">小顾实验田</div>
      <i data-v-5b76eaaa="" aria-hidden="true"
         class="v-icon notranslate mdi mdi-chevron-down theme--light white--text"></i></span></button>
                            <button data-v-5b76eaaa="" type="button"
                                    class="v-btn v-btn--flat v-btn--text v-btn--tile theme--light v-size--default white--text"
                                    style="height: 48px; min-width: 0px;"><span class="v-btn__content"><img
                                    data-v-5b76eaaa="" src="../../static/app/img/scaner.be11b2f5.svg" alt="icon"></span></button>
                        </div>
                        <div data-v-1d64daa3="" data-v-5b76eaaa="" class="state state" style="display: none;"><img
                                data-v-1d64daa3="" src="../../static/app/img/speaker.4f433910.svg" alt="icon" class="state__icon">
                            <div data-v-1d64daa3="" class="state__text-container"><span data-v-1d64daa3=""
                                                                                        class="state__text"></span>
                            </div>
                        </div>
                        <div data-v-77d37b1b="" data-v-5b76eaaa="" class="card weather-card v-sheet theme--light">
                            <div data-v-77d37b1b="" class="layout data align-center">
                                <div data-v-77d37b1b="" class="flex shrink"><img data-v-77d37b1b=""
                                                                                 src="../../static/app/img/light-rain.54b3985e.svg"
                                                                                 alt="icon"></div>
                                <div data-v-77d37b1b="" class="flex value temp">
                                    25℃ / 17℃
                                </div>
                            </div>
                            <div data-v-77d37b1b="" class="data">
                                <div data-v-77d37b1b="" class="name">
                                    光照强度：
                                </div>
                                <div data-v-77d37b1b="" class="value">0<span data-v-77d37b1b="">Lx</span></div>
                            </div>
                            <div data-v-77d37b1b="" class="data">
                                <div data-v-77d37b1b="" class="name">
                                    空气温度：
                                </div>
                                <div data-v-77d37b1b="" class="value">
                                    19.9<span data-v-77d37b1b="">℃</span></div>
                            </div>
                            <div data-v-77d37b1b="" class="data">
                                <div data-v-77d37b1b="" class="name">
                                    空气湿度：
                                </div>
                                <div data-v-77d37b1b="" class="value">
                                    86.1<span data-v-77d37b1b="">%</span></div>
                            </div>
                        </div>
                        <div data-v-7d53d484="" data-v-5b76eaaa="" class="info-card info-card">
                            <div data-v-7d53d484="" class="info-card__title-wrapper">
                                <div data-v-7d53d484="" class="layout justify-space-between">
                                    <div data-v-7d53d484="" class="flex info-card__title"><img data-v-7d53d484=""
                                                                                               src="../../static/app/img/eye.d4a30fde.svg"
                                                                                               alt="icon">
                                        土壤监测
                                    </div>
                                    <div data-v-7d53d484="" class="flex info-card__subtitle shrink">
                                        19:49:45
                                    </div>
                                </div>
                            </div>
                            <div data-v-7d53d484="" class="row mt-2 no-gutters justify-space-between">
                                <div data-v-7d53d484="" class="col col-3">
                                    <div data-v-1030ca6a="" data-v-7d53d484=""
                                         class="layout indicator column align-center">
                                        <div data-v-1030ca6a="" class="flex">
                                            <div data-v-1030ca6a="" class="layout indicator__icon-wrapper align-center">
                                                <div data-v-1030ca6a="" class="flex indivator__icon-wrapper shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-temp.99352097.svg"
                                                        alt="icon" class="indivator__icon-wrapper__icon"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-border.0ff9e847.svg"
                                                        class="indivator__icon-wrapper__border rotate1"></div>
                                                <div data-v-1030ca6a="" class="flex ml-2 indicator__level shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/level-3.83254440.svg"
                                                        alt="level"></div>
                                            </div>
                                        </div>
                                        <div data-v-1030ca6a="" class="flex indicator__text text-no-wrap"><span
                                                data-v-1030ca6a="" class="indicator__title">温度</span>：<span
                                                data-v-1030ca6a="" class="indicator__value">21.7</span><span
                                                data-v-1030ca6a="" class="indicator__unit">℃</span><span
                                                data-v-1030ca6a=""
                                                class="indicator__arrow indicator__arrow--down">↓</span></div>
                                    </div>
                                </div>
                                <div data-v-7d53d484="" class="col col-3">
                                    <div data-v-1030ca6a="" data-v-7d53d484=""
                                         class="layout indicator column align-center">
                                        <div data-v-1030ca6a="" class="flex">
                                            <div data-v-1030ca6a="" class="layout indicator__icon-wrapper align-center">
                                                <div data-v-1030ca6a="" class="flex indivator__icon-wrapper shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-wet.2d008e0f.svg"
                                                        alt="icon" class="indivator__icon-wrapper__icon"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-border.0ff9e847.svg"
                                                        class="indivator__icon-wrapper__border rotate2"></div>
                                                <div data-v-1030ca6a="" class="flex ml-2 indicator__level shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/level-2.5bd77f75.svg"
                                                        alt="level"></div>
                                            </div>
                                        </div>
                                        <div data-v-1030ca6a="" class="flex indicator__text text-no-wrap"><span
                                                data-v-1030ca6a="" class="indicator__title">湿度</span>：<span
                                                data-v-1030ca6a="" class="indicator__value">19.3</span><span
                                                data-v-1030ca6a="" class="indicator__unit">%</span><span
                                                data-v-1030ca6a=""
                                                class="indicator__arrow indicator__arrow--down">↓</span></div>
                                    </div>
                                </div>
                                <div data-v-7d53d484="" class="col col-3">
                                    <div data-v-1030ca6a="" data-v-7d53d484=""
                                         class="layout indicator column align-center">
                                        <div data-v-1030ca6a="" class="flex">
                                            <div data-v-1030ca6a="" class="layout indicator__icon-wrapper align-center">
                                                <div data-v-1030ca6a="" class="flex indivator__icon-wrapper shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-ph.feeb4957.svg"
                                                        alt="icon" class="indivator__icon-wrapper__icon"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-border.0ff9e847.svg"
                                                        class="indivator__icon-wrapper__border rotate3"></div>
                                                <div data-v-1030ca6a="" class="flex ml-2 indicator__level shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/level-3.83254440.svg"
                                                        alt="level"></div>
                                            </div>
                                        </div>
                                        <div data-v-1030ca6a="" class="flex indicator__text text-no-wrap"><span
                                                data-v-1030ca6a="" class="indicator__title">PH</span>：<span
                                                data-v-1030ca6a="" class="indicator__value">7.0</span><span
                                                data-v-1030ca6a="" class="indicator__unit"></span><span
                                                data-v-1030ca6a=""
                                                class="indicator__arrow indicator__arrow--down">↓</span></div>
                                    </div>
                                </div>
                                <div data-v-7d53d484="" class="col col-3">
                                    <div data-v-1030ca6a="" data-v-7d53d484=""
                                         class="layout indicator column align-center">
                                        <div data-v-1030ca6a="" class="flex">
                                            <div data-v-1030ca6a="" class="layout indicator__icon-wrapper align-center">
                                                <div data-v-1030ca6a="" class="flex indivator__icon-wrapper shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-electric.859c016b.svg"
                                                        alt="icon" class="indivator__icon-wrapper__icon"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/soil-border.0ff9e847.svg"
                                                        class="indivator__icon-wrapper__border rotate4"></div>
                                                <div data-v-1030ca6a="" class="flex ml-2 indicator__level shrink"><img
                                                        data-v-1030ca6a="" src="../../static/app/img/level-2.5bd77f75.svg"
                                                        alt="level"></div>
                                            </div>
                                        </div>
                                        <div data-v-1030ca6a="" class="flex indicator__text text-no-wrap"><span
                                                data-v-1030ca6a="" class="indicator__title">EC</span>：<span
                                                data-v-1030ca6a="" class="indicator__value">0.41</span><span
                                                data-v-1030ca6a="" class="indicator__unit">mS/cm</span><span
                                                data-v-1030ca6a=""
                                                class="indicator__arrow indicator__arrow--down">↓</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-v-5b76eaaa="" class="layout actions align-end column">
                            <div class="flex">
                                <div data-v-7dbf30ce="" data-v-4e748b0b="" class="button">
                                    <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                         style="width: 40px; height: 40px; line-height: 40px;"><img data-v-7dbf30ce=""
                                                                                                    src="../../static/app/img/camera.icon.031a540d.svg"
                                                                                                    alt="action"
                                                                                                    class="button__icon"><img
                                            data-v-7dbf30ce="" src="../../static/app/img/camera-red.0cf2166e.svg" alt="state"
                                            class="button__state button__state--inactive"></div>
                                    <div data-v-7dbf30ce="" class="button__title">
                                        监控
                                    </div>
                                </div>
                            </div>
                            <div class="flex mt-4">
                                <div data-v-7dbf30ce="" class="button">
                                    <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                         style="width: 40px; height: 40px; line-height: 40px;"><img data-v-7dbf30ce=""
                                                                                                    src="../../static/app/img/detail.fb358965.svg"
                                                                                                    alt="action"
                                                                                                    class="button__icon">
                                    </div>
                                    <div data-v-7dbf30ce="" class="button__title">
                                        详情
                                    </div>
                                </div>
                            </div>
                            <div class="flex mt-4">
                                <div class="layout reverse">
                                    <div class="flex">
                                        <div data-v-7dbf30ce="" data-v-493821cc="" class="button">
                                            <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                                 style="width: 40px; height: 40px; line-height: 40px;"><img
                                                    data-v-7dbf30ce="" src="../../static/app/img/robot.icon.eb7b7d68.svg"
                                                    alt="action" class="button__icon"><img data-v-7dbf30ce=""
                                                                                           src="../../static/app/img/robot-eyes.6ad5d0c9.svg"
                                                                                           alt="state"
                                                                                           class="button__state"></div>
                                            <div data-v-7dbf30ce="" class="button__title">
                                                操作
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex mr-4">
                                        <div data-v-7dbf30ce="" data-v-f27c9ff4="" class="button">
                                            <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                                 style="width: 40px; height: 40px; line-height: 40px;"><img
                                                    data-v-7dbf30ce="" src="../../static/app/img/water.66a8c54f.svg" alt="action"
                                                    class="button__icon"><img data-v-7dbf30ce=""
                                                                              src="../../static/app/img/water-border.2e778666.svg"
                                                                              alt="state"
                                                                              class="button__state button__state--inactive">
                                            </div>
                                            <div data-v-7dbf30ce="" class="button__title">
                                                浇水
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex mr-4">
                                        <div data-v-7dbf30ce="" data-v-6b730a9b="" class="button">
                                            <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                                 style="width: 40px; height: 40px; line-height: 40px;"><img
                                                    data-v-7dbf30ce="" src="../../static/app/img/fertilizer.af00c774.svg"
                                                    alt="action" class="button__icon"><img data-v-7dbf30ce=""
                                                                                           src="../../static/app/img/fertilizer-border.b9d1f68a.svg"
                                                                                           alt="state"
                                                                                           class="button__state button__state--inactive">
                                            </div>
                                            <div data-v-7dbf30ce="" class="button__title">
                                                施肥
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex mr-4">
                                        <div data-v-7dbf30ce="" data-v-002f7098="" class="button">
                                            <div data-v-7dbf30ce="" class="button__icon-wrapper"
                                                 style="width: 40px; height: 40px; line-height: 40px;"><img
                                                    data-v-7dbf30ce="" src="../../static/app/img/bug.9001cff8.svg" alt="action"
                                                    class="button__icon"><img data-v-7dbf30ce=""
                                                                              src="../../static/app/img/bug-border.43858de4.svg"
                                                                              alt="state"
                                                                              class="button__state button__state--inactive">
                                            </div>
                                            <div data-v-7dbf30ce="" class="button__title">
                                                杀虫
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="wrapper" data-v-789dbbe9="" data-v-5b76eaaa="" class="detail-layer animate transparent"
                             style="touch-action: pan-x; user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                            <div data-v-789dbbe9="">
                                <div data-v-789dbbe9="" class="outer-card v-card v-card--flat v-sheet theme--light" >
                                    <div data-v-789dbbe9="" class="v-card__title relative title" onclick="change()"><img data-v-789dbbe9=""
                                                                                                                         src="../../static/app/img/building.icon.582e4a18.svg"
                                                                                                                         alt="icon">
                                        智恒智慧生态农业园
                                        <i id="arrowRight" data-v-303164da="" aria-hidden="true" class="v-icon notranslate mdi mdi-chevron-right theme--light" style="display:none;"></i>
                                        <img id="arrowUp" data-v-789dbbe9="" src="../../static/app/img/arrow-up.e4f37f38.svg"
                                             alt="arrow-up" class="arrow-up" style="display: block"></div>
                                    <div data-v-303164da="" class="v-card v-card--flat v-sheet theme--light">
                                        <div data-v-303164da="" class="v-card__title subtitle-1 pt-2 pb-1"><img
                                                data-v-303164da="" src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon">
                                            生态指标
                                        </div>
                                        <div data-v-303164da="" class="container pt-0">
                                            <div data-v-303164da=""
                                                 class="data-card v-card v-card--flat v-sheet theme--light">
                                                <div data-v-303164da="" class="container pa-5">
                                                    <div data-v-303164da="" class="row no-gutters">
                                                        <div data-v-303164da="" class="text-center col">
                                                            <div data-v-303164da="" class="body-1">
                                                                1.21m²
                                                            </div>
                                                            <div data-v-303164da="" class="caption">返绿面积</div>
                                                        </div>
                                                        <div data-v-303164da="" class="text-center col">
                                                            <div data-v-303164da="" class="body-1">
                                                                1621L
                                                            </div>
                                                            <div data-v-303164da="" class="caption">沼液消耗</div>
                                                        </div>
                                                        <div data-v-303164da="" class="text-center col">
                                                            <div data-v-303164da="" class="body-1">
                                                                32420kg
                                                            </div>
                                                            <div data-v-303164da="" class="caption">沼泥消耗</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-v-303164da="" class="outer-card v-card v-card--flat v-sheet theme--light">
                                    <div data-v-303164da="" class="v-card__title subtitle-1 pt-2 pb-1"><img
                                            data-v-303164da="" src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon"> 生产指标
                                    </div>
                                    <div data-v-303164da="" class="container pt-0">
                                        <div data-v-303164da=""
                                             class="data-card v-card v-card--flat v-sheet theme--light">
                                            <div data-v-303164da="" class="container pa-5">
                                                <div data-v-303164da="" class="row no-gutters">
                                                    <div data-v-303164da="" class="text-center col">
                                                        <div id="batchCount" data-v-303164da="" class="body-1">

                                                        </div>
                                                        <div data-v-303164da="" class="caption">播种批次数</div>
                                                    </div>
                                                    <div data-v-303164da="" class="text-center col">
                                                        <div id="harvestCount" data-v-303164da="" class="body-1">

                                                        </div>
                                                        <div data-v-303164da="" class="caption">收成批次数</div>
                                                    </div>
                                                    <div data-v-303164da="" class="text-center col">
                                                        <div id="output" data-v-303164da="" class="body-1">

                                                        </div>
                                                        <div data-v-303164da="" class="caption">总产量</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-v-303164da="" class="outer-card v-card v-card--flat v-sheet theme--light" >
                                    <div data-v-303164da="" class="v-card__title subtitle-1 pt-2 pb-1"><img
                                            data-v-303164da="" src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon"> 提醒
                                    </div>
                                    <div data-v-303164da="" class="container pt-0">
                                        <div data-v-303164da="" class="row no-gutters justify-space-around">
                                            <div data-v-303164da="" class="text-center col"
                                                 style="flex: 0 0 auto; width: auto;"><a data-v-303164da=""
                                                                                         href="http://ag.soft.gszh.cn/early-warning/farm-work"
                                                                                         class="data-card v-card v-card--flat v-card--link v-sheet theme--light"
                                                                                         tabindex="0">
                                                <div data-v-303164da="" class="container pa-5">
                                                    <div id="farmRemind" data-v-303164da="" class="body-1"
                                                         style="color: rgb(24, 135, 55);">

                                                    </div>
                                                    <div data-v-303164da="" class="caption">农事提醒</div>
                                                </div>
                                            </a></div>
                                            <div data-v-303164da="" class="text-center col"
                                                 style="flex: 0 0 auto; width: auto;"><a data-v-303164da=""
                                                                                         href="http://ag.soft.gszh.cn/early-warning/damage"
                                                                                         class="data-card v-card v-card--flat v-card--link v-sheet theme--light"
                                                                                         tabindex="0">
                                                <div data-v-303164da="" class="container pa-5">
                                                    <div id="indexRemind" data-v-303164da="" class="body-1"
                                                         style="color: rgb(11, 122, 130);">

                                                    </div>
                                                    <div data-v-303164da="" class="caption">指标告警</div>
                                                </div>
                                            </a></div>
                                            <div data-v-303164da="" class="text-center col"
                                                 style="flex: 0 0 auto; width: auto;"><a data-v-303164da=""
                                                                                         href="http://ag.soft.gszh.cn/early-warning/disaster"
                                                                                         class="data-card v-card v-card--flat v-card--link v-sheet theme--light"
                                                                                         tabindex="0">
                                                <div data-v-303164da="" class="container pa-5">
                                                    <div id="disasterRemind" data-v-303164da="" class="body-1"
                                                         style="color: rgb(249, 22, 22);">

                                                    </div>
                                                    <div data-v-303164da="" class="caption">灾害预警</div>
                                                </div>
                                            </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div data-v-303164da="" class="outer-card v-card v-card--flat v-sheet theme--light">
                                    <div data-v-303164da="" class="v-card__title subtitle-1 pt-2 pb-1"><img
                                            data-v-303164da="" src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon">在植作物
                                        <div data-v-303164da="" class="spacer"></div><span data-v-303164da=""
                                                                                           class="caption">
                  种类：
                  <span id="count" data-v-303164da="" class="subtitle-1">
                    2
                  </span>
                  种
                </span></div>
                                    <div data-v-789dbbe9="" id="about">
                                        <div id="scroller">
                                        </div>
                                    </div>
                                </div>
                                <div data-v-303164da="" class="outer-card v-card v-card--flat v-sheet theme--light">
                                    <div data-v-303164da="" class="v-card__title subtitle-1 pt-2 pb-1"><img
                                            data-v-303164da="" src="../../static/app/img/dot.icon.7f85dfff.svg" alt="icon"> 能效指标
                                    </div>
                                    <div data-v-303164da="" class="v-card__text subtitle-2 py-0">
                                        平均单位耗能(每kg)
                                    </div>
                                    <div data-v-303164da="" class="container">
                                        <div data-v-303164da="" class="v-card v-card--flat v-sheet theme--light">
                                            <div data-v-303164da="" class="container pa-5">
                                                <div data-v-303164da="" class="row text-center no-gutters">
                                                    <div data-v-303164da=""
                                                         class="d-flex justify-center align-center col col-6"><img
                                                            data-v-303164da="" src="../../static/app/img/water.icon.51f50648.svg"
                                                            alt="icon">
                                                        <div  data-v-303164da="" class="body-1 pl-1"
                                                             style="color: rgb(51, 51, 51);">

                                                            <span id="liter" data-v-303164da="" class="unit"></span></div>
                                                    </div>
                                                    <div data-v-303164da=""
                                                         class="d-flex justify-center align-center col col-6"><img
                                                            data-v-303164da=""
                                                            src="../../static/app/img/electricity.icon.52debe81.svg" alt="icon">
                                                        <div  data-v-303164da="" class="body-1 pl-1"
                                                             style="color: rgb(51, 51, 51);">

                                                            <span id="kilowatt" data-v-303164da="" class="unit"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-v-303164da="" class="v-card__text subtitle-2 py-0">
                                        <div data-v-303164da="" class="d-flex">
                                            月指标
                                            <div data-v-303164da="" class="spacer"></div>
                                            <span data-v-303164da="" style="color: rgb(102, 102, 102);">2019/11</span>
                                        </div>
                                    </div>
                                    <div data-v-303164da="" class="container">
                                        <div data-v-303164da="" class="v-card v-card--flat v-sheet theme--light">
                                            <div data-v-303164da="" class="container pa-5">
                                                <div data-v-303164da="" class="row text-center no-gutters">
                                                    <div data-v-303164da="" class="col">
                                                        <div data-v-303164da="" class="row no-gutters">
                                                            <div data-v-303164da="" class="col col-12"><img
                                                                    data-v-303164da=""
                                                                    src="../../static/app/img/water.icon.51f50648.svg" alt="icon"
                                                                    style="width: 32px;"></div>
                                                        </div>
                                                        <div data-v-303164da="" class="row no-gutters align-end">
                                                            <div id="totalWaterConsumptionPerMonth" data-v-303164da="" class="caption col col-6"
                                                            style="color: rgb(51, 51, 51);">
                                                                消耗(L)
                                                            </div>
                                                            <div id="averageWaterConsumptionPerMonth" data-v-303164da="" class="caption col col-6"
                                                                 style="color: rgb(102, 102, 102);">
                                                                平均值(L)
                                                            </div>
                                                        </div>
                                                        <div data-v-303164da="" class="row no-gutters align-end">
                                                            <div id="perMonthWaterTotalConsumption" data-v-303164da="" class="body-1 col col-6"
                                                                 style="color: rgb(51, 51, 51);">

                                                            </div>
                                                            <div id="averageMonthWaterConsumption" data-v-303164da="" class="body-1 col col-6"
                                                                 style="color: rgb(102, 102, 102);">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div data-v-303164da="" class="col"
                                                         style="border-left: 1px solid rgb(204, 204, 204);">
                                                        <div data-v-303164da="" class="row no-gutters">
                                                            <div data-v-303164da="" class="col col-12"><img
                                                                    data-v-303164da=""
                                                                    src="../../static/app/img/electricity.icon.52debe81.svg"
                                                                    alt="icon" style="width: 32px;"></div>
                                                        </div>
                                                        <div data-v-303164da="" class="row no-gutters align-end">
                                                            <div id="totalElectricConsumption" data-v-303164da="" class="caption col col-6">
                                                                消耗(kWh)
                                                            </div>
                                                            <div id="averageElectricConsumption" data-v-303164da="" class="caption col col-6">
                                                                平均值(kWh)
                                                            </div>
                                                        </div>
                                                        <div data-v-303164da="" class="row no-gutters align-end">
                                                            <div id="perMonthElectricTotalConsumption" data-v-303164da="" class="body-1 col col-6">

                                                            </div>
                                                            <div id="averageMonthElectricConsumption" data-v-303164da="" class="body-1 col col-6">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div data-v-303164da="" class="v-card__text subtitle-2 py-0">
                                        增产排行
                                    </div>
                                    <div data-v-303164da="" class="container">
                                        <div data-v-303164da="" class="v-card v-card--flat v-sheet theme--light">
                                            <div data-v-303164da="" class="container pa-2">
                                                <div data-v-303164da="" class="row no-gutters align-center">
                                                    <div data-v-303164da="" class="rank caption col">排行</div>
                                                    <div data-v-303164da="" class="caption col col-4">名称</div>
                                                    <div data-v-303164da="" class="caption col col-6">产耗比</div>
                                                </div>
                                                <div data-v-303164da="" class="row mt-4 no-gutters align-center">
                                                    <div data-v-303164da="" class="rank col">
                                                        <div data-v-303164da="" class="rank__order rank__order--1">
                                                            1
                                                        </div>
                                                    </div>
                                                    <div data-v-303164da="" class="caption col col-4">小顾实验1田</div>
                                                    <div data-v-303164da="" class="col col-6">
                                                        <div data-v-303164da="" role="progressbar" aria-valuemin="0"
                                                             aria-valuemax="100" aria-valuenow="90"
                                                             class="v-progress-linear progress v-progress-linear--rounded theme--light"
                                                             style="height: 24px;">
                                                            <div class="v-progress-linear__background"
                                                                 style="opacity: 0.3; left: 90%; width: 10%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__buffer"></div>
                                                            <div class="v-progress-linear__determinate"
                                                                 style="width: 90%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__content">
                                                                <div data-v-303164da="" class="percent">90%</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-v-303164da="" class="row mt-4 no-gutters align-center">
                                                    <div data-v-303164da="" class="rank col">
                                                        <div data-v-303164da="" class="rank__order rank__order--2">
                                                            2
                                                        </div>
                                                    </div>
                                                    <div data-v-303164da="" class="caption col col-4">小陈实验1田</div>
                                                    <div data-v-303164da="" class="col col-6">
                                                        <div data-v-303164da="" role="progressbar" aria-valuemin="0"
                                                             aria-valuemax="100" aria-valuenow="88"
                                                             class="v-progress-linear progress v-progress-linear--rounded theme--light"
                                                             style="height: 24px;">
                                                            <div class="v-progress-linear__background"
                                                                 style="opacity: 0.3; left: 88%; width: 12%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__buffer"></div>
                                                            <div class="v-progress-linear__determinate"
                                                                 style="width: 88%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__content">
                                                                <div data-v-303164da="" class="percent">88%</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div data-v-303164da="" class="row mt-4 no-gutters align-center">
                                                    <div data-v-303164da="" class="rank col">
                                                        <div data-v-303164da="" class="rank__order rank__order--3">
                                                            3
                                                        </div>
                                                    </div>
                                                    <div data-v-303164da="" class="caption col col-4">小劳实验1田</div>
                                                    <div data-v-303164da="" class="col col-6">
                                                        <div data-v-303164da="" role="progressbar" aria-valuemin="0"
                                                             aria-valuemax="100" aria-valuenow="80"
                                                             class="v-progress-linear progress v-progress-linear--rounded theme--light"
                                                             style="height: 24px;">
                                                            <div class="v-progress-linear__background"
                                                                 style="opacity: 0.3; left: 80%; width: 20%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__buffer"></div>
                                                            <div class="v-progress-linear__determinate"
                                                                 style="width: 80%; background-color: rgb(195, 152, 100); border-color: rgb(195, 152, 100);"></div>
                                                            <div class="v-progress-linear__content">
                                                                <div data-v-303164da="" class="percent">80%</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div data-v-315f272a="" role="dialog" class="v-dialog__container">
                            <div role="document" data-v-315f272a="" class="v-dialog__content" style="z-index: 0;">
                                <div class="v-dialog v-dialog--fullscreen" style="display: none;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--右拉-->
<div class="leftNav">
    <nav class="v-application  v-application--is-ltr layer v-navigation-drawer"
         style=" width: 100%; ">
        <div data-v-5cd969a2="" class="container nav-header pt-8">
            <div  class="row align-center justify-center">
                <div  style="width: 104px; height: 48px;">
                    <div  class="v-responsive v-image">
                        <div class="v-responsive__sizer"
                             style="padding-bottom: 46.1538%;"></div>
                        <div class="v-image__image v-image__image--cover"
                             style="background-image: url(../../static/app/img/缘田logo1.png); background-position: center center;">
                        </div>
                    </div>
                </div>
            </div>
            <div  class="text-center mt-4">
                智恒智慧生态农业园
            </div>
        </div>
        <hr  aria-orientation="horizontal" class="v-divider theme--light">
        <div
                class="v-list v-sheet v-sheet--tile theme--light v-list--dense v-list--flat"><a
                data-v-5cd969a2="" href="/IndexController/FarmlandManage"
                class="v-list-item v-list-item--link theme--light" tabindex="0">
            <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                 aria-hidden="true"
                                                                 class="v-icon notranslate brown--text mdi mdi-view-dashboard theme--light"></i>
            </div>
            <div data-v-5cd969a2="" class="v-list-item__content">
                <div data-v-5cd969a2="" class="v-list-item__title">农田管理</div>
            </div>
        </a>
            <a data-v-5cd969a2="" href="/IndexController/PlantingManage"
               class="v-list-item v-list-item--link theme--light" tabindex="0">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate green--text mdi mdi-sprout-outline theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title">种植管理</div>
                </div>
            </a>
            <a data-v-5cd969a2="" href="/IndexController/ExpertLibrary"
               class="v-list-item v-list-item--link theme--light" tabindex="0">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate blue--text mdi mdi-book-open-page-variant theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title">专家库</div>
                </div>
            </a>
            <a data-v-5cd969a2="" href="/IndexController/FoodTraceability"
               class="v-list-item v-list-item--link theme--light" tabindex="0">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate cyan--text mdi mdi-source-branch theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title">食品溯源</div>
                </div>
            </a><a data-v-5cd969a2="" href="http://ag.soft.gszh.cn/emergency-control"
                   class="v-list-item v-list-item--link theme--light" tabindex="0">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate deep-purple--text mdi mdi-tune-vertical theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title">应急控制</div>
                </div>
            </a>
            <hr data-v-5cd969a2="" role="separator" aria-orientation="horizontal"
                class="v-divider theme--light">
            <a data-v-5cd969a2="" href="/IndexController/PasswordChange"
               class="v-list-item v-list-item--link theme--light" tabindex="0">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate indigo--text mdi mdi-square-edit-outline theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title indigo--text">修改密码</div>
                </div>
            </a>
            <div data-v-5cd969a2="" tabindex="0"
                 class="v-list-item v-list-item--link theme--light">
                <div data-v-5cd969a2="" class="v-list-item__icon"><i data-v-5cd969a2=""
                                                                     aria-hidden="true"
                                                                     class="v-icon notranslate red--text mdi mdi-logout-variant theme--light"></i>
                </div>
                <div data-v-5cd969a2="" class="v-list-item__content">
                    <div data-v-5cd969a2="" class="v-list-item__title red--text">退出登录</div>
                </div>
            </div>
        </div>
    </nav>
</div>
</body>
</html>