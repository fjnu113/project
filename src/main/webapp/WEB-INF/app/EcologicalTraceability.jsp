<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0061)http://ag.soft.gszh.cn/trace/result?unit_seq=ZH201907E2_14472 -->
<html lang="zh-Hans">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="./../../static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-4182d364.bbec304b.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-6a09404c.a7f98917.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-6d00f192.7af6302c.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-e7464a4c.942ad61f.css" rel="prefetch">
    <link href="./../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="./../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-2c59789a.c26f4895.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-6a09404c.a7f98917.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-e7464a4c.942ad61f.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-4182d364.bbec304b.css">
    <link rel="stylesheet" type="text/css" href="./../../static/app/css/chunk-6d00f192.7af6302c.css">

    <script type="text/javascript" src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            ecologicalTraceabilityDetail();
        })
        function ecologicalTraceabilityDetail(){
            var batchNum="${data.batchNum}"
            var data={"batchNum":batchNum}     //忘了添加键值
            $.ajax({
                url:'/PlantManage/ecologicalTraceabilityDetail',
                data:JSON.stringify(data),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    for(i=0;i<=res.data.length;i++){
                        document.getElementById("batchNum").innerHTML=res.data[i].batchNum;
                        document.getElementById("varietyName").innerHTML=res.data[i].varietyName;
                        document.getElementById("farmlandName").innerHTML=res.data[i].farmlandName;
                        document.getElementById("starTime").innerHTML=res.data[i].starTime;
                        document.getElementById("endTime").innerHTML=res.data[i].endTime;
                    }
                }
            })
        }
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent"
                     style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div>
                <header data-v-2eae54fc=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed primary"
                        data-booted="true"
                        style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div data-v-2eae54fc="" class="toolbar-left" onclick="window.location.href='/IndexController/PlantingManage'"><img data-v-2eae54fc=""
                                                                              src="./../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                              alt="icon"></div>
                            生态溯源
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                </header>
                <main class="v-content grey lighten-5" data-booted="true"
                      style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div class="v-responsive v-image" style="height: 250px;">
                            <div class="v-responsive__sizer" style="padding-bottom: 66.6%;"></div>
                            <div class="v-image__image v-image__image--cover"
                                 style="background-image: url(&quot;http://ag.soft.gszh.cn/image/cai/baocai.jpg&quot;); background-position: center center;"></div>
                            <div class="v-responsive__content" style="width: 500px;"></div>
                        </div>
                        <div role="list"
                             class="v-list my-3 v-sheet v-sheet--tile theme--light v-list--subheader v-list--two-line">
                            <div class="v-subheader theme--light"><img
                                    src="./../../static/app/img/dot.icon.7f85dfff.svg" alt="icon">
                                &nbsp;基本信息
                            </div>
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        批次号
                                    </div>
                                    <div class="v-list-item__title" id="batchNum">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal"
                                class="ml-4 v-divider theme--light">
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        作物名称
                                    </div>
                                    <div class="v-list-item__title" id="varietyName">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        栽种田
                                    </div>
                                    <div class="v-list-item__title" id="farmlandName">
                                    </div>
                                </div>
                            </div>
                            <hr role="separator" aria-orientation="horizontal"
                                class="ml-4 v-divider theme--light">
                            <div tabindex="-1" role="listitem" class="v-list-item theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        栽种时间
                                    </div>
                                    <div class="v-list-item__title" id="starTime">
                                    </div>
                                </div>
                                <hr role="separator" aria-orientation="vertical"
                                    class="ma-5 v-divider v-divider--vertical theme--light">
                                <div class="v-list-item__content">
                                    <div class="v-list-item__subtitle">
                                        收获时间
                                    </div>
                                    <div class="v-list-item__title" id="endTime">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-v-a16e4ffe="" class="qkl grey lighten-5">
                            <div data-v-a16e4ffe="" class="qkl_sm">本记录由智恒区块链存证中心提供</div>
                            <div data-v-a16e4ffe="" class="qkl_more">
                                没有更多数据
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>

</body>
</html>