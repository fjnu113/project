<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0048)http://ag.soft.gszh.cn/cultivation/units/history -->
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <link rel="icon" href="http://ag.soft.gszh.cn/favicon.ico">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/apphead.css" rel="stylesheet">
    <link href="./../../static/app/css/chunk-31c15f22.7e0f23d7.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-776d3ee6.3981d2b5.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="./../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="./../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="./../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="./../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-776d3ee6.3981d2b5.css">
    <link rel="stylesheet" type="text/css"
          href="./../../static/app/css/chunk-31c15f22.7e0f23d7.css">
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent"
                     style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div data-v-7533c0c4="">
                <header data-v-2eae54fc="" data-v-7533c0c4=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed primary"
                        data-booted="true"
                        style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div onclick="window.location.href='/IndexController/Index';" data-v-2eae54fc=""
                                 class="toolbar-left"><img data-v-2eae54fc=""
                                                           src="./../../static/app/img/arrow-left.ac6c6d62.svg"
                                                           alt="icon"></div>
                            食品溯源
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                </header>
                <main data-v-7533c0c4="" class="v-content grey lighten-5" data-booted="true"
                      style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div data-v-7533c0c4="" class="sy">
                            <div data-v-7533c0c4="" class="sy_content">
                                <ul data-v-7533c0c4="">
                                    <li data-v-7533c0c4=""><a data-v-7533c0c4="" elevation="10"
                                                              class="aBg_blue elevation-7"><img
                                            data-v-7533c0c4=""
                                            src="./../../static/app/img/number.a2f1e652.svg">
                                        <p data-v-7533c0c4="">编号查询</p></a></li>
                                    <li data-v-7533c0c4=""><a data-v-7533c0c4=""
                                                              class="aBg_orange elevation-7"><img
                                            data-v-7533c0c4=""
                                            src="./../../static/app/img/scan.c0a247e2.svg">
                                        <p data-v-7533c0c4="">扫一扫</p></a></li>
                                </ul>
                            </div>
                            <div data-v-7533c0c4="" role="dialog"
                                 class="v-dialog__container dark_form">
                                <div role="document" class="v-dialog__content" style="z-index: 0;">
                                    <div class="v-dialog v-dialog--persistent"
                                         style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>