<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link href="../../static/app/css/apphead.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-240cfc17.7393a102.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b87d2c68.77ab4765.css">
    <script type="text/javascript" src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            GetAllPlantMain();
        });
        function GetAllPlantMain() {
            $.ajax({
                url:'/ExpertLibrary/Main',
                type:'GET',
                success:function(res){
                    for(i = 0; i <= res.data.length; ++i){
                        if(res.data[i].name=="包菜")
                        {
                            var data={"varietyName":res.data[i].name}
                            content = '<div data-v-210fea08="" class="px-3 zjk" window.>';
                            content += '<ul data-v-210fea08="">';
                            content += '<li data-v-210fea08="">';
                            content += '<div data-v-210fea08="" class="shucai_bg">';
                            content += '<img data-v-210fea08="" src="../../static/app/img/baocai.jpg">';
                            content += '</div>';
                            content += '<div data-v-210fea08="" class="sc_js"><p data-v-210fea08="">';
                            content += res.data[i].name;
                            content += '</p>';
                            content += '<p data-v-210fea08="">';
                            content += res.data[i].describetion;
                            content += '</p>';
                            content += '<a data-v-210fea08="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'">培育详情 ></a></div>';
                            content += '<div data-v-210fea08="" class="clearfix"></div>';
                            content += '</li>';
                            content += '</ul>';
                            content += '</div>';
                            $(".pt-2").append(content);
                        }
                        else if(res.data[i].name=="茄子")
                        {
                            var data={"varietyName":res.data[i].name}
                            content = '<div data-v-210fea08="" class="px-3 zjk" window.>';
                            content += '<ul data-v-210fea08="">';
                            content += '<li data-v-210fea08="">';
                            content += '<div data-v-210fea08="" class="shucai_bg">';
                            content += '<img data-v-210fea08="" src="../../static/app/img/qiezi.jpg">';
                            content += '</div>';
                            content += '<div data-v-210fea08="" class="sc_js"><p data-v-210fea08="">';
                            content += res.data[i].name;
                            content += '</p>';
                            content += '<p data-v-210fea08="">';
                            content += res.data[i].describetion;
                            content += '</p>';
                            content += '<a data-v-210fea08="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'">培育详情 ></a></div>';
                            content += '<div data-v-210fea08="" class="clearfix"></div>';
                            content += '</li>';
                            content += '</ul>';
                            content += '</div>';
                            $(".pt-2").append(content);
                        }
                        else
                        {
                            var data={"varietyName":res.data[i].name}
                            content = '<div data-v-210fea08="" class="px-3 zjk" window.>';
                            content += '<ul data-v-210fea08="">';
                            content += '<li data-v-210fea08="">';
                            content += '<div data-v-210fea08="" class="shucai_bg">';
                            content += '<img data-v-210fea08="" src="../../static/app/img/baocai.jpg">';
                            content += '</div>';
                            content += '<div data-v-210fea08="" class="sc_js"><p data-v-210fea08="">';
                            content += res.data[i].name;
                            content += '</p>';
                            content += '<p data-v-210fea08="">';
                            content += res.data[i].describetion;
                            content += '</p>';
                            content += '<a data-v-210fea08="" href="/IndexController/Cultivation?data='+encodeURIComponent(JSON.stringify(data))+'">培育详情 ></a></div>';
                            content += '<div data-v-210fea08="" class="clearfix"></div>';
                            content += '</li>';
                            content += '</ul>';
                            content += '</div>';
                            $(".pt-2").append(content);
                        }
                    }
                }
            });
        };
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent"
                     style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div>
                <header data-v-2eae54fc=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed v-app-bar--is-scrolled primary"
                        data-booted="true"
                        style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div onclick="window.location.href='/IndexController/Index'" data-v-2eae54fc="" class="toolbar-left"><img data-v-2eae54fc=""
                                                                                                                                                        src="../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                                                                                                        alt="icon"></div>
                            专家库
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                </header>
                <main class="v-content" data-booted="true" style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div class="py-3 px-3">
                            <button type="button"
                                    class="grey lighten-2 grey--text text--lighten-1 v-btn v-btn--block v-btn--depressed v-btn--flat v-btn--outlined v-btn--rounded theme--light v-size--small"><span
                                    class="v-btn__content"><i aria-hidden="true"
                                                              class="v-icon notranslate mdi mdi-magnify theme--light"
                                                              style="font-size: 16px;"></i>
      搜索
    </span></button>
                            <div role="dialog" class="v-dialog__container">
                                <div role="document" class="v-dialog__content" style="z-index: 0;">
                                    <div class="v-dialog v-dialog--fullscreen v-dialog--scrollable"
                                         style="display: none;"></div>
                                </div>
                            </div>
                        </div>
                        <div class="pt-2">

                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>