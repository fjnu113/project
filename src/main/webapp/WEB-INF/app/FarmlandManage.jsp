<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<!DOCTYPE html>
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style class="vjs-styles-defaults">
        .video-js {
            width: 300px;
            height: 150px;
        }

        .vjs-fluid {
            padding-top: 56.25%
        }
    </style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5dc2f224.009d7923.css" rel="prefetch">
    <link href="../../static/app/css/chunk-6a09404c.a7f98917.css" rel="prefetch">
    <link href="../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="../../static/app/css/chunk-e7464a4c.942ad61f.css" rel="prefetch">
    <link href="../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-73671046.d07ccefa.css">

    <style type="text/css" id="vuetify-theme-stylesheet">:root {
        --v-anchor-base: #107064;
        --v-primary-base: #107064;
        --v-primary-lighten5: #a4f8e9;
        --v-primary-lighten4: #88dccd;
        --v-primary-lighten3: #6cc0b1;
        --v-primary-lighten2: #50a497;
        --v-primary-lighten1: #348a7d;
        --v-primary-darken1: #00574c;
        --v-primary-darken2: #003f36;
        --v-primary-darken3: #002920;
        --v-primary-darken4: #001809;
        --v-secondary-base: #81c784;
        --v-secondary-lighten5: #ffffff;
        --v-secondary-lighten4: #f1fff2;
        --v-secondary-lighten3: #d4ffd5;
        --v-secondary-lighten2: #b8ffba;
        --v-secondary-lighten1: #9ce39e;
        --v-secondary-darken1: #66ab6a;
        --v-secondary-darken2: #4c9152;
        --v-secondary-darken3: #31773a;
        --v-secondary-darken4: #145d23;
        --v-accent-base: #00c853;
        --v-accent-lighten5: #c4ffdb;
        --v-accent-lighten4: #a5ffbf;
        --v-accent-lighten3: #85ffa3;
        --v-accent-lighten2: #65ff88;
        --v-accent-lighten1: #41e56d;
        --v-accent-darken1: #00ac39;
        --v-accent-darken2: #00901e;
        --v-accent-darken3: #007500;
        --v-accent-darken4: #005b00;
        --v-error-base: #ff5252;
        --v-error-lighten5: #ffe4d5;
        --v-error-lighten4: #ffc6b9;
        --v-error-lighten3: #ffa99e;
        --v-error-lighten2: #ff8c84;
        --v-error-lighten1: #ff6f6a;
        --v-error-darken1: #df323b;
        --v-error-darken2: #bf0025;
        --v-error-darken3: #9f0010;
        --v-error-darken4: #800000;
        --v-info-base: #2196f3;
        --v-info-lighten5: #d4ffff;
        --v-info-lighten4: #b5ffff;
        --v-info-lighten3: #95e8ff;
        --v-info-lighten2: #75ccff;
        --v-info-lighten1: #51b0ff;
        --v-info-darken1: #007cd6;
        --v-info-darken2: #0064ba;
        --v-info-darken3: #004d9f;
        --v-info-darken4: #003784;
        --v-success-base: #4caf50;
        --v-success-lighten5: #dcffd6;
        --v-success-lighten4: #beffba;
        --v-success-lighten3: #a2ff9e;
        --v-success-lighten2: #85e783;
        --v-success-lighten1: #69cb69;
        --v-success-darken1: #2d9437;
        --v-success-darken2: #00791e;
        --v-success-darken3: #006000;
        --v-success-darken4: #004700;
        --v-warning-base: #fb8c00;
        --v-warning-lighten5: #ffff9e;
        --v-warning-lighten4: #fffb82;
        --v-warning-lighten3: #ffdf67;
        --v-warning-lighten2: #ffc24b;
        --v-warning-lighten1: #ffa72d;
        --v-warning-darken1: #db7200;
        --v-warning-darken2: #bb5900;
        --v-warning-darken3: #9d4000;
        --v-warning-darken4: #802700;
    }

    .v-application a {
        color: var(--v-anchor-base);
    }

    .v-application .primary {
        background-color: var(--v-primary-base) !important;
        border-color: var(--v-primary-base) !important;
    }

    .v-application .primary--text {
        color: var(--v-primary-base) !important;
        caret-color: var(--v-primary-base) !important;
    }

    .v-application .primary.lighten-5 {
        background-color: var(--v-primary-lighten5) !important;
        border-color: var(--v-primary-lighten5) !important;
    }

    .v-application .primary--text.text--lighten-5 {
        color: var(--v-primary-lighten5) !important;
        caret-color: var(--v-primary-lighten5) !important;
    }

    .v-application .primary.lighten-4 {
        background-color: var(--v-primary-lighten4) !important;
        border-color: var(--v-primary-lighten4) !important;
    }

    .v-application .primary--text.text--lighten-4 {
        color: var(--v-primary-lighten4) !important;
        caret-color: var(--v-primary-lighten4) !important;
    }

    .v-application .primary.lighten-3 {
        background-color: var(--v-primary-lighten3) !important;
        border-color: var(--v-primary-lighten3) !important;
    }

    .v-application .primary--text.text--lighten-3 {
        color: var(--v-primary-lighten3) !important;
        caret-color: var(--v-primary-lighten3) !important;
    }

    .v-application .primary.lighten-2 {
        background-color: var(--v-primary-lighten2) !important;
        border-color: var(--v-primary-lighten2) !important;
    }

    .v-application .primary--text.text--lighten-2 {
        color: var(--v-primary-lighten2) !important;
        caret-color: var(--v-primary-lighten2) !important;
    }

    .v-application .primary.lighten-1 {
        background-color: var(--v-primary-lighten1) !important;
        border-color: var(--v-primary-lighten1) !important;
    }

    .v-application .primary--text.text--lighten-1 {
        color: var(--v-primary-lighten1) !important;
        caret-color: var(--v-primary-lighten1) !important;
    }

    .v-application .primary.darken-1 {
        background-color: var(--v-primary-darken1) !important;
        border-color: var(--v-primary-darken1) !important;
    }

    .v-application .primary--text.text--darken-1 {
        color: var(--v-primary-darken1) !important;
        caret-color: var(--v-primary-darken1) !important;
    }

    .v-application .primary.darken-2 {
        background-color: var(--v-primary-darken2) !important;
        border-color: var(--v-primary-darken2) !important;
    }

    .v-application .primary--text.text--darken-2 {
        color: var(--v-primary-darken2) !important;
        caret-color: var(--v-primary-darken2) !important;
    }

    .v-application .primary.darken-3 {
        background-color: var(--v-primary-darken3) !important;
        border-color: var(--v-primary-darken3) !important;
    }

    .v-application .primary--text.text--darken-3 {
        color: var(--v-primary-darken3) !important;
        caret-color: var(--v-primary-darken3) !important;
    }

    .v-application .primary.darken-4 {
        background-color: var(--v-primary-darken4) !important;
        border-color: var(--v-primary-darken4) !important;
    }

    .v-application .primary--text.text--darken-4 {
        color: var(--v-primary-darken4) !important;
        caret-color: var(--v-primary-darken4) !important;
    }

    .v-application .secondary {
        background-color: var(--v-secondary-base) !important;
        border-color: var(--v-secondary-base) !important;
    }

    .v-application .secondary--text {
        color: var(--v-secondary-base) !important;
        caret-color: var(--v-secondary-base) !important;
    }

    .v-application .secondary.lighten-5 {
        background-color: var(--v-secondary-lighten5) !important;
        border-color: var(--v-secondary-lighten5) !important;
    }

    .v-application .secondary--text.text--lighten-5 {
        color: var(--v-secondary-lighten5) !important;
        caret-color: var(--v-secondary-lighten5) !important;
    }

    .v-application .secondary.lighten-4 {
        background-color: var(--v-secondary-lighten4) !important;
        border-color: var(--v-secondary-lighten4) !important;
    }

    .v-application .secondary--text.text--lighten-4 {
        color: var(--v-secondary-lighten4) !important;
        caret-color: var(--v-secondary-lighten4) !important;
    }

    .v-application .secondary.lighten-3 {
        background-color: var(--v-secondary-lighten3) !important;
        border-color: var(--v-secondary-lighten3) !important;
    }

    .v-application .secondary--text.text--lighten-3 {
        color: var(--v-secondary-lighten3) !important;
        caret-color: var(--v-secondary-lighten3) !important;
    }

    .v-application .secondary.lighten-2 {
        background-color: var(--v-secondary-lighten2) !important;
        border-color: var(--v-secondary-lighten2) !important;
    }

    .v-application .secondary--text.text--lighten-2 {
        color: var(--v-secondary-lighten2) !important;
        caret-color: var(--v-secondary-lighten2) !important;
    }

    .v-application .secondary.lighten-1 {
        background-color: var(--v-secondary-lighten1) !important;
        border-color: var(--v-secondary-lighten1) !important;
    }

    .v-application .secondary--text.text--lighten-1 {
        color: var(--v-secondary-lighten1) !important;
        caret-color: var(--v-secondary-lighten1) !important;
    }

    .v-application .secondary.darken-1 {
        background-color: var(--v-secondary-darken1) !important;
        border-color: var(--v-secondary-darken1) !important;
    }

    .v-application .secondary--text.text--darken-1 {
        color: var(--v-secondary-darken1) !important;
        caret-color: var(--v-secondary-darken1) !important;
    }

    .v-application .secondary.darken-2 {
        background-color: var(--v-secondary-darken2) !important;
        border-color: var(--v-secondary-darken2) !important;
    }

    .v-application .secondary--text.text--darken-2 {
        color: var(--v-secondary-darken2) !important;
        caret-color: var(--v-secondary-darken2) !important;
    }

    .v-application .secondary.darken-3 {
        background-color: var(--v-secondary-darken3) !important;
        border-color: var(--v-secondary-darken3) !important;
    }

    .v-application .secondary--text.text--darken-3 {
        color: var(--v-secondary-darken3) !important;
        caret-color: var(--v-secondary-darken3) !important;
    }

    .v-application .secondary.darken-4 {
        background-color: var(--v-secondary-darken4) !important;
        border-color: var(--v-secondary-darken4) !important;
    }

    .v-application .secondary--text.text--darken-4 {
        color: var(--v-secondary-darken4) !important;
        caret-color: var(--v-secondary-darken4) !important;
    }

    .v-application .accent {
        background-color: var(--v-accent-base) !important;
        border-color: var(--v-accent-base) !important;
    }

    .v-application .accent--text {
        color: var(--v-accent-base) !important;
        caret-color: var(--v-accent-base) !important;
    }

    .v-application .accent.lighten-5 {
        background-color: var(--v-accent-lighten5) !important;
        border-color: var(--v-accent-lighten5) !important;
    }

    .v-application .accent--text.text--lighten-5 {
        color: var(--v-accent-lighten5) !important;
        caret-color: var(--v-accent-lighten5) !important;
    }

    .v-application .accent.lighten-4 {
        background-color: var(--v-accent-lighten4) !important;
        border-color: var(--v-accent-lighten4) !important;
    }

    .v-application .accent--text.text--lighten-4 {
        color: var(--v-accent-lighten4) !important;
        caret-color: var(--v-accent-lighten4) !important;
    }

    .v-application .accent.lighten-3 {
        background-color: var(--v-accent-lighten3) !important;
        border-color: var(--v-accent-lighten3) !important;
    }

    .v-application .accent--text.text--lighten-3 {
        color: var(--v-accent-lighten3) !important;
        caret-color: var(--v-accent-lighten3) !important;
    }

    .v-application .accent.lighten-2 {
        background-color: var(--v-accent-lighten2) !important;
        border-color: var(--v-accent-lighten2) !important;
    }

    .v-application .accent--text.text--lighten-2 {
        color: var(--v-accent-lighten2) !important;
        caret-color: var(--v-accent-lighten2) !important;
    }

    .v-application .accent.lighten-1 {
        background-color: var(--v-accent-lighten1) !important;
        border-color: var(--v-accent-lighten1) !important;
    }

    .v-application .accent--text.text--lighten-1 {
        color: var(--v-accent-lighten1) !important;
        caret-color: var(--v-accent-lighten1) !important;
    }

    .v-application .accent.darken-1 {
        background-color: var(--v-accent-darken1) !important;
        border-color: var(--v-accent-darken1) !important;
    }

    .v-application .accent--text.text--darken-1 {
        color: var(--v-accent-darken1) !important;
        caret-color: var(--v-accent-darken1) !important;
    }

    .v-application .accent.darken-2 {
        background-color: var(--v-accent-darken2) !important;
        border-color: var(--v-accent-darken2) !important;
    }

    .v-application .accent--text.text--darken-2 {
        color: var(--v-accent-darken2) !important;
        caret-color: var(--v-accent-darken2) !important;
    }

    .v-application .accent.darken-3 {
        background-color: var(--v-accent-darken3) !important;
        border-color: var(--v-accent-darken3) !important;
    }

    .v-application .accent--text.text--darken-3 {
        color: var(--v-accent-darken3) !important;
        caret-color: var(--v-accent-darken3) !important;
    }

    .v-application .accent.darken-4 {
        background-color: var(--v-accent-darken4) !important;
        border-color: var(--v-accent-darken4) !important;
    }

    .v-application .accent--text.text--darken-4 {
        color: var(--v-accent-darken4) !important;
        caret-color: var(--v-accent-darken4) !important;
    }

    .v-application .error {
        background-color: var(--v-error-base) !important;
        border-color: var(--v-error-base) !important;
    }

    .v-application .error--text {
        color: var(--v-error-base) !important;
        caret-color: var(--v-error-base) !important;
    }

    .v-application .error.lighten-5 {
        background-color: var(--v-error-lighten5) !important;
        border-color: var(--v-error-lighten5) !important;
    }

    .v-application .error--text.text--lighten-5 {
        color: var(--v-error-lighten5) !important;
        caret-color: var(--v-error-lighten5) !important;
    }

    .v-application .error.lighten-4 {
        background-color: var(--v-error-lighten4) !important;
        border-color: var(--v-error-lighten4) !important;
    }

    .v-application .error--text.text--lighten-4 {
        color: var(--v-error-lighten4) !important;
        caret-color: var(--v-error-lighten4) !important;
    }

    .v-application .error.lighten-3 {
        background-color: var(--v-error-lighten3) !important;
        border-color: var(--v-error-lighten3) !important;
    }

    .v-application .error--text.text--lighten-3 {
        color: var(--v-error-lighten3) !important;
        caret-color: var(--v-error-lighten3) !important;
    }

    .v-application .error.lighten-2 {
        background-color: var(--v-error-lighten2) !important;
        border-color: var(--v-error-lighten2) !important;
    }

    .v-application .error--text.text--lighten-2 {
        color: var(--v-error-lighten2) !important;
        caret-color: var(--v-error-lighten2) !important;
    }

    .v-application .error.lighten-1 {
        background-color: var(--v-error-lighten1) !important;
        border-color: var(--v-error-lighten1) !important;
    }

    .v-application .error--text.text--lighten-1 {
        color: var(--v-error-lighten1) !important;
        caret-color: var(--v-error-lighten1) !important;
    }

    .v-application .error.darken-1 {
        background-color: var(--v-error-darken1) !important;
        border-color: var(--v-error-darken1) !important;
    }

    .v-application .error--text.text--darken-1 {
        color: var(--v-error-darken1) !important;
        caret-color: var(--v-error-darken1) !important;
    }

    .v-application .error.darken-2 {
        background-color: var(--v-error-darken2) !important;
        border-color: var(--v-error-darken2) !important;
    }

    .v-application .error--text.text--darken-2 {
        color: var(--v-error-darken2) !important;
        caret-color: var(--v-error-darken2) !important;
    }

    .v-application .error.darken-3 {
        background-color: var(--v-error-darken3) !important;
        border-color: var(--v-error-darken3) !important;
    }

    .v-application .error--text.text--darken-3 {
        color: var(--v-error-darken3) !important;
        caret-color: var(--v-error-darken3) !important;
    }

    .v-application .error.darken-4 {
        background-color: var(--v-error-darken4) !important;
        border-color: var(--v-error-darken4) !important;
    }

    .v-application .error--text.text--darken-4 {
        color: var(--v-error-darken4) !important;
        caret-color: var(--v-error-darken4) !important;
    }

    .v-application .info {
        background-color: var(--v-info-base) !important;
        border-color: var(--v-info-base) !important;
    }

    .v-application .info--text {
        color: var(--v-info-base) !important;
        caret-color: var(--v-info-base) !important;
    }

    .v-application .info.lighten-5 {
        background-color: var(--v-info-lighten5) !important;
        border-color: var(--v-info-lighten5) !important;
    }

    .v-application .info--text.text--lighten-5 {
        color: var(--v-info-lighten5) !important;
        caret-color: var(--v-info-lighten5) !important;
    }

    .v-application .info.lighten-4 {
        background-color: var(--v-info-lighten4) !important;
        border-color: var(--v-info-lighten4) !important;
    }

    .v-application .info--text.text--lighten-4 {
        color: var(--v-info-lighten4) !important;
        caret-color: var(--v-info-lighten4) !important;
    }

    .v-application .info.lighten-3 {
        background-color: var(--v-info-lighten3) !important;
        border-color: var(--v-info-lighten3) !important;
    }

    .v-application .info--text.text--lighten-3 {
        color: var(--v-info-lighten3) !important;
        caret-color: var(--v-info-lighten3) !important;
    }

    .v-application .info.lighten-2 {
        background-color: var(--v-info-lighten2) !important;
        border-color: var(--v-info-lighten2) !important;
    }

    .v-application .info--text.text--lighten-2 {
        color: var(--v-info-lighten2) !important;
        caret-color: var(--v-info-lighten2) !important;
    }

    .v-application .info.lighten-1 {
        background-color: var(--v-info-lighten1) !important;
        border-color: var(--v-info-lighten1) !important;
    }

    .v-application .info--text.text--lighten-1 {
        color: var(--v-info-lighten1) !important;
        caret-color: var(--v-info-lighten1) !important;
    }

    .v-application .info.darken-1 {
        background-color: var(--v-info-darken1) !important;
        border-color: var(--v-info-darken1) !important;
    }

    .v-application .info--text.text--darken-1 {
        color: var(--v-info-darken1) !important;
        caret-color: var(--v-info-darken1) !important;
    }

    .v-application .info.darken-2 {
        background-color: var(--v-info-darken2) !important;
        border-color: var(--v-info-darken2) !important;
    }

    .v-application .info--text.text--darken-2 {
        color: var(--v-info-darken2) !important;
        caret-color: var(--v-info-darken2) !important;
    }

    .v-application .info.darken-3 {
        background-color: var(--v-info-darken3) !important;
        border-color: var(--v-info-darken3) !important;
    }

    .v-application .info--text.text--darken-3 {
        color: var(--v-info-darken3) !important;
        caret-color: var(--v-info-darken3) !important;
    }

    .v-application .info.darken-4 {
        background-color: var(--v-info-darken4) !important;
        border-color: var(--v-info-darken4) !important;
    }

    .v-application .info--text.text--darken-4 {
        color: var(--v-info-darken4) !important;
        caret-color: var(--v-info-darken4) !important;
    }

    .v-application .success {
        background-color: var(--v-success-base) !important;
        border-color: var(--v-success-base) !important;
    }

    .v-application .success--text {
        color: var(--v-success-base) !important;
        caret-color: var(--v-success-base) !important;
    }

    .v-application .success.lighten-5 {
        background-color: var(--v-success-lighten5) !important;
        border-color: var(--v-success-lighten5) !important;
    }

    .v-application .success--text.text--lighten-5 {
        color: var(--v-success-lighten5) !important;
        caret-color: var(--v-success-lighten5) !important;
    }

    .v-application .success.lighten-4 {
        background-color: var(--v-success-lighten4) !important;
        border-color: var(--v-success-lighten4) !important;
    }

    .v-application .success--text.text--lighten-4 {
        color: var(--v-success-lighten4) !important;
        caret-color: var(--v-success-lighten4) !important;
    }

    .v-application .success.lighten-3 {
        background-color: var(--v-success-lighten3) !important;
        border-color: var(--v-success-lighten3) !important;
    }

    .v-application .success--text.text--lighten-3 {
        color: var(--v-success-lighten3) !important;
        caret-color: var(--v-success-lighten3) !important;
    }

    .v-application .success.lighten-2 {
        background-color: var(--v-success-lighten2) !important;
        border-color: var(--v-success-lighten2) !important;
    }

    .v-application .success--text.text--lighten-2 {
        color: var(--v-success-lighten2) !important;
        caret-color: var(--v-success-lighten2) !important;
    }

    .v-application .success.lighten-1 {
        background-color: var(--v-success-lighten1) !important;
        border-color: var(--v-success-lighten1) !important;
    }

    .v-application .success--text.text--lighten-1 {
        color: var(--v-success-lighten1) !important;
        caret-color: var(--v-success-lighten1) !important;
    }

    .v-application .success.darken-1 {
        background-color: var(--v-success-darken1) !important;
        border-color: var(--v-success-darken1) !important;
    }

    .v-application .success--text.text--darken-1 {
        color: var(--v-success-darken1) !important;
        caret-color: var(--v-success-darken1) !important;
    }

    .v-application .success.darken-2 {
        background-color: var(--v-success-darken2) !important;
        border-color: var(--v-success-darken2) !important;
    }

    .v-application .success--text.text--darken-2 {
        color: var(--v-success-darken2) !important;
        caret-color: var(--v-success-darken2) !important;
    }

    .v-application .success.darken-3 {
        background-color: var(--v-success-darken3) !important;
        border-color: var(--v-success-darken3) !important;
    }

    .v-application .success--text.text--darken-3 {
        color: var(--v-success-darken3) !important;
        caret-color: var(--v-success-darken3) !important;
    }

    .v-application .success.darken-4 {
        background-color: var(--v-success-darken4) !important;
        border-color: var(--v-success-darken4) !important;
    }

    .v-application .success--text.text--darken-4 {
        color: var(--v-success-darken4) !important;
        caret-color: var(--v-success-darken4) !important;
    }

    .v-application .warning {
        background-color: var(--v-warning-base) !important;
        border-color: var(--v-warning-base) !important;
    }

    .v-application .warning--text {
        color: var(--v-warning-base) !important;
        caret-color: var(--v-warning-base) !important;
    }

    .v-application .warning.lighten-5 {
        background-color: var(--v-warning-lighten5) !important;
        border-color: var(--v-warning-lighten5) !important;
    }

    .v-application .warning--text.text--lighten-5 {
        color: var(--v-warning-lighten5) !important;
        caret-color: var(--v-warning-lighten5) !important;
    }

    .v-application .warning.lighten-4 {
        background-color: var(--v-warning-lighten4) !important;
        border-color: var(--v-warning-lighten4) !important;
    }

    .v-application .warning--text.text--lighten-4 {
        color: var(--v-warning-lighten4) !important;
        caret-color: var(--v-warning-lighten4) !important;
    }

    .v-application .warning.lighten-3 {
        background-color: var(--v-warning-lighten3) !important;
        border-color: var(--v-warning-lighten3) !important;
    }

    .v-application .warning--text.text--lighten-3 {
        color: var(--v-warning-lighten3) !important;
        caret-color: var(--v-warning-lighten3) !important;
    }

    .v-application .warning.lighten-2 {
        background-color: var(--v-warning-lighten2) !important;
        border-color: var(--v-warning-lighten2) !important;
    }

    .v-application .warning--text.text--lighten-2 {
        color: var(--v-warning-lighten2) !important;
        caret-color: var(--v-warning-lighten2) !important;
    }

    .v-application .warning.lighten-1 {
        background-color: var(--v-warning-lighten1) !important;
        border-color: var(--v-warning-lighten1) !important;
    }

    .v-application .warning--text.text--lighten-1 {
        color: var(--v-warning-lighten1) !important;
        caret-color: var(--v-warning-lighten1) !important;
    }

    .v-application .warning.darken-1 {
        background-color: var(--v-warning-darken1) !important;
        border-color: var(--v-warning-darken1) !important;
    }

    .v-application .warning--text.text--darken-1 {
        color: var(--v-warning-darken1) !important;
        caret-color: var(--v-warning-darken1) !important;
    }

    .v-application .warning.darken-2 {
        background-color: var(--v-warning-darken2) !important;
        border-color: var(--v-warning-darken2) !important;
    }

    .v-application .warning--text.text--darken-2 {
        color: var(--v-warning-darken2) !important;
        caret-color: var(--v-warning-darken2) !important;
    }

    .v-application .warning.darken-3 {
        background-color: var(--v-warning-darken3) !important;
        border-color: var(--v-warning-darken3) !important;
    }

    .v-application .warning--text.text--darken-3 {
        color: var(--v-warning-darken3) !important;
        caret-color: var(--v-warning-darken3) !important;
    }

    .v-application .warning.darken-4 {
        background-color: var(--v-warning-darken4) !important;
        border-color: var(--v-warning-darken4) !important;
    }

    .v-application .warning--text.text--darken-4 {
        color: var(--v-warning-darken4) !important;
        caret-color: var(--v-warning-darken4) !important;
    }</style>
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-19e06ac6.9864336b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-2c59789a.c26f4895.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5dc2f224.009d7923.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-6a09404c.a7f98917.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-e7464a4c.942ad61f.css">

    <script src="../../static/app/js/jquery-2.1.1.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            GetAllFarmlandMain();
            traditionalFarmlandMain();
        });
        function GetAllFarmlandMain()
        {
            var login={"login":'123'};
            $.ajax({
                url:'/FarmlandManage/intelligentFarmlandMain',
                data:JSON.stringify(login),
                type: 'POST',
                contentType: 'application/json',
                success: function (res)
                {
                    for(i=res.data.length-1;i>=0;i--)
                    {
                        var data={"farmlandId":res.data[i].farmlandId,"farmId":res.data[i].farmId}
                        content = '<div class="pa-2 col col-6">';
                        content += '<a href="/IndexController/SmartFarmland?data='+encodeURIComponent(JSON.stringify(data))+'"'
                            +'class="mx-auto card-radius v-card v-card--link v-sheet theme--light" tabindex="0">';
                        content += '<div class="v-responsive v-image" style="height: 130px;">';
                        content += '<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content += '<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content += '<div class="v-responsive__content" style="width: 1080px;"></div>';
                        content += '</div>';
                        content += '<div class="v-card__text pt-2 pb-1">';
                        content += '<div class="row pa-1 pt-0"><span class="font-weight-medium primary--text">';
                        content += res.data[i].farmlandName;
                        content += '</span>';
                        content += '<div class="spacer"></div>';
                        content += '<span>';
                        content += res.data[i].farmlandArea;
                        content += '</span></div>';
                        content += '<div class="row pa-1 pt-0"><span>';
                        content += '认养人：';
                        content += '</span>';
                        content += '<div class="spacer"></div>';
                        content += '<span>';
                        content += res.data[i].farmerName;
                        content += '</span></div>';
                        content += '</div>';
                        content += '</a></div>';
                        $('#wisdom').append(content);
                    }

                }
            })
        };

        function traditionalFarmlandMain()
        {
            var login={"login":'123'};
            $.ajax({
                url:'/FarmlandManage/traditionalFarmlandMain',
                data:JSON.stringify(login),
                type: 'POST',
                contentType: 'application/json',
                success: function (res)
                {

                    for(i=res.data.length-1;i>=0;i--){
                        var data={"farmlandId":res.data[i].farmlandId,"farmId":res.data[i].farmId}
                        content ='<div class="pa-2 col col-6">';
                        content +='<a href="/IndexController/TraditionalFarmland?data='+encodeURIComponent(JSON.stringify(data))+'" ' +
                            'class="mx-auto card-radius v-card v-card--link v-sheet theme--light" tabindex="0">';
                        content +='<div class="v-responsive v-image" style="height: 130px;">';
                        content +='<div class="v-responsive__sizer" style="padding-bottom: 177.778%;"></div>';
                        content +='<div class="v-image__image v-image__image--cover" style="background-image: url(&quot;http://ag.soft.gszh.cn/img/cropland-title.236f1aff.jpg&quot;); background-position: center center;"></div>';
                        content +='<div class="v-responsive__content" style="width: 1080px;"></div>';
                        content +='</div>';
                        content +='<div class="v-card__text pt-2 pb-1">';
                        content +='<div class="row pa-1 pt-0"><span class="font-weight-medium primary--text">';
                        content +=res.data[i].farmlandName;
                        content +='</span>';
                        content +='<div class="spacer"></div>';
                        content +='<span>';
                        content +=res.data[i].farmlandArea;
                        content +='</span></div>';
                        content +='<div class="row pa-1 pt-0"><span>';
                        content +='认养人：';
                        content +='</span>';
                        content +='<div class="spacer"></div>';
                        content +='<span>';
                        content +=res.data[i].farmerName;
                        content +='</span></div>';
                        content +='</div>';
                        content +='</a></div>';
                        $('#tradition').append(content);
                    }
                }
            })
        };

        function changeW(){
            var wisdom1=$('#wisdom1');
            var wisdom2=$('#wisdom2');
            var tradition1=$('#tradition1');
            var tradition2=$('#tradition2');
            var c=document.getElementById('wisdom1').className;
            if(c=="font-weight-medium v-tab"){
                wisdom1.addClass('v-tab--active');
                wisdom1.attr('aria-selected','true');
                tradition1.removeClass('v-tab--active');
                tradition1.attr('aria-selected','false');
                wisdom2.addClass('v-window-item--active');
                wisdom2.css('display','block');
                tradition2.removeClass('v-window-item--active');
                tradition2.css('display','none');
            }
        }

        function changeT(){
            var wisdom1=$('#wisdom1');
            var wisdom2=$('#wisdom2');
            var tradition1=$('#tradition1');
            var tradition2=$('#tradition2');
            var c=document.getElementById('tradition1').className;
            if(c=="font-weight-medium v-tab"){
                wisdom1.removeClass('v-tab--active');
                wisdom1.attr('aria-selected','false');
                tradition1.addClass('v-tab--active');
                tradition1.attr('aria-selected','true');
                wisdom2.removeClass('v-window-item--active');
                wisdom2.css('display','none');
                tradition2.addClass('v-window-item--active');
                tradition2.css('display','block');
            }
        }
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="farmlandManage">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent" style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div data-v-239fab31="">
                <header data-v-2eae54fc="" data-v-239fab31=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-toolbar--extended v-app-bar v-app-bar--fixed primary"
                        data-booted="true" style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div onclick="window.location.href='/IndexController/Index';" data-v-2eae54fc="" class="toolbar-left"><img data-v-2eae54fc=""
                                                                                                                                                         src="../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                                                                                                         alt="icon"></div>
                            农田管理
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                    <div class="v-toolbar__extension" style="height: 48px;">
                        <div data-v-239fab31="" class="v-tabs v-tabs--grow theme--light">
                            <div role="tablist"
                                 class="v-item-group theme--light v-slide-group v-tabs-bar v-tabs-bar--show-arrows primary--text"
                                 data-booted="true">
                                <div class="v-slide-group__prev v-slide-group__prev--disabled"><i aria-hidden="true"
                                                                                                  class="v-icon notranslate v-icon--disabled mdi mdi-chevron-left theme--light"></i>
                                </div>
                                <div class="v-slide-group__wrapper">
                                    <div class="v-slide-group__content v-tabs-bar__content">
                                        <button id="wisdom1" style="height: 48px; width: 194px;" aria-selected="true" class="font-weight-medium v-tab v-tab--active" onclick="changeW()">
                                            智慧农田
                                        </button>
                                        <button id="tradition1" style="height: 48px; width: 194px;" aria-selected="false" class="font-weight-medium v-tab" onclick="changeT()">
                                            传统农田
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <main data-v-239fab31="" class="v-content grey lighten-5" data-booted="true"
                      style="padding: 96px 0px 0px;">
                    <div class="v-content__wrap">
                        <div data-v-239fab31="" class="container">
                            <div data-v-239fab31="" class="py-2">
                                <button type="button"
                                        class="grey lighten-2 grey--text text--lighten-1 v-btn v-btn--block v-btn--depressed v-btn--flat v-btn--outlined v-btn--rounded theme--light v-size--small"><span
                                        class="v-btn__content"><i aria-hidden="true"
                                                                  class="v-icon notranslate mdi mdi-magnify theme--light"
                                                                  style="font-size: 16px;"></i>
      搜索
    </span></button>
                                <div role="dialog" class="v-dialog__container">
                                    <div role="document" class="v-dialog__content" style="z-index: 0;">
                                        <div class="v-dialog v-dialog--fullscreen v-dialog--scrollable"
                                             style="display: none;"></div>
                                    </div>
                                </div>
                            </div>
                            <div data-v-239fab31=""
                                 class="v-window px-2 grey lighten-5 v-item-group theme--light v-tabs-items">
                                <div class="v-window__container">
                                    <!--智慧农田-->
                                    <div id="wisdom2" data-v-239fab31="" class="v-window-item v-window-item--active" style="">
                                        <div data-v-239fab31="" class="row" id="wisdom">
                                        </div>
                                    </div>
                                    <!--传统农田-->
                                    <div id="tradition2" data-v-239fab31="" class="v-window-item" style="display: none;">
                                        <div data-v-239fab31="" class="row" id="tradition">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>