<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<!-- saved from url=(0045)http://ag.soft.gszh.cn/repository/cultivars/1 -->
<html lang="zh-Hans" class="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="Cache-control" content="no-store">
    <title>智恒智慧生态农业园</title><!--<base href="/">-->
    <base href=".">
    <link href="../../static/app/css/apphead.css" rel="stylesheet">
    <link href="../../static/app/css/chunk-107b5196.d80540dc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-1574105c.c6210d3e.css" rel="prefetch">
    <link href="../../static/app/css/chunk-19e06ac6.9864336b.css" rel="prefetch">
    <link href="../../static/app/css/chunk-2c59789a.c26f4895.css" rel="prefetch">
    <link href="../../static/app/css/chunk-34951fe0.a46de2ef.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css" rel="prefetch">
    <link href="../../static/app/css/chunk-5dc2f224.009d7923.css" rel="prefetch">
    <link href="../../static/app/css/chunk-73671046.d07ccefa.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7ce1909b.1976c0ac.css" rel="prefetch">
    <link href="../../static/app/css/chunk-7df73ecf.f28a53a1.css" rel="prefetch">
    <link href="../../static/app/css/chunk-b14a74f2.e8d83e75.css" rel="prefetch">
    <link href="../../static/app/css/chunk-c96b6fda.9a391f00.css" rel="prefetch">
    <link href="../../static/app/css/app.f748b306.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="preload" as="style">
    <link href="../../static/app/css/chunk-vendors.bba9413c.css" rel="stylesheet">
    <link href="../../static/app/css/app.f748b306.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7df73ecf.f28a53a1.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7a3eedc6.8907fdb4.css">t>
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-19e06ac6.9864336b.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-b14a74f2.e8d83e75.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-34951fe0.a46de2ef.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-c96b6fda.9a391f00.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-7ce1909b.1976c0ac.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-73671046.d07ccefa.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-2c59789a.c26f4895.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5cfc99f5.f769f2fc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-5dc2f224.009d7923.css">>
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-107b5196.d80540dc.css">
    <link rel="stylesheet" type="text/css" href="../../static/app/css/chunk-1574105c.c6210d3e.css">
    <script src="../../static/app/js/jquery-2.1.1.min.js"></script>
    <script>
        $(document).ready(function(){
            getVarietyDetail();
        })
        function getVarietyDetail(){
            var varietyName="${data.varietyName}";
            var data={"varietyName":varietyName};
            $.ajax({
                url:'/ExpertLibrary/Detail',
                data:JSON.stringify(data),
                type:'POST',
                contentType:'application/json',
                success:function(res){
                    for(i=0;i<=res.data.length;i++){
                        document.getElementById("varietyName").innerHTML=res.data[i].varietyName;
                        document.getElementById("describetion").innerHTML=res.data[i].describetion;
                        document.getElementById("suitableGrowthTime").innerHTML=res.data[i].suitableGrowthTime;
                        document.getElementById("cycleLength").innerHTML=res.data[i].cycleLength;
                        document.getElementById("perMu").innerHTML=res.data[i].perMu;
                        document.getElementById("nutritionalValue").innerHTML=res.data[i].nutritionalValue;
                        document.getElementById("plantHabit").innerHTML=res.data[i].plantHabit;
                        document.getElementById("plantingMethod").innerHTML=res.data[i].plantingMethod;
                        document.getElementById("plantingConsumption").innerHTML=res.data[i].plantingConsumption;
                    }
                }
            })
        }
    </script>
</head>
<body>
<div data-app="true" class="v-application patch v-application--is-ltr theme--light" id="app">
    <div class="v-application--wrap">
        <div style="height: 100%; width: 100%;">
            <div role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"
                 class="v-progress-linear v-progress-linear--fixed theme--light"
                 style="height: 0px; top: 0px; z-index: 9999;">
                <div class="v-progress-linear__background accent" style="opacity: 0.3; left: 0%; width: 100%;"></div>
                <div class="v-progress-linear__buffer" style="height: 0px;"></div>
                <div class="v-progress-linear__determinate accent" style="width: 0%;"></div>
            </div>
            <div data-v-226bd49d="" cultivarid="1">
                <header data-v-2eae54fc="" data-v-226bd49d=""
                        class="app-bar v-sheet v-sheet--tile theme--dark v-toolbar v-toolbar--dense v-app-bar v-app-bar--fixed primary"
                        data-booted="true" style="margin-top: 0px; transform: translateY(0px); left: 0px; right: 0px;">
                    <div class="v-toolbar__content" style="height: 48px;">
                        <div data-v-2eae54fc="" class="v-toolbar__title">
                            <div data-v-2eae54fc="" class="toolbar-left" onclick="window.location.href='javascript:window.history.back(-1)'"><img data-v-2eae54fc=""
                                                                              src="../../static/app/img/arrow-left.ac6c6d62.svg"
                                                                              alt="icon"></div>
                            作物详情
                            <div data-v-2eae54fc="" class="toolbar-right"></div>
                        </div>
                    </div>
                </header>
                <main data-v-226bd49d="" class="v-content white" data-booted="true" style="padding: 48px 0px 0px;">
                    <div class="v-content__wrap">
                        <div data-v-226bd49d="" class="zjk_detail">
                            <div data-v-226bd49d="" class="zjk_detail_top">
                                <div data-v-226bd49d="" class="zjkdt_left"><img data-v-226bd49d=""
                                                                                src="../../static/app/img/baocai.jpg"></div>
                                <div data-v-226bd49d="" class="zjkdt_right">
                                    <div data-v-226bd49d="" class="zjkdt_right_title" id="varietyName"></div>
                                    <p data-v-226bd49d=""><span data-v-226bd49d="">播种季节</span><span data-v-226bd49d="" id="suitableGrowthTime"></span>
                                    </p>
                                    <p data-v-226bd49d=""><span data-v-226bd49d="">生长周期</span><span data-v-226bd49d="" id="cycleLength"></span>
                                    </p>
                                    <p data-v-226bd49d=""><span data-v-226bd49d="">亩产</span><span data-v-226bd49d="" id="perMu"></span>
                                    </p></div>
                                <div data-v-226bd49d="" class="clearfix"></div>
                            </div>
                            <div data-v-226bd49d="" class="zjkd_content">
                                <div data-v-226bd49d="" class="zjkdc_one"><p data-v-226bd49d="" id="describetion">
                                    </p></div>
                                <div data-v-226bd49d="" class="zjkdc_two">营养价值</div>
                                <div data-v-226bd49d="" class="zjkdc_one"><p data-v-226bd49d="" id="nutritionalValue">

                                </p></div>
                                <div data-v-226bd49d="" class="zjkdc_two">习性</div>
                                <div data-v-226bd49d="" class="zjkdc_one"><p data-v-226bd49d="" id="plantHabit">

                                </p></div>
                                <div data-v-226bd49d="" class="zjkdc_two">栽培方法</div>
                                <div data-v-226bd49d="" class="zjkdc_one"><p data-v-226bd49d="" id="plantingMethod">
                                </p></div>
                                <div data-v-226bd49d="" class="zjkdc_two">消耗</div>
                                <div data-v-226bd49d="" class="zjkdc_one"><p data-v-226bd49d="" id="plantingConsumption">

                                </p></div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
</body>
</html>