<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="../static/layui/css/layui.css">
</head>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>编辑预警信息</legend>
</fieldset>
<div class="layui-form  layui-from-pane" action="" accept-charset="UTF-8" id="formdemo">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" name="id" id="date1" autocomplete="off" class="layui-input" readonly="" value="${data.id}">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">农场ID</label>
            <div class="layui-input-inline">
                <input type="text" name="farm_id" autocomplete="off" placeholder="请输入农场ID" class="layui-input" value="${data.farmId}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">农田ID</label>
        <div class="layui-input-block">
            <input type="text" name="farmland_id" autocomplete="off" placeholder="请输入农田ID" class="layui-input" value="${data.farmlandId}">
            <div class="layui-form-mid layui-word-aux">请务必填写农田ID</div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">预警标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" autocomplete="off" placeholder="请输入预警标题" class="layui-input" value="${data.title}">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">预警内容</label>
        <div class="layui-input-block">
            <input type="text" name="content" autocomplete="off" placeholder="请输入预警内容" class="layui-input" value="${data.content}">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">提醒时间</label>
        <div class="layui-input-inline">
            <input type="text" name="remind_time" placeholder="点击选择提醒时间" autocomplete="off" class="layui-input test-item" id="remindtime" value="${data.remindTime}">
        </div>
    </div>



    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">提醒状态</label>
            <div class="layui-input-block">
                <input type="text" name="status"autocomplete="off" class="layui-input" value="${data.status}">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">阅读时间</label>
            <div class="layui-input-inline">
                <input type="text" name="read_time" autocomplete="off"  placeholder="点击选择阅读时间" class="layui-input test-item" id="readtime" value="${data.readTime}">
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">提醒人</label>
        <div class="layui-input-inline">
            <input type="text" name="reminder" placeholder="请输入提醒人" autocomplete="off" class="layui-input" value="${data.reminder}">
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">类别</label>
            <div class="layui-input-block">
                <input type="text" name="type"autocomplete="off" class="layui-input" value="${data.type}">
            </div>
        </div>
    </div>



    <div class="layui-form-item">
        <button class="layui-btn layui-btn-submit " lay-submit="" lay-filter="edit" style="margin-left: 45%;" onsubmit="return false">提交</button>
    </div>

</div>

<script src="../static/layui/layui.js" charset="utf-8"></script>
<script src="../static/layui/layui.all.js" charset="utf-8"></script>


<script>


    layui.use(['form','layer'] ,function(){
        var
            form=layui.form,
            layer = layui.layer,
            laydate=layui.laydate
        form.render();


        laydate.render({
            elem:'#readtime',
            type:'datetime',
            trigger : 'click'
            });

        laydate.render({
            elem:'#remindtime',
            type:'datetime',
            trigger : 'click'
        });

        //监听提交
        form.on('submit(edit)', function(data)
        {
            layui.jquery.ajax({
                url: "/Warning/edit",
                type: "POST",
                async: false,   //不要让它异步提交
                data:JSON.stringify(data.field),
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    if(data.msg!='0'){
                        layer.alert("修改成功",{icon: 1,time:2000},function () {
                            layer.close(layer.index);
                            window.parent.location.reload();    //重新加载父页面，进行数据刷新
                        });
                    } else{
                        layer.alert("修改失败",{icon: 2,time:2000});
                    }
                }
            });
            return false;
        });

    });
</script>

</body>
</html>

