<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" type="text/css" href="../static/layui/css/layui.css">

</head>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>农田设备信息</legend>
</fieldset>
<div class="layui-form  layui-from-pane" action="" accept-charset="UTF-8" id="formdemo">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" name="id" id="date1" autocomplete="off" class="layui-input" readonly="" value="${data.id}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-inline">
        <label class="layui-form-label">编号</label>
        <div class="layui-input-block">
            <input type="text" name="num" autocomplete="off" placeholder="编号" class="layui-input" value="${data.num}">
        </div>
        </div>

        <div class="layui-inline">
        <label class="layui-form-label">产品编号</label>
        <div class="layui-input-block">
            <input type="text" name="deviceProduct" autocomplete="off" placeholder="产品编号" class="layui-input" value="${data.deviceProduct}">
        </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">产品用途类型</label>
        <div class="layui-input-block">
            <input type="text" name="usageType" autocomplete="off" placeholder="产品用途类型" class="layui-input" value="${data.usageType}">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">安装区域</label>
        <div class="layui-input-block">
            <input type="text" name="area" autocomplete="off" placeholder="安装区域" class="layui-input" value="${data.area}">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">农田ID</label>
        <div class="layui-input-inline">
            <input type="text" name="farmlandId" placeholder="请输入农田ID" autocomplete="off" class="layui-input" value="${data.farmlandId}">
        </div>

        <label class="layui-form-label">农场ID</label>
        <div class="layui-input-inline">
            <input type="text" name="farmId" placeholder="请输入农场ID" autocomplete="off" class="layui-input" value="${data.farmId}">
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">设备工艺</label>
            <div class="layui-input-block">
                <input type="text" name="technology"autocomplete="off" placeholder="设备工艺" class="layui-input" value="${data.technology}">
            </div>
        </div>
    </div>



    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">设备参数</label>
            <div class="layui-input-block">
                <input type="text" name="parameter" autocomplete="off" placeholder="设备参数" class="layui-input" value="${data.parameter}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">设备协议</label>
            <div class="layui-input-block">
                <input type="text" name="protocol"autocomplete="off" placeholder="设备协议" class="layui-input" value="${data.protocol}">
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">设备地址</label>
            <div class="layui-input-block">
                <input type="text" name="host"autocomplete="off" placeholder="设备地址" class="layui-input" value="${data.host}">
            </div>
        </div>


        <div class="layui-inline">
            <label class="layui-form-label">工作状态</label>
            <div class="layui-input-block">
                <input type="text" name="workStatus"autocomplete="off" placeholder="工作状态" class="layui-input" value="${data.workStatus}">
            </div>
        </div>
    </div>



    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">dtu_设备</label>
            <div class="layui-input-block">
                <input type="text" name="dtuDevice"autocomplete="off" placeholder="dtu_设备" class="layui-input" value="${data.dtuDevice}">
            </div>
        </div>


        <div class="layui-inline">
            <label class="layui-form-label">产品类型</label>
            <div class="layui-input-block">
                <input type="text" name="type"autocomplete="off" placeholder="产品类型" class="layui-input" value="${data.type}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <button class="layui-btn layui-btn-submit " lay-submit="" lay-filter="edit" style="margin-left: 45%;" onsubmit="return false">提交</button>
    </div>

</div>

<script src="../static/layui/layui.js" charset="utf-8"></script>
<script src="../static/layui/layui.all.js" charset="utf-8"></script>

<script>


    layui.use(['form','layer'] ,function(){
        var
            form=layui.form,
            layer = layui.layer

        var index = parent.layer.getFrameIndex(window.name);
        form.render();


        //监听提交
        form.on('submit(edit)', function(data)
        {
            layui.jquery.ajax({
                url: "/Device/edit",
                type: "POST",
                async: false,   //不要让它异步提交
                data:JSON.stringify(data.field),
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    if(data.msg!='0'){
                        layer.alert("修改成功",{icon: 1,time:2000},function () {
                            layer.close(layer.index);
                            window.parent.location.reload();    //重新加载父页面，进行数据刷新
                        });
                    } else{
                        layer.alert("修改失败",{icon: 2,time:2000});
                    }
                }
            });
            return false;
        });

    });
</script>

</body>
</html>

