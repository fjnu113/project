<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" type="text/css" href="../static/layui/css/layui.css">
</head>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加农田信息</legend>
</fieldset>
<div class="layui-form  layui-from-pane" action="" accept-charset="UTF-8" id="formdemo">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">农田ID</label>
            <div class="layui-input-inline">
                <input type="text" name="num" autocomplete="off" placeholder="请输入农田ID" class="layui-input">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">农田面积</label>
        <div class="layui-input-block">
            <input type="text" name="area" autocomplete="off" placeholder="请输入农田面积" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">农田位置</label>
        <div class="layui-input-block">
            <input type="text" name="position" autocomplete="off" placeholder="请输入农田位置" class="layui-input" >
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">光照系数</label>
        <div class="layui-input-block">
            <input type="text" name="ic" autocomplete="off" placeholder="请输入光照系数" class="layui-input" >
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">负责人</label>
        <div class="layui-input-inline">
            <input type="text" name="farmer" placeholder="请输入负责人" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">请务必填写负责人</div>
    </div>



    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">土壤状态</label>
            <div class="layui-input-block">
                <input type="text" name="status"autocomplete="off" class="layui-input" >
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">土壤深度</label>
            <div class="layui-input-inline">
                <input type="text" name="deepth" autocomplete="off" class="layui-input" >
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">所属农场</label>
        <div class="layui-input-inline">
            <input type="text" name="farm_id" placeholder="请输入所属农场" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid layui-word-aux">请务必填写所属农场</div>
    </div>


    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">类型</label>
            <div class="layui-input-block">
                <input type="text" name="type"autocomplete="off" class="layui-input" placeholder="请输入农场类型">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">值守类型</label>
            <div class="layui-input-inline">
                <input type="text" name="duty_type" autocomplete="off" class="layui-input" placeholder="请输入值守类型">
            </div>
        </div>
    </div>



    <div class="layui-form-item">
        <button class="layui-btn layui-btn-submit " lay-submit="" lay-filter="add" style="margin-left: 45%;" onsubmit="return false">提交</button>
    </div>

</div>

<script src="../static/layui/layui.js" charset="utf-8"></script>
<script src="../static/layui/layui.all.js" charset="utf-8"></script>

<script>


    layui.use(['form','layer'] ,function(){
        var
            form=layui.form,
            layer = layui.layer

        var index = parent.layer.getFrameIndex(window.name);
        form.render();


        //监听提交
        form.on('submit(add)', function(data)
        {
            layui.jquery.ajax({
                url: "/FarmlandInfomation/add",
                type: "POST",
                async: false,   //不要让它异步提交
                data:JSON.stringify(data.field),
                contentType: 'application/json;charset=utf-8',
                loadingText:'数据提交中，请稍后',
                success: function (data) {
                    if(data.msg!='0'){
                        layer.alert("添加成功",{icon: 1,time:2000},function () {
                            layer.close(layer.index);
                            window.parent.location.reload();    //重新加载父页面，进行数据刷新
                        });
                    } else{
                        layer.alert("添加失败",{icon: 2,time:2000});
                    }
                }
            });
            return false;
        });

    });
</script>

</body>
</html>

