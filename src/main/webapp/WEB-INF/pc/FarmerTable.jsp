<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="../static/layui/css/layui.css">
</head>
<body>
<div style="margin-bottom: 5px;">

</div>



<table id="demo" lay-filter="test"></table>





<script src="../static/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use('table', function(){
        var table = layui.table;

        table.render({
            elem: '#demo'
            ,height: 450
            ,url: '/UserInformation/getPage' //数据接口
            ,toolbar: 'default'
            ,page: true //开启分页
            ,cellMinWidth: 80
            ,request:
                {
                    pageName:"curr"
                    ,limitName:"limit"
                }
            ,curr:1
            ,limit:10
            ,id:'idTest'
            ,cols: [[ //表头
                {type:'checkbox', fixed: 'left'}
                ,{field: 'id', title: 'ID', align: 'center', sort: true, fixed: 'left'}
                ,{field: 'name', title: '姓名', align: 'center'}
                ,{field: 'gender', title: '性别', align: 'center'}
                ,{field: 'level', title: '等级', align: 'center'}
                ,{field: 'exp', title: '经验值', align: 'center'}
                ,{field: 'introduction', title: '简介', align: 'center'}
                ,{field: 'login', title: '账号', align: 'center'}
                ,{field: 'title', title: '称号', align: 'center'}
            ]]
        });


        //监听表格复选框选择
        table.on('checkbox(demo)', function(obj){
            console.log(obj)
        });

        //监听头工具栏事件
        table.on('toolbar(test)', function(obj)
        {
            var checkStatus = table.checkStatus(obj.config.id),
                data = checkStatus.data,
                editList=[];
            for(var i=0;i<data.length;i++)
            { //因为这块获取的是数组，所以当前行数据应该为数组中的第一条，所以要遍历数组
                layui.jquery.each(data[i],function(name,value)
                {
                    editList.push(value);
                })
            }

            switch(obj.event)
            {
                case 'add':
                    layer.msg('添加');
                    break;
                case 'update':
                    if(data.length === 0)
                    {
                        layer.msg('请选择一行');
                    } else if(data.length > 1)
                    {
                        layer.msg('只能同时编辑一个');
                    } else
                    {
                        layer.open({
                            anim: 3,
                            type: 2,
                            shadeClose: true,
                            shade: 0.8,
                            maxmin: true,
                            area: ['70%', '90%'],
                            content: '/UserInformation/FarmerInfoEdit?data='+encodeURIComponent(JSON.stringify(checkStatus.data)),  //设置你要弹出的jsp页面
                            success: function(layer, index){
                                var body = layer.getChildFrame('body', index);
                                var iframeWin = window[layer.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                                var inputList = body.find("input");//获取到子窗口的所有的input标签
                                for (var i = 0; i < inputList.length; i++ ) {
                                    layui.jquery(inputList[i]).val(editList[i]); //遍历子窗口的input标签，将之前数组中的值一次放入显示
                                }
                            }
                        });

                    }
                    break;
                case 'delete':
                    if(data.length === 0)
                    {
                        layer.msg('请选择一行');
                    }
                    else
                    {

                        layer.confirm('真的删除行', function(index){
                            layui.jquery.ajax({


                                url: "/UserInformation/delete",
                                type: "POST",
                                data:JSON.stringify(data),
                                contentType:"application/json;charset=utf-8",
                                success: function(data){
                                    var json=eval(data);

                                    if(json.code==0){
                                        //关闭弹框
                                        var num=layui.jquery(".layui-laypage-limits").find("option:selected").val();
                                        var page=json.count%num;
                                        var page1;
                                        if (page==0)
                                        {
                                            page1=parseInt(json.count/num);
                                        }
                                        else
                                        {
                                            page1=parseInt(json.count/num+1);
                                        }
                                        layer.close(index);
                                        layer.msg("删除成功", {icon: 6});
                                        layui.jquery(".layui-laypage-btn").click();
                                        table.reload('demo',
                                            {
                                                page: {
                                                    curr: page1,limit:10
                                                },
                                                where: {
                                                    time:new Date()
                                                }
                                            });

                                    }else{
                                        layer.msg("删除失败", {icon: 5});
                                    }
                                }
                            });
                        });
                    }
                    break;
            };
        });



        layui.jquery('.demoTable .layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
    });
</script>

</body>
</html>