<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>layui</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" type="text/css" href="../static/layui/css/layui.css">
</head>

<body>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>编辑用户信息</legend>
</fieldset>
<div class="layui-form  layui-from-pane" action="" accept-charset="UTF-8" id="formdemo">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">ID</label>
            <div class="layui-input-block">
                <input type="text" name="id" id="date1" autocomplete="off" class="layui-input" readonly="" value="${data.id}">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">用户姓名</label>
            <div class="layui-input-inline">
                <input type="text" name="name" autocomplete="off" placeholder="用户姓名" class="layui-input" value="${data.name}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">用户性别</label>
        <div class="layui-input-block">
            <input type="text" name="gender" autocomplete="off" placeholder="用户性别" class="layui-input" value="${data.gender}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">用户等级</label>
        <div class="layui-input-block">
            <input type="text" name="level" autocomplete="off" placeholder="用户等级" class="layui-input" value="${data.level}">
        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">用户经验值</label>
        <div class="layui-input-block">
            <input type="text" name="exp" autocomplete="off" placeholder="用户经验值" class="layui-input" value="${data.exp}">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">用户介绍</label>
        <div class="layui-input-inline">
            <input type="text" name="introduction" placeholder="请输入用户介绍" autocomplete="off" class="layui-input" value="${data.introduction}">
        </div>
    </div>



    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">用户账号</label>
            <div class="layui-input-block">
                <input type="text" name="login"autocomplete="off" class="layui-input" value="${data.login}">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">用户称号</label>
            <div class="layui-input-inline">
                <input type="text" name="title" autocomplete="off" class="layui-input" value="${data.title}">
            </div>
        </div>
    </div>


    <div class="layui-form-item">
        <button class="layui-btn layui-btn-submit " lay-submit="" lay-filter="edit" style="margin-left: 45%;" onsubmit="return false">提交</button>
    </div>

</div>

<script src="../static/layui/layui.js" charset="utf-8"></script>
<script src="../static/layui/layui.all.js" charset="utf-8"></script>

<script>


    layui.use(['form','layer'] ,function(){
        var
            form=layui.form,
            layer = layui.layer

        var index = parent.layer.getFrameIndex(window.name);
        form.render();


        //监听提交
        form.on('submit(edit)', function(data)
        {
            layui.jquery.ajax({
                url: "/UserInformation/edit",
                type: "POST",
                async: false,   //不要让它异步提交
                data:JSON.stringify(data.field),
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    if(data.msg!='0'){
                        layer.alert("修改成功",{icon: 1,time:2000},function () {
                            layer.close(layer.index);
                            window.parent.location.reload();    //重新加载父页面，进行数据刷新
                        });
                    } else{
                        layer.alert("修改失败",{icon: 2,time:2000});
                    }
                }
            });
            return false;
        });

    });
</script>

</body>
</html>

